import {
  REACT_APP_BACKEND,
  REACT_APP_OAUTH_CLIENT_ID,
  REACT_APP_OAUTH_CLIENT_SECRET,
} from '@env';
import {
  Montserrat_100Thin,
  Montserrat_100Thin_Italic,
  Montserrat_200ExtraLight,
  Montserrat_200ExtraLight_Italic,
  Montserrat_300Light,
  Montserrat_300Light_Italic,
  Montserrat_400Regular,
  Montserrat_400Regular_Italic,
  Montserrat_500Medium,
  Montserrat_500Medium_Italic,
  Montserrat_600SemiBold,
  Montserrat_600SemiBold_Italic,
  Montserrat_700Bold,
  Montserrat_700Bold_Italic,
  Montserrat_800ExtraBold,
  Montserrat_800ExtraBold_Italic,
  Montserrat_900Black,
  Montserrat_900Black_Italic,
} from '@expo-google-fonts/montserrat';
import {
  ActionSheetProvider,
  connectActionSheet,
} from '@expo/react-native-action-sheet';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {
  faArrowCircleLeft,
  faArrowRight,
  faBars,
  faChevronDown,
  faChevronLeft,
  faClock,
  faCog,
  faComment,
  faEllipsisV,
  faExclamationTriangle,
  faEye,
  faGraduationCap,
  faHeart,
  faHistory,
  faHome,
  faInfoCircle,
  faLightbulb,
  faMapMarker,
  faPaperPlane,
  faSearch,
  faTimes,
  faUnlockAlt,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import Toast from '@mitesh-v2stech/react-native-toast-message';
import axios from 'axios';
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import { useFonts } from 'expo-font';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { ThemeProvider } from 'react-native-elements';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AppRouter, { store } from './src';
import { getCurrentToken, setCurrentToken } from './src/helpers/Utils';
import { logoutUser } from './src/redux/auth/actions';
import * as RootNavigation from './src/RootNavigation';
import Loading from './src/screens/loading';

library.add(
  fab,
  faHome,
  faTimes,
  faCog,
  faEllipsisV,
  faHeart,
  faPaperPlane,
  faComment,
  faEye,
  faChevronLeft,
  faChevronDown,
  faBars,
  faSearch,
  faClock,
  faArrowCircleLeft,
  faUnlockAlt,
  faExclamationTriangle,
  faPaperPlane,
  faMapMarker,
  faHistory,
  faArrowRight,
  faUsers,
  faLightbulb,
  faInfoCircle,
  faGraduationCap
);

axios.interceptors.request.use(
  async (config) => {
    const token = await getCurrentToken();

    return {
      ...config,
      baseURL: REACT_APP_BACKEND,
      headers: {
        ...config.headers,
        Authorization: `Bearer ${token?.access_token}`,
        'Content-Type': 'application/json',
        Accept: '*/*',
      },
    };
  },
  (error) => {
    return Promise.reject(error);
  }
);

const refreshAuthLogic = async (failedRequest) => {
  const token = await getCurrentToken();
  const refreshClient = axios.create();
  return refreshClient
    .post(`${REACT_APP_BACKEND}/oauth/token`, {
      grant_type: 'refresh_token',
      refresh_token: token.refresh_token,
      client_id: REACT_APP_OAUTH_CLIENT_ID,
      client_secret: REACT_APP_OAUTH_CLIENT_SECRET,
      scope: '',
    })
    .then((tokenRefreshResponse) => {
      setCurrentToken(tokenRefreshResponse.data);
      // eslint-disable-next-line no-param-reassign
      failedRequest.response.config.headers.Authorization = `Bearer ${tokenRefreshResponse.data.token}`;
      return Promise.resolve();
    })
    .catch(() => {
      store.dispatch(
        // eslint-disable-next-line no-underscore-dangle
        logoutUser(RootNavigation.appContainerRef.current._navigation)
      );
    });
};

// Instantiate the interceptor (you can chain it as it returns the axios instance)
createAuthRefreshInterceptor(axios, refreshAuthLogic, {});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const App = () => {
  const [appIsReady, setAppIsReady] = useState(false);
  const [montserratLoaded] = useFonts({
    Montserrat_100Thin,
    Montserrat_200ExtraLight,
    Montserrat_300Light,
    Montserrat_400Regular,
    Montserrat_500Medium,
    Montserrat_600SemiBold,
    Montserrat_700Bold,
    Montserrat_800ExtraBold,
    Montserrat_900Black,
    Montserrat_100Thin_Italic,
    Montserrat_200ExtraLight_Italic,
    Montserrat_300Light_Italic,
    Montserrat_400Regular_Italic,
    Montserrat_500Medium_Italic,
    Montserrat_600SemiBold_Italic,
    Montserrat_700Bold_Italic,
    Montserrat_800ExtraBold_Italic,
    Montserrat_900Black_Italic,
  });

  useEffect(() => {
    async function prepare() {
      try {
        // todo: any bootstrap logic put here
        await new Promise((resolve) => setTimeout(resolve, 1000));
      } catch (e) {
        console.warn(e);
      } finally {
        // Tell the application to render
        setAppIsReady(true);
      }
    }

    prepare();
  }, []);

  return (
    <ThemeProvider>
      <SafeAreaProvider>
        <ActionSheetProvider>
          {!montserratLoaded || !appIsReady ? (
            <Loading />
          ) : (
            <View style={styles.container}>
              <AppRouter />
              <Toast ref={(ref) => Toast.setRef(ref)} />
            </View>
          )}
        </ActionSheetProvider>
      </SafeAreaProvider>
    </ThemeProvider>
  );
};

const ConnectedApp = connectActionSheet(App);

export default ConnectedApp;
