import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { COLOR_CONSTANT } from '../../constants/v2/style-constant';

const UnderConstruction = ({ navigation }) => {
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          width: '100%',
          height: '100%',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text>Mohon maaf, modul belum tersedia</Text>
        <TouchableOpacity
          style={{ marginTop: 16 }}
          onPress={() => navigation.goBack()}
        >
          <Text
            style={{
              color: COLOR_CONSTANT.BLUE,
              fontWeight: 'bold',
              fontSize: 10,
            }}
          >
            Kembali
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default withNavigation(UnderConstruction);
