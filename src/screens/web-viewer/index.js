import React, { useEffect } from 'react';
import WebView from 'react-native-webview';
import { withNavigation } from 'react-navigation';
import CommonHeader from '../../components/common/common-header';

const WebViewer = ({ navigation }) => {
  useEffect(() => {
    if (
      !(
        navigation &&
        navigation.state &&
        navigation.state.params &&
        navigation.state.params.data
      )
    ) {
      navigation.goBack();
    }
  }, []);
  return (
    <>
      <CommonHeader
        title={navigation.state.params.data.title}
        onBackPressed={() => navigation.goBack()}
      />
      {navigation.state.params.data.url && (
        <WebView
          javaScriptEnabled
          style={{ flex: 1 }}
          source={{
            uri: navigation.state.params.data.url,
          }}
          onError={(err) => console.log(`${err}`)}
        />
      )}
    </>
  );
};

export default withNavigation(WebViewer);
