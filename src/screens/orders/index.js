// @ts-nocheck
import React from 'react';
import { SearchBar } from 'react-native-elements';
import { useActionSheet } from '@expo/react-native-action-sheet';
import { connect } from 'react-redux';
import {
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
// import { withNavigation } from 'react-navigation';
import CommonHeader from '../../components/common/common-header';
// import { COLOR_CONSTANT } from '../../constants/style-constant';
import ListItem from './components/list-item';
import { loadOrders } from '../../redux/order/action';
import LoadingView from '../../components/LoadingView';

const Orders = ({ navigation, loadOrdersAction, list }) => {
  const { data, page, loading } = list;

  const { showActionSheetWithOptions } = useActionSheet();

  // useEffect(() => {
  //   loadOrdersAction();
  // }, [loadOrdersAction]);

  return (
    <>
      <CommonHeader title="Orders" onBackPressed={() => navigation.goBack()} />

      <SearchBar
        inputContainerStyle={{
          backgroundColor: '#E5E5E5',
          borderColor: '#8E8E8E',
          borderWidth: 1,
        }}
        round
        containerStyle={{
          backgroundColor: 'white',
        }}
        searchIcon={{ size: 24 }}
        placeholder="Type order number..."
        style={{ fontSize: 14, fontStyle: 'italic' }}
      />
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={data == null && loading}
            onRefresh={() => loadOrdersAction()}
          />
        }
      >
        {data && (
          <>
            {data.map((t, i) => {
              return (
                <ListItem
                  showActionSheetWithOptions={showActionSheetWithOptions}
                  key={`${t.rfq}`.concat(`${i}`)}
                  data={t}
                />
              );
            })}
            {loading ? (
              <View
                style={{
                  padding: 24,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
              >
                <LoadingView />
              </View>
            ) : (
              <>
                {page.currentPage < page.totalPage && (
                  <View
                    style={{
                      padding: 24,
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        loadOrdersAction({
                          selectedPage: page.currentPage + 1,
                        })
                      }
                    >
                      <Text
                        style={{
                          color: 'gray',
                          fontSize: 16,
                        }}
                      >
                        Load More
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </>
            )}
          </>
        )}
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ orderApp }) => {
  const { list } = orderApp;

  return {
    list,
  };
};

export default connect(mapStateToProps, {
  loadOrdersAction: loadOrders,
})(Orders);
