/* eslint-disable no-nested-ternary */
import React from 'react';
import moment from 'moment';

import { Text, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faClipboardList } from '@fortawesome/free-solid-svg-icons';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';

const ListItem = ({ data }) => {
  return (
    <>
      {data && (
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 1,
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.WHITE,
              height: 85,
              borderRadius: 5,
              overflow: 'hidden',
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <View
              style={{ width: 60, justifyContent: 'center', marginRight: 5 }}
            >
              <View
                style={{
                  backgroundColor: COLOR_CONSTANT.WHITE,
                  margin: 0,
                  borderRadius: 4,
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 85,
                  width: 65,
                }}
              >
                <FontAwesomeIcon
                  icon={faClipboardList}
                  size={40}
                  style={{ color: COLOR_CONSTANT.DARKBLUE }}
                />
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', margin: 4 }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 4,
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Order Number
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 12,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {data.orderno == null ? '-' : data.orderno}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Service Group
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 10,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {data.service_group == null ? '-' : data.service_group}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Order Date
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 10,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {moment(data.tglorder).format('DD MMMM yyyy')}
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', marginBottom: 4 }}>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Status
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor:
                      data.quot == null && data.orderno == null
                        ? COLOR_CONSTANT.ORANGE
                        : data.quot != null && data.orderno == null
                        ? COLOR_CONSTANT.DARKBLUE
                        : COLOR_CONSTANT.CYAN,
                    borderRadius: 12,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 0,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      textAlign: 'right',
                      fontSize: 10,
                      marginLeft: 10,
                      marginRight: 10,
                      color: COLOR_CONSTANT.WHITE,
                    }}
                  >
                    {data.quot == null && data.orderno == null
                      ? 'RFQ'
                      : data.quot != null && data.orderno == null
                      ? 'Quotation'
                      : 'On Going'}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      )}
    </>
  );
};

export default ListItem;
