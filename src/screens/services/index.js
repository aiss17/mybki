/* eslint-disable eqeqeq */
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { Image, ScrollView, View, Text, TouchableOpacity } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
// @ts-ignore
import CartSvg from '../../assets/icons/cart.svg';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import MenuItems from './components/menu-items';
import MostOrderedServices from './components/most-ordered-services';
import Search from './components/search';
import {
  loadServices,
  selectedServices,
  selectedCart,
  clearRfq,
} from '../../redux/services/action';

// @ts-ignore
const logo = require('../../assets/logos/logo-color.png');

const Services = ({
  list,
  rfqCRM,
  selectedCartList,
  remove,
  navigation,
  loadServicesAction,
  selectedServicesAction,
  selectedCartAction,
  clearRfqAction,
}) => {
  console.log('list', list);
  const insets = useSafeAreaInsets();

  const [count, setCount] = useState(0);
  const [cartCountKlasifikasi, setCartCountKlasifikasi] = useState([]);
  const [cartCountKomersil, setCartCountKomersil] = useState([]);

  useEffect(() => {
    loadServicesAction();
  }, [loadServicesAction]);

  useEffect(() => {
    if (selectedCartList?.message === 'Klasifikasi') {
      setCartCountKlasifikasi([]);
    }
    if (selectedCartList?.message === 'Komersil') {
      setCartCountKomersil([]);
    }
  }, [selectedCartList]);

  useEffect(() => {
    if (rfqCRM?.message === 'Klasifikasi' || rfqCRM?.message === 'Komersil') {
      clearRfqAction(null);
    }
  }, [rfqCRM]);

  const removeServices = (id, status) => {
    if (status === 'Klasifikasi') {
      let array = [...cartCountKlasifikasi];
      if (array.length > 0) {
        array = array.filter((q) => q.items_id != id);
        setCartCountKlasifikasi(array);
        selectedCartAction(array, cartCountKomersil);
      }
    } else if (status === 'Komersil') {
      let array = [...cartCountKomersil];
      if (array.length > 0) {
        array = array.filter((q) => q.items_id != id);
        setCartCountKomersil(array);
        selectedCartAction(cartCountKlasifikasi, array);
      }
    }
  };

  useEffect(() => {
    if (remove?.message === 'Success') {
      removeServices(remove.id, remove.status);
    }
  }, [remove]);

  return (
    <>
      <View style={{ flex: 1, backgroundColor: COLOR_CONSTANT.WHITE }}>
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              height: 78,
              marginTop: insets.top,
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 18,
              borderBottomWidth: 1,
              borderColor: COLOR_CONSTANT.GRAY,
            }}
          >
            <View>
              <Image
                source={logo}
                resizeMode="contain"
                style={{ height: 44, width: 103 }}
              />
            </View>
            <View style={{ flex: 1, marginHorizontal: 8 }}>
              <Search />
            </View>
            {count != 0 && (
              <View
                style={{
                  position: 'absolute',
                  backgroundColor: 'red',
                  width: 16,
                  height: 16,
                  borderRadius: 15 / 2,
                  right: 10,
                  top: +10,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Text
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    color: '#FFFFFF',
                    fontSize: 8,
                  }}
                >
                  {count}
                </Text>
              </View>
            )}

            <TouchableOpacity
              onPress={() => {
                if (count != 0) {
                  selectedCartAction(cartCountKlasifikasi, cartCountKomersil);
                  navigation.navigate('CartDetail', {
                    screen: 'CartDetail',
                  });
                } else {
                  navigation.navigate('Service', {
                    screen: 'Service',
                  });
                }
              }}
              // key={t.rfq_id}
            >
              <CartSvg style={{ color: COLOR_CONSTANT.DARKGRAY }} />
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 16, marginHorizontal: 18 }}>
            <MenuItems />
          </View>
          <View style={{ marginTop: 16, marginHorizontal: 18 }}>
            <MostOrderedServices
              navigation={navigation}
              list={list}
              selectedServicesAction={selectedServicesAction}
              setCount={setCount}
              cartCountKlasifikasi={cartCountKlasifikasi}
              setCartCountKlasifikasi={setCartCountKlasifikasi}
              setCartCountKomersil={setCartCountKomersil}
              cartCountKomersil={cartCountKomersil}
            />
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const mapStateToProps = ({ servicesApp }) => {
  const { list, rfqCRM, remove } = servicesApp;

  return {
    list,
    rfqCRM,
    remove,
    selectedCartList: servicesApp.selectedCart,
  };
};

export default connect(mapStateToProps, {
  loadServicesAction: loadServices,
  selectedServicesAction: selectedServices,
  selectedCartAction: selectedCart,
  clearRfqAction: clearRfq,
})(Services);
