import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { Col, Row } from 'react-native-responsive-grid-system';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';

const dummy = require('../../../../assets/sample/media1.png');

const OtherServices = ({ navigation }) => {
  return (
    <>
      <View style={{ marginBottom: 12 }}>
        <Text
          style={{
            fontFamily: 'Montserrat_700Bold',
            fontSize: 20,
            color: COLOR_CONSTANT.DARKBLUE,
          }}
        >
          Other Services
        </Text>
      </View>
      <View style={{ paddingVertical: 4 }}>
        <Row>
          {[1, 2, 3, 4].map((t) => {
            return (
              <Col xs={6} sm={6} md={6} lg={6} key={t}>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('Service', {
                      screen: 'ServiceDetail',
                    })
                  }
                >
                  <View
                    style={[
                      {
                        borderRadius: 5,
                        marginBottom: 12,
                      },
                      STYLE_CONSTANT.SHADOW2,
                    ]}
                  >
                    <View
                      style={{
                        height: 172,
                        width: '100%',
                        overflow: 'hidden',
                        borderRadius: 5,
                        backgroundColor: 'white',
                      }}
                    >
                      <View style={{ backgroundColor: 'blue', height: 96 }}>
                        <Image
                          source={dummy}
                          resizeMode="stretch"
                          style={{ height: '100%', width: '100%' }}
                        />
                      </View>
                      <View style={{ marginLeft: 8 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            fontSize: 12,
                            color: COLOR_CONSTANT.BLACK,
                            marginTop: 6,
                          }}
                        >
                          Service 1
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_400Regular',
                            fontSize: 9,
                            color: COLOR_CONSTANT.BLACK,
                            marginVertical: 6,
                          }}
                        >
                          Short description about service
                        </Text>
                        <View
                          style={{
                            backgroundColor: COLOR_CONSTANT.CYAN,
                            width: 82,
                            height: 21,
                            borderRadius: 14,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                        >
                          <Text
                            style={{
                              fontFamily: 'Montserrat_700Bold',
                              fontSize: 8,
                              color: COLOR_CONSTANT.WHITE,
                            }}
                          >
                            + Add Services
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              </Col>
            );
          })}
        </Row>
      </View>
    </>
  );
};

export default OtherServices;
