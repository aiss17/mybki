import React from 'react';
import { Image, Text, View } from 'react-native';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';

const dummy = require('../../../../assets/sample/media1.png');

const ServiceDetail = ({ navigation, data }) => {
  return (
    <>
      {data && (
        <>
          <View style={{ marginBottom: 12 }}>
            <Text
              style={{
                fontFamily: 'Montserrat_700Bold',
                fontSize: 20,
                color: COLOR_CONSTANT.DARKBLUE,
              }}
            >
              {data.description}
            </Text>
          </View>
          <View
            style={[
              {
                borderRadius: 5,
                marginBottom: 12,
              },
              STYLE_CONSTANT.SHADOW2,
            ]}
          >
            <View
              style={{
                height: 376,
                width: '100%',
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: 'white',
              }}
            >
              <View style={{ backgroundColor: 'blue', height: 206 }}>
                <Image
                  source={dummy}
                  resizeMode="stretch"
                  style={{ height: '100%', width: '100%' }}
                />
              </View>
              <View style={{ flex: 1, marginLeft: 8 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 12,
                    color: COLOR_CONSTANT.BLACK,
                    marginVertical: 6,
                  }}
                >
                  {data.long_description}
                </Text>
                <Text
                  style={{
                    fontFamily: 'Montserrat_700Bold',
                    fontSize: 14,
                    color: COLOR_CONSTANT.BLACK,
                    marginVertical: 6,
                  }}
                >
                  Services Rate
                </Text>
                <Text
                  style={{
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 12,
                    color: COLOR_CONSTANT.BLACK,
                    marginVertical: 6,
                  }}
                >
                  Rp {data.rate}
                </Text>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                    justifyContent: 'flex-end',
                    margin: 12,
                  }}
                >
                  <View
                    style={{
                      backgroundColor: COLOR_CONSTANT.CYAN,
                      width: 116,
                      height: 30,
                      borderRadius: 14,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'Montserrat_700Bold',
                        fontSize: 11,
                        color: COLOR_CONSTANT.WHITE,
                      }}
                    >
                      + Add Services
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </>
      )}
    </>
  );
};

export default ServiceDetail;
