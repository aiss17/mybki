import React from 'react';
import { connect } from 'react-redux';

import { Image, ScrollView, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
// @ts-ignore
import CartSvg from '../../../../assets/icons/cart.svg';
import { COLOR_CONSTANT } from '../../../../constants/style-constant';
import Search from '../search';
import OtherServices from './other-services';
import ServiceDetail from './service-detail';
import { selectedServices } from '../../../../redux/services/action';

// @ts-ignore
const logo = require('../../../../assets/logos/logo-color.png');

const Detail = ({ navigation, selectedServices }) => {
  const insets = useSafeAreaInsets();
  return (
    <>
      <View style={{ flex: 1, backgroundColor: COLOR_CONSTANT.WHITE }}>
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              height: 78,
              marginTop: insets.top,
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 18,
              borderBottomWidth: 1,
              borderColor: COLOR_CONSTANT.GRAY,
            }}
          >
            <View>
              <Image
                source={logo}
                resizeMode="contain"
                style={{ height: 44, width: 106 }}
              />
            </View>
            <View style={{ flex: 1, marginHorizontal: 8 }}>
              <Search />
            </View>
            <View>
              <CartSvg style={{ color: COLOR_CONSTANT.DARKGRAY }} />
            </View>
          </View>
          <View style={{ marginTop: 16, marginHorizontal: 18 }}>
            <ServiceDetail navigation={navigation} data={selectedServices.data} />
          </View>
          <View style={{ marginTop: 16, marginHorizontal: 18 }}>
            <OtherServices navigation={navigation} />
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const mapStateToProps = ({ servicesApp }) => {
  const { selectedServices } = servicesApp;

  return {
    selectedServices,
  };
};

export default connect(mapStateToProps, {
  selectedServicesAction: selectedServices,
})(Detail);
