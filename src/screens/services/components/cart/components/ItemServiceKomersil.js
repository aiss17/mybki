/* eslint-disable no-param-reassign */
/* eslint-disable eqeqeq */
// @ts-nocheck
import React, { useState, useEffect } from 'react';
import {
  ScrollView,
  Text,
  View,
  Button,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { TextInput } from 'react-native-paper';
import ModalSelector from 'react-native-modal-selector';
import { COLOR_CONSTANT } from '../../../../../constants/style-constant';
import ThrashSvg from '../../../../../assets/icons/thrash.svg';
import ArrowDownSvg from '../../../../../assets/icons/arrowdown.svg';

const Itemservice = ({
  selectedCart,
  vessel,
  onSubmitKomersil,
  setItemDataKomersil,
  removeService,
}) => {
  const [expanded, setExpanded] = useState(false);
  const [expandedForm, setExpandedForm] = useState(false);
  const [checkId, setCheckId] = useState();

  const [lokasiFinal, setLokasiFinal] = useState([]);
  const [lokasi, setLokasi] = useState();
  const [lokasiId, setLokasiId] = useState();

  const [scopeFinal, setScopeFinal] = useState([]);
  const [scope, setScope] = useState();
  const [scopeId, setScopeId] = useState();

  const [catatanFinal, setCatatanFinal] = useState([]);
  const [catatan, setCatatan] = useState();
  const [catatanId, setCatatanId] = useState();

  const [assetFinal, setAssetFinal] = useState([]);
  const [assetTemporary, setAssetTemporary] = useState();

  useEffect(() => {
    if (onSubmitKomersil === true) {
      const filter = lokasiFinal.map((q) => {
        return {
          id: q.id,
          item_location: q.name,
          item_scope: '-',
          item_note: '-',
          asset: '-',
          label: '-',
        };
      });

      if (scopeFinal) {
        scopeFinal.forEach((q) => {
          filter.forEach((x) => {
            if (q.id == x.id) {
              x.item_scope = q.name;
            }
          });
        });
      }

      if (catatanFinal) {
        catatanFinal.forEach((q) => {
          filter.forEach((x) => {
            if (q.id == x.id) {
              x.item_note = q.name;
            }
          });
        });
      }

      if (assetFinal) {
        assetFinal.forEach((q) => {
          filter.forEach((x) => {
            if (q.id == x.id) {
              x.asset = q.value;
              x.label = q.label;
            }
          });
        });
      }
      setItemDataKomersil(filter);
    }
  }, [onSubmitKomersil]);

  let vesselList = [];
  if (vessel?.data)
    vesselList = vessel?.data.map((p) => {
      return {
        key: p.register,
        value: p.register,
        label: p.name,
      };
    });

  const locationChange = () => {
    const named = [lokasi];
    let data = [];
    if (lokasiFinal.length > 0 && lokasiFinal.some((q) => q.id == lokasiId)) {
      data = [...lokasiFinal];
      lokasiFinal.forEach((z) => {
        if (z.id == lokasiId) {
          z.name = lokasi;
        }
      });
    } else if (
      lokasiFinal.length > 0 &&
      lokasiFinal.some((q) => q.id != lokasiId)
    ) {
      data = [...lokasiFinal];
      named.forEach((p) => {
        const obj = {
          id: lokasiId,
          name: p,
        };
        data.push(obj);
      });
    } else {
      data = named.map((q) => {
        return {
          id: lokasiId,
          name: q,
        };
      });
    }
    setLokasiFinal(data);
  };

  const scopeChange = () => {
    const named = [scope];
    let data = [];
    if (scopeFinal.length > 0 && scopeFinal.some((q) => q.id == scopeId)) {
      data = [...scopeFinal];
      scopeFinal.forEach((z) => {
        if (z.id == scopeId) {
          z.name = scope;
        }
      });
    } else if (
      scopeFinal.length > 0 &&
      scopeFinal.some((q) => q.id != scopeId)
    ) {
      data = [...scopeFinal];
      named.forEach((p) => {
        const obj = {
          id: scopeId,
          name: p,
        };
        data.push(obj);
      });
    } else {
      data = named.map((q) => {
        return {
          id: scopeId,
          name: q,
        };
      });
    }
    setScopeFinal(data);
  };

  const catatanChange = () => {
    const named = [catatan];
    let data = [];
    if (
      catatanFinal.length > 0 &&
      catatanFinal.some((q) => q.id == catatanId)
    ) {
      data = [...catatanFinal];
      catatanFinal.forEach((z) => {
        if (z.id == catatanId) {
          z.name = catatan;
        }
      });
    } else if (
      catatanFinal.length > 0 &&
      catatanFinal.some((q) => q.id != catatanId)
    ) {
      data = [...catatanFinal];
      named.forEach((p) => {
        const obj = {
          id: catatanId,
          name: p,
        };
        data.push(obj);
      });
    } else {
      data = named.map((q) => {
        return {
          id: catatanId,
          name: q,
        };
      });
    }
    setCatatanFinal(data);
  };

  const assetChange = (val, idDom) => {
    const named = [val];
    let data = [];
    if (assetFinal.length > 0 && assetFinal.some((q) => q.id == idDom)) {
      data = [...assetFinal];
      assetFinal.forEach((z) => {
        if (z.id == idDom) {
          z.value = val.value;
          z.label = val.label;
          z.key = val.key;
        }
      });
    } else if (assetFinal.length > 0 && assetFinal.some((q) => q.id != idDom)) {
      data = [...assetFinal];
      named.forEach((p) => {
        const obj = {
          id: idDom,
          key: p.key,
          value: p.value,
          label: p.label,
        };
        data.push(obj);
      });
    } else {
      data = named.map((q) => {
        return {
          id: idDom,
          key: q.key,
          value: q.value,
          label: q.label,
        };
      });
    }
    setAssetFinal(data);
    setAssetTemporary(null);
  };

  const styles = StyleSheet.create({
    textInput: {
      height: 50,
      paddingLeft: 6,
      color: COLOR_CONSTANT.DARKGRAY,
      flexWrap: 'wrap',
      lineHeight: 23,
      backgroundColor: 'white',
    },
  });

  const renderInfo = (description, name, id) => {
    let finalLocation;
    const checkLocation = lokasiFinal.some((q) => q.id == id);
    if (lokasiFinal.length > 0 && checkLocation) {
      finalLocation = lokasiFinal.filter((q) => q.id == id);
    }

    let finalScope;
    const checkScope = scopeFinal.some((q) => q.id == id);
    if (scopeFinal.length > 0 && checkScope) {
      finalScope = scopeFinal.filter((q) => q.id == id);
    }

    let finalCatatan;
    const checkCatatan = catatanFinal.some((q) => q.id == id);
    if (catatanFinal.length > 0 && checkCatatan) {
      finalCatatan = catatanFinal.filter((q) => q.id == id);
    }

    let finalAsset;
    const checkAsset = assetFinal.some((q) => q.id == id);
    if (assetFinal.length > 0 && checkAsset) {
      finalAsset = assetFinal.filter((q) => q.id == id);
    }

    return (
      <ScrollView style={{ paddingTop: 1 }}>
        <View style={{ flex: 1 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 12,
              marginVertical: 2,
              color: COLOR_CONSTANT.DARKGRAY,
            }}
          >
            Deksripsi Layanan
          </Text>
        </View>
        <View style={{ flex: 1, marginTop: 5 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 12,
              paddingHorizontal: 8,
            }}
          >
            {description == '' ? '-' : description}
          </Text>
        </View>
        <View style={{ flex: 1, marginTop: 7 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 12,
              marginVertical: 2,
              color: COLOR_CONSTANT.DARKGRAY,
            }}
          >
            Kategori Layanan
          </Text>
        </View>
        <View style={{ flex: 1, marginTop: 5 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 12,
              paddingHorizontal: 8,
            }}
          >
            {name}
          </Text>
        </View>
        <View style={{ flex: 1, marginTop: 7 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 12,
              marginVertical: 2,
              color: COLOR_CONSTANT.DARKGRAY,
            }}
          >
            Scope Layanan
          </Text>
        </View>
        <View style={{ flex: 1, marginTop: 7 }}>
          <TextInput
            placeholder="Scope Layanan…"
            onChangeText={(e) => {
              setScope(e);
              setScopeId(id);
            }}
            numberOfLines={100}
            maxLength={1000}
            multiline
            editable
            defaultValue={!checkScope ? '' : finalScope && finalScope[0].name}
            selectionColor={COLOR_CONSTANT.BLUE}
            underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
            style={styles.textInput}
            left={
              <TextInput.Icon
                name="clipboard-check-outline"
                size={28}
                color={COLOR_CONSTANT.DARKGRAY}
              />
            }
          />
        </View>
        <View style={{ flex: 1, marginTop: 7 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 12,
              marginVertical: 2,
              color: COLOR_CONSTANT.DARKGRAY,
            }}
          >
            Catatan Layanan
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: COLOR_CONSTANT.WHITE,
            marginTop: 5,
          }}
        >
          <TextInput
            placeholder="Catatan Layanan…"
            onChangeText={(e) => {
              setCatatan(e);
              setCatatanId(id);
            }}
            numberOfLines={100}
            maxLength={1000}
            multiline
            editable
            defaultValue={
              !checkCatatan ? '' : finalCatatan && finalCatatan[0].name
            }
            selectionColor={COLOR_CONSTANT.BLUE}
            underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
            style={styles.textInput}
            left={
              <TextInput.Icon
                name="file-document-edit-outline"
                size={28}
                color={COLOR_CONSTANT.DARKGRAY}
              />
            }
          />
        </View>
        <View style={{ flex: 1, marginTop: 7 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 12,
              marginVertical: 2,
              color: COLOR_CONSTANT.DARKGRAY,
            }}
          >
            Lokasi Layanan
          </Text>
        </View>
        <View style={{ flex: 1, marginTop: 5 }}>
          <TextInput
            onChangeText={(e) => {
              setLokasi(e);
              setLokasiId(id);
            }}
            placeholder="Lokasi Layanan…"
            numberOfLines={100}
            maxLength={1000}
            multiline
            editable
            defaultValue={
              !checkLocation ? '' : finalLocation && finalLocation[0].name
            }
            selectionColor={COLOR_CONSTANT.BLUE}
            underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
            style={styles.textInput}
            left={
              <TextInput.Icon
                name="map-marker"
                size={28}
                color={COLOR_CONSTANT.DARKGRAY}
              />
            }
          />
        </View>
        <View style={{ flex: 1, marginTop: 7 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 12,
              marginVertical: 2,
              color: COLOR_CONSTANT.DARKGRAY,
            }}
          >
            Asset di BKI
          </Text>
        </View>
        <View style={{ flex: 1, marginTop: 5 }}>
          <ModalSelector
            data={vesselList}
            initValue={
              !checkAsset ? assetTemporary : finalAsset && finalAsset[0].label
            }
            onChange={(option) => {
              setAssetTemporary(option.label);
              assetChange(option, id);
            }}
          />
        </View>
        <View style={{ flex: 1, marginTop: 10, marginBottom: 15 }}>
          <Button
            title="Simpan"
            onPress={() => {
              locationChange();
              scopeChange();
              catatanChange();
              setExpanded(!expanded);
              setCheckId(id);
            }}
          />
        </View>
      </ScrollView>
    );
  };
  return (
    <>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          borderColor: COLOR_CONSTANT.GRAY,
          minWidth: 100,
          minHeight: 50,
          paddingHorizontal: 8,
          backgroundColor: COLOR_CONSTANT.DARKBLUE,
          paddingTop: 14,
          paddingBottom: 10,
          borderTopEndRadius: 5,
          borderTopStartRadius: 5,
        }}
      >
        <Text
          style={{
            color: COLOR_CONSTANT.WHITE,
            minWidth: 270,
            fontSize: 13,
            fontFamily: 'Montserrat_700Bold',
          }}
        >
          Services
        </Text>
        <TouchableOpacity
          onPress={() => {
            setExpandedForm(!expandedForm);
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              paddingHorizontal: 80,
            }}
          >
            <ArrowDownSvg />
          </View>
        </TouchableOpacity>
      </View>
      {vessel.message === 'Success' && expandedForm === false && (
        <>
          <View>
            <ScrollView style={{ paddingTop: 10, marginHorizontal: 8 }}>
              {selectedCart && (
                <>
                  {selectedCart.komersil.map((q, i) => {
                    return (
                      <View key={q.items_id || i}>
                        <View
                          key={q.items_id || i}
                          style={{ flexDirection: 'row', marginBottom: 5 }}
                        >
                          <View
                            style={{
                              flex: 1,
                              borderColor: COLOR_CONSTANT.GRAY,
                              minWidth: 250,
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                setCheckId(q.items_id);
                                setExpanded(!expanded);
                              }}
                            >
                              <Text
                                style={{
                                  color: COLOR_CONSTANT.BLUE,
                                  fontSize: 15,
                                }}
                              >
                                {q.description}
                              </Text>
                            </TouchableOpacity>
                          </View>

                          <View
                            style={{
                              flex: 1,
                              borderColor: COLOR_CONSTANT.GRAY,
                              marginBottom: 5,
                              paddingTop: 2,
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                removeService(q.items_id, 'Komersil');
                              }}
                            >
                              <ThrashSvg
                                style={{
                                  flex: 1,
                                  color: COLOR_CONSTANT.RED,
                                  paddingHorizontal: 45,
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                        {checkId == q.items_id && expanded && (
                          <>
                            {renderInfo(q.long_description, q.name, q.items_id)}
                          </>
                        )}
                      </View>
                    );
                  })}
                </>
              )}
            </ScrollView>
          </View>
        </>
      )}
    </>
  );
};

export default Itemservice;
