/* eslint-disable prefer-template */
/* eslint-disable eqeqeq */
import React, { useState } from 'react';
import {
  ScrollView,
  Text,
  View,
  Button,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { TextInput } from 'react-native-paper';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import ModalSelector from 'react-native-modal-selector';
import { COLOR_CONSTANT } from '../../../../../constants/style-constant';

const Userform = ({
  currentUser,
  unit,
  setAlamat,
  setOnSubmitKlasifikasi,
  setCabangSubmit,
  setCabangSubmitRico,
  setDateSubmit,
  setEmailPic,
  setPhonePic,
  setPic,
  cabangSubmitRico,
}) => {
  const [open, setOpen] = useState(false);
  const [condition, setCondition] = useState(false);
  const [date, setDate] = useState({});

  const monthNames = [
    'JAN',
    'FEB',
    'MAR',
    'APR',
    'MAY',
    'JUN',
    'JUL',
    'AUG',
    'SEP',
    'OCT',
    'NOV',
    'DEC',
  ];

  const handleConfirm = (dates) => {
    const day = dates.getDate().toString().padStart(2, '0');
    const month = monthNames[dates.getMonth()];
    const monthsubmit = dates.getMonth().toString().padStart(2, '0');
    const year = dates.getFullYear();
    const final = `${day} ${month} ${year}`;
    const finalForSubmit = `${year}-${monthsubmit}-${day}`;
    const finalDisplay = final.toString();
    setDate(finalDisplay);
    setOpen(false);
    setCondition(true);
    setDateSubmit(finalForSubmit);
  };

  let unitList = [];
  if (unit?.data)
    unitList = unit?.data.map((p) => {
      return {
        key: p.attributes.code,
        value: p.attributes.code,
        label: p.attributes.name,
      };
    });
  const styles = StyleSheet.create({
    textInput: {
      height: 50,
      paddingLeft: 6,
      color: COLOR_CONSTANT.DARKGRAY,
      flexWrap: 'wrap',
      lineHeight: 23,
      backgroundColor: 'white',
    },
  });
  return (
    <>
      {currentUser && unit.message === 'Success' && (
        <>
          <ScrollView style={{ paddingTop: 10, marginHorizontal: 8 }}>
            <View style={{ flexDirection: 'column' }}>
              <View style={{ flex: 1, marginTop: 5 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 12,
                    marginVertical: 2,
                    marginLeft: 3,
                  }}
                >
                  PIC Layanan
                </Text>
              </View>
              <View style={{ flex: 1, marginTop: 5 }}>
                <TextInput
                  placeholder="PIC Layanan…"
                  defaultValue={currentUser.company.pic}
                  onChangeText={setPic}
                  selectionColor={COLOR_CONSTANT.BLUE}
                  underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
                  style={styles.textInput}
                  left={
                    <TextInput.Icon
                      name="account"
                      size={28}
                      color={COLOR_CONSTANT.DARKGRAY}
                    />
                  }
                />
              </View>
              <View style={{ flex: 1, marginTop: 5 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 12,
                    marginVertical: 2,
                  }}
                >
                  Email PIC
                </Text>
              </View>
              <View style={{ flex: 1, marginTop: 5 }}>
                <TextInput
                  placeholder="Email PIC…"
                  defaultValue={currentUser.company.pic_email}
                  onChangeText={setEmailPic}
                  selectionColor={COLOR_CONSTANT.BLUE}
                  underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
                  style={styles.textInput}
                  left={
                    <TextInput.Icon
                      name="email"
                      size={28}
                      color={COLOR_CONSTANT.DARKGRAY}
                    />
                  }
                />
              </View>
              <View style={{ flex: 1, marginTop: 5 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 12,
                    marginVertical: 2,
                  }}
                >
                  No. Telepon PIC
                </Text>
              </View>
              <View style={{ flex: 1, marginTop: 5 }}>
                <TextInput
                  placeholder="No. Telepon PIC…"
                  defaultValue={currentUser.company.pic_mobile}
                  onChangeText={setPhonePic}
                  selectionColor={COLOR_CONSTANT.BLUE}
                  underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
                  style={styles.textInput}
                  left={
                    <TextInput.Icon
                      name="phone"
                      size={28}
                      color={COLOR_CONSTANT.DARKGRAY}
                    />
                  }
                />
                <View style={{ flex: 1, marginTop: 5 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 12,
                      marginVertical: 2,
                    }}
                  >
                    Alamat Billing
                  </Text>
                </View>
                <View style={{ flex: 1, marginTop: 5 }}>
                  <TextInput
                    placeholder="Alamat Billing…"
                    numberOfLines={50}
                    maxLength={300}
                    multiline={false}
                    defaultValue={currentUser.company.address}
                    onChangeText={setAlamat}
                    selectionColor={COLOR_CONSTANT.BLUE}
                    underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
                    style={styles.textInput}
                    left={
                      <TextInput.Icon
                        name="book-open"
                        size={28}
                        color={COLOR_CONSTANT.DARKGRAY}
                      />
                    }
                  />
                </View>

                <View style={{ flex: 1, marginTop: 5 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 12,
                      marginVertical: 2,
                    }}
                  >
                    Tanggal RFQ
                  </Text>
                </View>

                <TouchableOpacity
                  onPress={() => {
                    setOpen(true);
                  }}
                >
                  <View style={{ flex: 1, marginTop: 5 }}>
                    <TextInput
                      value={
                        condition === true ? date.toString() : 'Select date...'
                      }
                      editable
                      selectionColor={COLOR_CONSTANT.BLUE}
                      underlineColorAndroid={COLOR_CONSTANT.DARKGRAY}
                      style={styles.textInput}
                      left={
                        <TextInput.Icon
                          name="calendar-month"
                          size={28}
                          color={COLOR_CONSTANT.DARKGRAY}
                        />
                      }
                    />
                    {/* <Text
                      style={{
                        paddingTop: 3,
                        fontFamily: 'Montserrat_400Regular',
                        fontSize: 14,
                      }}
                    >
                      <TextInput.Icon
                        name="calendar-month"
                        size={28}
                        color={COLOR_CONSTANT.DARKGRAY}
                      />
                      {condition === true ? date.toString() : 'Select date...'}
                    </Text> */}
                  </View>
                </TouchableOpacity>
                <DateTimePickerModal
                  isVisible={open}
                  mode="date"
                  onConfirm={handleConfirm}
                  onCancel={() => setOpen(false)}
                />

                <View style={{ flex: 1, marginTop: 5 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 12,
                      marginVertical: 2,
                    }}
                  >
                    Pilih Cabang
                  </Text>
                </View>
                <ModalSelector
                  data={unitList}
                  style={{ marginTop: 5 }}
                  initValue={cabangSubmitRico.label || 'Select...'}
                  onChange={(option) => {
                    setCabangSubmit(option.value);
                    setCabangSubmitRico(option);
                  }}
                />

                <View
                  style={{
                    flex: 1,
                    marginTop: 20,
                    borderRadius: 40,
                    marginBottom: 10,
                  }}
                >
                  <Button
                    title="Lanjutkan"
                    onPress={() => {
                      // filterSubmit();
                      setOnSubmitKlasifikasi(true);
                    }}
                  />
                </View>
              </View>
            </View>
            {/* <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column' }}>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{ fontFamily: 'Montserrat_300Light', fontSize: 14 }}
                  >
                    PIC Layanan
                  </Text>
                </View>
                <View style={{ flex: 1, marginTop: 5 }}>
                  <TextInput
                    placeholder="PIC Layanan…"
                    defaultValue={currentUser.company.pic}
                    onChangeText={setPic}
                    style={{
                      flex: 1,
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 14,
                      paddingHorizontal: 8,
                      flexWrap: 'wrap',
                      borderWidth: 1,
                      borderColor: COLOR_CONSTANT.DARKGRAY,
                      borderRadius: 8,
                      width: 300,
                    }}
                  />
                </View>
                <View style={{ flex: 1, marginTop: 7 }}>
                  <Text
                    style={{ fontFamily: 'Montserrat_300Light', fontSize: 14 }}
                  >
                    Email PIC
                  </Text>
                </View>
                <View style={{ flex: 1, marginTop: 5 }}>
                  <TextInput
                    placeholder="Email PIC…"
                    defaultValue={currentUser.company.pic_email}
                    onChangeText={setEmailPic}
                    style={{
                      flex: 1,
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 14,
                      paddingHorizontal: 8,
                      flexWrap: 'wrap',
                      borderWidth: 1,
                      borderColor: COLOR_CONSTANT.DARKGRAY,
                      borderRadius: 8,
                      width: 300,
                    }}
                  />
                </View>
                <View style={{ flex: 1, marginTop: 7 }}>
                  <Text
                    style={{ fontFamily: 'Montserrat_300Light', fontSize: 14 }}
                  >
                    No. Telepon PIC
                  </Text>
                </View>
                <View style={{ flex: 1, marginTop: 5 }}>
                  <TextInput
                    placeholder="No. Telepon PIC…"
                    defaultValue={currentUser.company.pic_mobile}
                    onChangeText={setPhonePic}
                    style={{
                      flex: 1,
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 14,
                      paddingHorizontal: 8,
                      flexWrap: 'wrap',
                      borderWidth: 1,
                      borderColor: COLOR_CONSTANT.DARKGRAY,
                      borderRadius: 8,
                      width: 300,
                    }}
                  />
                  <View style={{ flex: 1, marginTop: 7 }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat_300Light',
                        fontSize: 14,
                      }}
                    >
                      Alamat Billing
                    </Text>
                  </View>
                  <View style={{ flex: 1, marginTop: 5 }}>
                    <TextInput
                      placeholder="Alamat Billing…"
                      numberOfLines={50}
                      multiline
                      editable
                      maxLength={300}
                      defaultValue={currentUser.company.address}
                      onChangeText={setAlamat}
                      style={{
                        flex: 1,
                        fontFamily: 'Montserrat_400Regular',
                        fontSize: 14,
                        paddingHorizontal: 8,
                        flexWrap: 'wrap',
                        borderWidth: 1,
                        borderColor: COLOR_CONSTANT.DARKGRAY,
                        borderRadius: 12,
                        height: 70,
                        width: 300,
                      }}
                    />
                  </View>

                  <View style={{ flex: 1, marginTop: 7 }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat_300Light',
                        fontSize: 14,
                      }}
                    >
                      Date
                    </Text>
                  </View>

                  <TouchableOpacity
                    onPress={() => {
                      setOpen(true);
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        paddingHorizontal: 8,
                        flexWrap: 'wrap',
                        borderWidth: 1,
                        borderColor: COLOR_CONSTANT.DARKGRAY,
                        borderRadius: 8,
                        width: 300,
                        height: 40,
                      }}
                    >
                      <Text
                        style={{
                          paddingTop: 9,
                          fontFamily: 'Montserrat_400Regular',
                          fontSize: 14,
                        }}
                      >
                        {condition === true
                          ? date.toString()
                          : 'Select date...'}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <DateTimePickerModal
                    isVisible={open}
                    mode="date"
                    onConfirm={handleConfirm}
                    onCancel={() => setOpen(false)}
                  />

                  <View style={{ flex: 1, marginTop: 7 }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat_300Light',
                        fontSize: 14,
                      }}
                    >
                      Pilih Cabang
                    </Text>
                  </View>
                  <ModalSelector
                    data={unitList}
                    style={{ marginTop: 5 }}
                    initValue="Select..."
                    onChange={(option) => {
                      setCabangSubmit(option.value);
                      setCabangSubmitRico(option);
                    }}
                  />

                  <View
                    style={{
                      flex: 1,
                      marginTop: 20,
                      borderRadius: 40,
                      marginBottom: 10,
                    }}
                  >
                    <Button
                      title="Lanjutkan"
                      onPress={() => {
                        // filterSubmit();
                        setOnSubmitKlasifikasi(true);
                      }}
                    />
                  </View>
                </View>
              </View>
            </View> */}
          </ScrollView>
        </>
      )}
    </>
  );
};

export default Userform;
