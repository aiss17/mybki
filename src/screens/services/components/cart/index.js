// @ts-nocheck
/* eslint-disable no-param-reassign */
/* eslint-disable eqeqeq */
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import { ScrollView, View } from 'react-native';
import CommonHeader from '../../../../components/common/common-header';
// import { useSafeAreaInsets } from 'react-native-safe-area-context';
// @ts-ignore
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';
import TabItem from '../../../../components/common/tab-item';
import UserForm from './components/userform';
import UserFormKomersil from './components/userformKomersil';
import ItemService from './components/itemservice';
import ItemServiceKomersil from './components/ItemServiceKomersil';
import {
  loadUnit,
  loadVessel,
  saveRfqCrm,
  clearCart,
  removeServices,
} from '../../../../redux/services/action';

const CartDetail = ({
  selectedCartList,
  currentUser,
  unit,
  vessel,
  rfqCRM,
  navigation,
  loadUnitAction,
  loadVesselAction,
  saveRfqCrmAction,
  clearCartAction,
  removeServicesAction,
}) => {
  const [activeTab, setActiveTab] = useState('class');
  const [onSubmitKlasifikasi, setOnSubmitKlasifikasi] = useState(false);
  const [onSubmitKomersil, setOnSubmitKomersil] = useState(false);
  const [dateSubmit, setDateSubmit] = useState();
  const [cabangSubmit, setCabangSubmit] = useState();
  const [cabangSubmitRico, setCabangSubmitRico] = useState({});
  const [alamat, setAlamat] = useState();
  const [emailPic, setEmailPic] = useState();
  const [phonePic, setPhonePic] = useState();
  const [pic, setPic] = useState();

  const [itemDataKlasifikasi, setItemDataKlasifikasi] = useState([]);
  const [itemDataKomersil, setItemDataKomersil] = useState([]);
  const handleTabChange = (val) => {
    setActiveTab(val);
  };

  useEffect(() => {
    loadUnitAction();
    loadVesselAction(currentUser.company_id);
  }, [loadUnitAction, loadVesselAction]);

  useEffect(() => {
    if (rfqCRM.message === 'Klasifikasi') {
      navigation.goBack();
      clearCartAction('Klasifikasi');
    } else if (rfqCRM.message === 'Komersil') {
      navigation.goBack();
      clearCartAction('Komersil');
    }
  }, [rfqCRM]);

  const filterSubmitKlasifikasi = () => {
    const carts = selectedCartList?.klasifikasi?.map((item) => item) ?? [];

    carts?.map((item) => {
      const test = itemDataKlasifikasi.find((i) => i.id == item.items_id);
      if (test) {
        item.asset.label = test.label;
        item.asset.value = test.asset;
        item.vessel.vessel_code = test.asset;
        item.vessel.vessel_name = test.label;
        item.note = test.item_note;
        item.scope = test.item_scope;
        item.location = test.item_location;
        item.label = test.item_note;
      }
      return item;
    });

    const FormData = {
      rfq_date: dateSubmit,
      client: currentUser.company_id,
      department: cabangSubmit,
      service_group: 'klasifikasi',
      status: 1,
      note: '-',
      items: itemDataKlasifikasi,
    };

    const ricoData = {
      alamat: alamat || currentUser.company.address,
      cabang: {
        label: cabangSubmitRico.label,
        value: cabangSubmitRico.value,
      },
      date: dateSubmit,
      divisi: cabangSubmit,
      email: emailPic || currentUser.company.pic_email,
      jenis: 'klasifikasi',
      phone: phonePic || currentUser.company.pic_mobile,
      picName: pic || currentUser.company.pic,
      pic_alamat: alamat || currentUser.company.address,
      pic_email: emailPic || currentUser.company.pic_email,
      pic_name: pic || currentUser.company.pic,
      pic_telepon: phonePic || currentUser.company.pic_mobile,
      rfq_date: dateSubmit,
      type: 'klasifikasi',
      sendCRM: true,
      service: carts,
    };

    if (itemDataKlasifikasi.length > 0) {
      saveRfqCrmAction(FormData, ricoData);
    }
  };

  useEffect(() => {
    if (itemDataKlasifikasi.length > 0) {
      filterSubmitKlasifikasi();
    }
  }, [itemDataKlasifikasi.length > 0]);

  const filterSubmitKomersil = () => {
    const carts = selectedCartList?.komersil?.map((item) => item) ?? [];

    carts?.map((item) => {
      const test = itemDataKomersil.find((i) => i.id == item.items_id);
      if (test) {
        item.asset.label = test.label;
        item.asset.value = test.asset;
        item.vessel.vessel_code = test.asset;
        item.vessel.vessel_name = test.label;
        item.note = test.item_note;
        item.scope = test.item_scope;
        item.location = test.item_location;
        item.label = test.item_note;
      }
      return item;
    });

    const FormData = {
      rfq_date: dateSubmit,
      client: currentUser.company_id,
      department: cabangSubmit,
      service_group: 'komersil',
      status: 1,
      note: '-',
      items: itemDataKlasifikasi,
    };

    const ricoData = {
      alamat: alamat || currentUser.company.address,
      cabang: {
        label: cabangSubmitRico.label,
        value: cabangSubmitRico.value,
      },
      date: dateSubmit,
      divisi: cabangSubmit,
      email: emailPic || currentUser.company.pic_email,
      jenis: 'komersil',
      phone: phonePic || currentUser.company.pic_mobile,
      picName: pic || currentUser.company.pic,
      pic_alamat: alamat || currentUser.company.address,
      pic_email: emailPic || currentUser.company.pic_email,
      pic_name: pic || currentUser.company.pic,
      pic_telepon: phonePic || currentUser.company.pic_mobile,
      rfq_date: dateSubmit,
      type: 'komersil',
      sendCRM: true,
      service: carts,
    };

    if (itemDataKomersil.length > 0) {
      saveRfqCrmAction(FormData, ricoData);
    }
  };

  useEffect(() => {
    if (itemDataKomersil.length > 0) {
      filterSubmitKomersil();
    }
  }, [itemDataKomersil.length > 0]);

  const removeService = (id, status) => {
    if (status === 'Klasifikasi') {
      removeServicesAction(id, status);
    } else if (status === 'Komersil') {
      removeServicesAction(id, status);
    }
  };

  return (
    <>
      <CommonHeader
        title="Selected Service"
        onBackPressed={() => navigation.goBack()}
      />
      <View style={{ flex: 1, backgroundColor: COLOR_CONSTANT.WHITE }}>
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
            paddingHorizontal: 0,
            paddingVertical: 0,
          }}
        >
          <View
            style={[
              {
                borderRadius: 5,
                marginHorizontal: 5,
                marginTop: 8,
                marginBottom: 8,
              },
              STYLE_CONSTANT.SHADOW2,
            ]}
          >
            <View
              style={{
                height: 60,
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 5,
                borderBottomWidth: 1,
                borderColor: COLOR_CONSTANT.GRAY,
              }}
            >
              <TabItem
                label="Layanan Klasifikasi"
                isActive={activeTab === 'class'}
                onPressed={() => handleTabChange('class')}
              />
              <TabItem
                label="Layanan Komersil"
                isActive={activeTab === 'statutory'}
                onPressed={() => handleTabChange('statutory')}
              />
            </View>
          </View>
          {(activeTab === 'class' && (
            <View
              style={{
                // flexDirection: 'row',
                // height: 100,
                marginHorizontal: 0,
                paddingHorizontal: 5,
              }}
            >
              <View
                style={[
                  {
                    flex: 1,
                    marginBottom: 10,
                    borderRadius: 5,
                  },
                  STYLE_CONSTANT.SHADOW2,
                ]}
              >
                <ItemService
                  selectedCart={selectedCartList}
                  vessel={vessel}
                  onSubmitKlasifikasi={onSubmitKlasifikasi}
                  setItemDataKlasifikasi={setItemDataKlasifikasi}
                  removeService={removeService}
                />
              </View>
              <View style={{ width: 16 }} />
              <View
                style={[
                  {
                    flex: 1,
                    marginBottom: 10,
                    borderRadius: 5,
                  },
                  STYLE_CONSTANT.SHADOW2,
                ]}
              >
                <UserForm
                  currentUser={currentUser}
                  unit={unit}
                  // filterSubmit={filterSubmit}
                  setOnSubmitKlasifikasi={setOnSubmitKlasifikasi}
                  setDateSubmit={setDateSubmit}
                  setCabangSubmit={setCabangSubmit}
                  setCabangSubmitRico={setCabangSubmitRico}
                  setAlamat={setAlamat}
                  setEmailPic={setEmailPic}
                  setPhonePic={setPhonePic}
                  setPic={setPic}
                  cabangSubmitRico={cabangSubmitRico}
                />
              </View>
            </View>
          )) ||
            (activeTab === 'statutory' && (
              <View
                style={{
                  // flexDirection: 'row',
                  // height: 100,
                  marginHorizontal: 0,
                  paddingHorizontal: 5,
                }}
              >
                <View
                  style={[
                    {
                      flex: 1,
                      marginBottom: 10,
                      borderRadius: 5,
                    },
                    STYLE_CONSTANT.SHADOW2,
                  ]}
                >
                  <ItemServiceKomersil
                    selectedCart={selectedCartList}
                    vessel={vessel}
                    onSubmitKomersil={onSubmitKomersil}
                    setItemDataKomersil={setItemDataKomersil}
                    removeService={removeService}
                  />
                </View>
                <View style={{ width: 16 }} />
                <View
                  style={[
                    {
                      flex: 1,
                      marginBottom: 10,
                      borderRadius: 5,
                    },
                    STYLE_CONSTANT.SHADOW2,
                  ]}
                >
                  <UserFormKomersil
                    currentUser={currentUser}
                    unit={unit}
                    // filterSubmit={filterSubmit}
                    setOnSubmitKomersil={setOnSubmitKomersil}
                    setDateSubmit={setDateSubmit}
                    setCabangSubmit={setCabangSubmit}
                    setCabangSubmitRico={setCabangSubmitRico}
                    setAlamat={setAlamat}
                    setEmailPic={setEmailPic}
                    setPhonePic={setPhonePic}
                    setPic={setPic}
                    cabangSubmitRico={cabangSubmitRico}
                  />
                </View>
              </View>
            ))}
        </ScrollView>
      </View>
    </>
  );
};

const mapStateToProps = ({ servicesApp, authApp }) => {
  const { unit, vessel, rfqCRM } = servicesApp;
  const { currentUser } = authApp;

  return {
    selectedCartList: servicesApp.selectedCart,
    currentUser,
    unit,
    vessel,
    rfqCRM,
  };
};

export default connect(mapStateToProps, {
  loadUnitAction: loadUnit,
  loadVesselAction: loadVessel,
  saveRfqCrmAction: saveRfqCrm,
  clearCartAction: clearCart,
  removeServicesAction: removeServices,
})(CartDetail);
