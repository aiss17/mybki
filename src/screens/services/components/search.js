import React from 'react';
import { TextInput, View } from 'react-native';
// import BannerSvg from '../../assets/sample/banner.svg';
import SearchSvg from '../../../assets/icons/search.svg';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';

const Search = () => {
  return (
    <View
      style={[
        {
          borderWidth: 1,
          borderColor: COLOR_CONSTANT.GRAY,
          flexDirection: 'row',
          paddingVertical: 8,
          paddingHorizontal: 8,
          borderRadius: 10,
          alignItems: 'center',
          flexWrap: 'wrap',
        },
        STYLE_CONSTANT.SHADOW2,
      ]}
    >
      <SearchSvg style={{ marginHorizontal: 12 }} />
      <TextInput
        placeholder="Search Services…"
        style={{
          fontFamily: 'Montserrat_400Regular',
          fontSize: 14,
        }}
      />
    </View>
  );
};

export default Search;
