/* eslint-disable eqeqeq */
import React, { useEffect, useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { Col, Row } from 'react-native-responsive-grid-system';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';
import LoadingView from '../../../components/LoadingView';

// @ts-ignore
const dummy = require('../../../assets/sample/media1.png');

const MostOrderedServices = ({
  navigation,
  list,
  selectedServicesAction,
  setCount,
  setCartCountKlasifikasi,
  cartCountKlasifikasi,
  setCartCountKomersil,
  cartCountKomersil,
}) => {
  const { data, loading } = list;
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);

  const addCartKlasifikasi = (item) => {
    const numberId = [item];
    let number = [];
    if (cartCountKlasifikasi.length > 0) {
      number = [...cartCountKlasifikasi];
      numberId.forEach((p) => {
        const obj = {
          benefit: p.benefit,
          code: p.code,
          deliverable: p.deliverable,
          description: p.description,
          group_id: p.group_id,
          id: p.id,
          is_order: p.is_order,
          is_periodik: p.is_periodik,
          is_vessel: p.is_vessel,
          items_group_id: p.items_group_id,
          items_id: p.items_id,
          jasa_bki: p.jasa_bki,
          jasa_id: p.jasa_id,
          keyword: p.keyword,
          long_description: p.long_description,
          location: '-',
          label: '-',
          material_code: p.material_code,
          name: p.name,
          note: p.note,
          portofolio_id: p.portofolio_id,
          rate: p.rate,
          rate_currency_1: p.rate_currency_1,
          requirement: p.requirement,
          sap_code: p.sap_code,
          scope: p.scope,
          sector: p.sector,
          sertificate_sign: p.sertificate_sign,
          service_type: p.service_type,
          seven_bki_service: p.seven_bki_service,
          sla: p.sla,
          sub_portofolio_id: p.sub_portofolio_id,
          sub_service: p.sub_service,
          tax: p.tax,
          tax2: p.tax2,
          unit: p.unit,
          value: p.items_id,
          asset: {
            label: '-',
            value: '-',
          },
          vessel: {
            vessel_code: '-',
            vessel_name: '-',
          },
        };
        const isExist = cartCountKlasifikasi.some(
          (w) => w.items_id == obj.items_id
        );

        if (!isExist) {
          number.push(obj);
        } else if (isExist) {
          number = number.filter((q) => q.items_id !== obj.items_id);
        }
      });
    } else {
      number = numberId.map((q) => {
        return {
          benefit: q.benefit,
          code: q.code,
          deliverable: q.deliverable,
          description: q.description,
          group_id: q.group_id,
          id: q.id,
          is_order: q.is_order,
          is_periodik: q.is_periodik,
          is_vessel: q.is_vessel,
          items_group_id: q.items_group_id,
          items_id: q.items_id,
          jasa_bki: q.jasa_bki,
          jasa_id: q.jasa_id,
          keyword: q.keyword,
          long_description: q.long_description,
          location: '-',
          label: '-',
          material_code: q.material_code,
          name: q.name,
          note: q.note,
          portofolio_id: q.portofolio_id,
          rate: q.rate,
          rate_currency_1: q.rate_currency_1,
          requirement: q.requirement,
          sap_code: q.sap_code,
          scope: q.scope,
          sector: q.sector,
          sertificate_sign: q.sertificate_sign,
          service_type: q.service_type,
          seven_bki_service: q.seven_bki_service,
          sla: q.sla,
          sub_portofolio_id: q.sub_portofolio_id,
          sub_service: q.sub_service,
          tax: q.tax,
          tax2: q.tax2,
          unit: q.unit,
          value: q.items_id,
          asset: {
            label: '-',
            value: '-',
          },
          vessel: {
            vessel_code: '-',
            vessel_name: '-',
          },
        };
      });
    }
    setCartCountKlasifikasi(number);
  };

  const addCartKomersil = (item) => {
    const numberId = [item];
    let number = [];
    if (cartCountKomersil.length > 0) {
      number = [...cartCountKomersil];
      numberId.forEach((p) => {
        const obj = {
          benefit: p.benefit,
          code: p.code,
          deliverable: p.deliverable,
          description: p.description,
          group_id: p.group_id,
          id: p.id,
          is_order: p.is_order,
          is_periodik: p.is_periodik,
          is_vessel: p.is_vessel,
          items_group_id: p.items_group_id,
          items_id: p.items_id,
          jasa_bki: p.jasa_bki,
          jasa_id: p.jasa_id,
          keyword: p.keyword,
          long_description: p.long_description,
          location: '-',
          label: '-',
          material_code: p.material_code,
          name: p.name,
          note: p.note,
          portofolio_id: p.portofolio_id,
          rate: p.rate,
          rate_currency_1: p.rate_currency_1,
          requirement: p.requirement,
          sap_code: p.sap_code,
          scope: p.scope,
          sector: p.sector,
          sertificate_sign: p.sertificate_sign,
          service_type: p.service_type,
          seven_bki_service: p.seven_bki_service,
          sla: p.sla,
          sub_portofolio_id: p.sub_portofolio_id,
          sub_service: p.sub_service,
          tax: p.tax,
          tax2: p.tax2,
          unit: p.unit,
          value: p.items_id,
          asset: {
            label: '-',
            value: '-',
          },
          vessel: {
            vessel_code: '-',
            vessel_name: '-',
          },
        };
        const isExist = cartCountKomersil.some(
          (w) => w.items_id == obj.items_id
        );

        if (!isExist) {
          number.push(obj);
        } else if (isExist) {
          number = number.filter((q) => q.items_id !== obj.items_id);
        }
      });
    } else {
      number = numberId.map((q) => {
        return {
          benefit: q.benefit,
          code: q.code,
          deliverable: q.deliverable,
          description: q.description,
          group_id: q.group_id,
          id: q.id,
          is_order: q.is_order,
          is_periodik: q.is_periodik,
          is_vessel: q.is_vessel,
          items_group_id: q.items_group_id,
          items_id: q.items_id,
          jasa_bki: q.jasa_bki,
          jasa_id: q.jasa_id,
          keyword: q.keyword,
          long_description: q.long_description,
          location: '-',
          label: '-',
          material_code: q.material_code,
          name: q.name,
          note: q.note,
          portofolio_id: q.portofolio_id,
          rate: q.rate,
          rate_currency_1: q.rate_currency_1,
          requirement: q.requirement,
          sap_code: q.sap_code,
          scope: q.scope,
          sector: q.sector,
          sertificate_sign: q.sertificate_sign,
          service_type: q.service_type,
          seven_bki_service: q.seven_bki_service,
          sla: q.sla,
          sub_portofolio_id: q.sub_portofolio_id,
          sub_service: q.sub_service,
          tax: q.tax,
          tax2: q.tax2,
          unit: q.unit,
          value: q.items_id,
          asset: {
            label: '-',
            value: '-',
          },
          vessel: {
            vessel_code: '-',
            vessel_name: '-',
          },
        };
      });
    }
    setCartCountKomersil(number);
  };

  useEffect(() => {
    setCount(cartCountKlasifikasi.length + cartCountKomersil.length);
  }, [cartCountKlasifikasi, cartCountKomersil]);

  return (
    <>
      <View style={{ marginBottom: 12 }}>
        <Text
          style={{
            fontFamily: 'Montserrat_700Bold',
            fontSize: 20,
            color: COLOR_CONSTANT.DARKBLUE,
          }}
        >
          Most Ordered Services
        </Text>
      </View>

      <View style={{ paddingVertical: 4, paddingRight: 8 }}>
        <Row>
          {data == null && (
            <View
              style={{
                padding: 24,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <LoadingView />
            </View>
          )}
          {data && (
            <>
              {data.slice(page, limit).map((t, i) => {
                return (
                  <Col
                    xs={6}
                    sm={6}
                    md={6}
                    lg={6}
                    key={`${t.id}`.concat(`${i}`)}
                  >
                    <View
                      style={[
                        {
                          borderRadius: 5,
                          marginBottom: 12,
                        },
                        STYLE_CONSTANT.SHADOW2,
                      ]}
                    >
                      <View
                        style={{
                          height: 172,
                          width: '100%',
                          overflow: 'hidden',
                          borderRadius: 5,
                          backgroundColor: 'white',
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            selectedServicesAction(t);
                            navigation.navigate('Service', {
                              screen: 'ServiceDetail',
                            });
                          }}
                        >
                          <View style={{ backgroundColor: 'blue', height: 96 }}>
                            <Image
                              source={dummy}
                              resizeMode="stretch"
                              style={{ height: '100%', width: '100%' }}
                            />
                          </View>
                        </TouchableOpacity>
                        <View style={{ marginLeft: 8 }}>
                          <Text
                            style={{
                              fontFamily: 'Montserrat_700Bold',
                              fontSize: 12,
                              color: COLOR_CONSTANT.BLACK,
                              marginTop: 6,
                            }}
                          >
                            {t.description.substring(0, 17).concat('...')}
                          </Text>
                          <Text
                            style={{
                              fontFamily: 'Montserrat_400Regular',
                              fontSize: 9,
                              color: COLOR_CONSTANT.BLACK,
                              marginVertical: 6,
                            }}
                          >
                            {t.long_description.substring(0, 25).concat('...')}
                          </Text>
                          <View
                            style={{
                              backgroundColor:
                                cartCountKlasifikasi.some(
                                  (q) => q.items_id == t.items_id
                                ) ||
                                cartCountKomersil.some(
                                  (q) => q.items_id == t.items_id
                                )
                                  ? COLOR_CONSTANT.CYAN
                                  : COLOR_CONSTANT.DARKORANGE,
                              width: 82,
                              height: 21,
                              borderRadius: 14,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                if (t.name === 'Klasifikasi') {
                                  addCartKlasifikasi(t);
                                } else if (t.name === 'Komersil') {
                                  addCartKomersil(t);
                                }
                              }}
                              key={t.id}
                            >
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_700Bold',
                                  fontSize: 8,
                                  color: COLOR_CONSTANT.WHITE,
                                }}
                              >
                                {cartCountKlasifikasi.some(
                                  (q) => q.items_id == t.items_id
                                ) ||
                                cartCountKomersil.some(
                                  (q) => q.items_id == t.items_id
                                )
                                  ? 'Added'
                                  : '+ Add Services'}
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </View>
                  </Col>
                );
              })}
              {loading ? (
                <View
                  style={{
                    padding: 24,
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}
                >
                  <LoadingView />
                </View>
              ) : (
                <>
                  <View
                    style={{
                      padding: 24,
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        setPage(page + 0);
                        setLimit(limit + 10);
                      }}
                    >
                      <Text
                        style={{
                          color: 'gray',
                          fontSize: 16,
                        }}
                      >
                        Load More
                      </Text>
                    </TouchableOpacity>
                  </View>
                </>
              )}
            </>
          )}
        </Row>
      </View>
    </>
  );
};

export default MostOrderedServices;
