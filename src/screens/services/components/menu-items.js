import React from 'react';
import { Text, View } from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';
// import BannerSvg from '../../assets/sample/banner.svg';
import ShipSvg from '../../../assets/icons/3d-ship.svg';
import AllServiceSvg from '../../../assets/icons/all-service.svg';
import CertificationSvg from '../../../assets/icons/certificate.svg';
import ConsultancySvg from '../../../assets/icons/consultancy.svg';
import InspectionSvg from '../../../assets/icons/inspection.svg';
import StatutorySvg from '../../../assets/icons/statutory.svg';
import TrainingSvg from '../../../assets/icons/training.svg';
import WrenchSvg from '../../../assets/icons/wrench-and-hammer.svg';
import { COLOR_CONSTANT } from '../../../constants/style-constant';

const MenuItems = () => {
  const menuItem = (label, Avg) => {
    return (
      <View style={{ alignItems: 'center' }}>
        <View
          style={{
            backgroundColor: COLOR_CONSTANT.DARKORANGE,
            width: 72,
            height: 72,
            borderRadius: 36,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {Avg && <Avg style={{ color: 'white' }} />}
        </View>
        <Text
          style={{
            fontFamily: 'Montserrat_600SemiBold',
            fontSize: 11,
            color: COLOR_CONSTANT.DARKBLUE,
            marginVertical: 12,
          }}
        >
          {label}
        </Text>
      </View>
    );
  };
  return (
    <>
      <View style={{ marginBottom: 12 }}>
        <Text
          style={{
            fontFamily: 'Montserrat_700Bold',
            fontSize: 20,
            color: COLOR_CONSTANT.DARKBLUE,
          }}
        >
          Services Category
        </Text>
      </View>
      <Grid>
        <Row>
          <Col>{menuItem('Testing', WrenchSvg)}</Col>
          <Col>{menuItem('Inspection', InspectionSvg)}</Col>
          <Col>{menuItem('Consultancy', ConsultancySvg)}</Col>
          <Col>{menuItem('Training', TrainingSvg)}</Col>
        </Row>
        <Row>
          <Col>{menuItem('Certification', CertificationSvg)}</Col>
          <Col>{menuItem('Classification', ShipSvg)}</Col>
          <Col>{menuItem('Statutory', StatutorySvg)}</Col>
          <Col>{menuItem('All Services', AllServiceSvg)}</Col>
        </Row>
      </Grid>
    </>
  );
};

export default MenuItems;
