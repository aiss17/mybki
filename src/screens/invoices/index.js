// @ts-nocheck
import React, { useEffect } from 'react';
import { SearchBar } from 'react-native-elements';
import { useActionSheet } from '@expo/react-native-action-sheet';
import { connect } from 'react-redux';
import {
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
// import { withNavigation } from 'react-navigation';
import CommonHeader from '../../components/common/common-header';
// import { COLOR_CONSTANT } from '../../constants/style-constant';
import ListItem from './components/list-item';
import { loadInvoices } from '../../redux/invoice/action';
import LoadingView from '../../components/LoadingView';

const Invoices = ({ navigation, loadInvoicesAction, list, currentUser }) => {
  const { data, page, loading } = list;
  const { showActionSheetWithOptions } = useActionSheet();
  useEffect(() => {
    loadInvoicesAction(currentUser?.company_id);
  }, [loadInvoicesAction]);

  return (
    <>
      <CommonHeader
        title="Invoices"
        onBackPressed={() => navigation.goBack()}
      />
      <SearchBar
        inputContainerStyle={{
          backgroundColor: '#E5E5E5',
          borderColor: '#8E8E8E',
          borderWidth: 1,
        }}
        round
        containerStyle={{
          backgroundColor: 'white',
        }}
        searchIcon={{ size: 24 }}
        placeholder="Type invoice number or order number..."
        style={{ fontSize: 14, fontStyle: 'italic' }}
      />
      <ScrollView
        style={{ paddingTop: 5 }}
        refreshControl={
          <RefreshControl
            refreshing={data == null && loading}
            onRefresh={() => loadInvoicesAction(currentUser?.company_id)}
          />
        }
      >
        {data && (
          <>
            {data.map((t) => {
              return (
                <ListItem
                  showActionSheetWithOptions={showActionSheetWithOptions}
                  key={`${t.invoice_id}`}
                  data={t}
                />
              );
            })}
            {loading ? (
              <View
                style={{
                  padding: 24,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
              >
                <LoadingView />
              </View>
            ) : (
              <>
                {page.currentPage < page.totalPage && (
                  <View
                    style={{
                      padding: 24,
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        loadInvoicesAction({
                          selectedPage: page.currentPage + 1,
                        })
                      }
                    >
                      <Text
                        style={{
                          color: 'gray',
                          fontSize: 16,
                        }}
                      >
                        Load More
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </>
            )}
          </>
        )}
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ invoiceApp, authApp }) => {
  const { list } = invoiceApp;
  const { currentUser } = authApp;

  return {
    list,
    currentUser,
  };
};

export default connect(mapStateToProps, {
  loadInvoicesAction: loadInvoices,
})(Invoices);
