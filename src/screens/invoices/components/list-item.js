/* eslint-disable no-nested-ternary */
import moment from 'moment';
import React from 'react';
// import moment from 'moment';

import { Text, View } from 'react-native';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';

const ListItem = ({ data }) => {
  const checkSwitch = (param) => {
    switch (param) {
      case '1':
        return 'Unpaid';
      case '2':
        return 'Paid';
      case '3':
        return 'Partially';
      case '4':
        return 'Overdue';
      case '5':
        return 'Cancelled';
      case '6':
        return 'Draft';
      case '7':
        return 'Approved';
      default:
        return 'Rejected';
    }
  };
  const getBackgrounColor = (param) => {
    switch (param) {
      case '1':
        return COLOR_CONSTANT.RED;
      case '2':
        return COLOR_CONSTANT.GREEN;
      case '3':
        return COLOR_CONSTANT.DARKORANGE;
      case '4':
        return COLOR_CONSTANT.RED;
      case '5':
        return COLOR_CONSTANT.RED;
      case '6':
        return COLOR_CONSTANT.LIGHTGRAY;
      case '7':
        return COLOR_CONSTANT.DARKBLUE;
      default:
        return COLOR_CONSTANT.RED;
    }
  };
  return (
    <>
      {data && (
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 5,
              marginLeft: 5,
              marginRight: 5,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.WHITE,
              height: 100,
              borderRadius: 5,
              overflow: 'hidden',
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <View
              style={{
                width: '20%',
                backgroundColor: COLOR_CONSTANT.WHITE,
                flexDirection: 'column',
                margin: 0,
              }}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Text
                  style={{
                    color: COLOR_CONSTANT.DARKBLUE,
                    fontFamily: 'Montserrat_700Bold',
                    fontSize: 13,
                  }}
                >
                  {moment(new Date(data.tgl_jatuh_tempo)).format('DD MMM')}
                </Text>
                <Text
                  style={{
                    color: COLOR_CONSTANT.DARKBLUE,
                    fontFamily: 'Montserrat_700Bold',
                    fontSize: 18,
                  }}
                >
                  {moment(new Date(data.tgl_jatuh_tempo)).format('YYYY')}
                </Text>
              </View>
              <View style={{ alignItems: 'center', marginBottom: 11 }}>
                <Text
                  style={{
                    color: COLOR_CONSTANT.DARKBLUE,
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 9,
                  }}
                >
                  Due Date
                </Text>
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', margin: 4 }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 4,
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Invoice Number
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 10,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {data.nomor_invoice_no == null
                      ? '-'
                      : data.nomor_invoice_no}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Order Number
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 10,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {data.no_order == null ? '-' : data.no_order}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Total Amount
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 12,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {data.nilai_kontrak == null
                      ? '-'
                      : 'Rp '.concat(
                          parseInt(data.nilai_kontrak, 10)
                            .toFixed(0)
                            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                        )}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Receivable Age
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 10,
                      color: COLOR_CONSTANT.DARKORANGE,
                    }}
                  >
                    {data.umur_piutang == null
                      ? '-'
                      : parseInt(data.umur_piutang, 10)
                          .toFixed(0)
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                          .concat(' days')}
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', marginBottom: 4 }}>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Status
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: getBackgrounColor(data.status),
                    borderRadius: 12,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 0,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      textAlign: 'right',
                      fontSize: 10,
                      marginLeft: 10,
                      marginRight: 10,
                      color: COLOR_CONSTANT.WHITE,
                    }}
                  >
                    {checkSwitch(data.status)}
                  </Text>
                </View>
              </View>
            </View>
            {/* <View style={{ flex: 1, flexDirection: 'row', marginLeft: 12 }}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                }}
              >
                <Text
                  style={{
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 12,
                    marginVertical: 4,
                  }}
                >
                  Invoice No :{' '}
                  <Text style={{ fontFamily: 'Montserrat_700Bold' }}>
                    {data.nomor_invoice_no == null
                      ? '-'
                      : data.nomor_invoice_no}
                  </Text>
                </Text>
                <Text
                  style={{
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 12,
                    marginVertical: 4,
                  }}
                >
                  Order No :{' '}
                  <Text style={{ fontFamily: 'Montserrat_700Bold' }}>
                    {data.no_order == null ? '-' : data.no_order}
                  </Text>
                </Text>
                <Text
                  style={{
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 12,
                    marginVertical: 4,
                  }}
                >
                  Value :{' '}
                  <Text style={{ fontFamily: 'Montserrat_700Bold' }}>
                    {data.nilai_kontrak == null
                      ? '-'
                      : 'Rp '.concat(
                          parseInt(data.nilai_kontrak, 10)
                            .toFixed(0)
                            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                        )}
                  </Text>
                </Text>
                <Text
                  style={{
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 12,
                    marginVertical: 4,
                  }}
                >
                  Receivable Age :{' '}
                  <Text style={{ fontFamily: 'Montserrat_700Bold' }}>
                    {data.umur_piutang == null
                      ? '-'
                      : parseInt(data.umur_piutang, 10)
                          .toFixed(0)
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                          .concat(' days')}
                  </Text>
                </Text>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  // justifyContent: 'flex-start',
                  alignItems: 'flex-end',
                  marginRight: 8,
                  marginTop: 4,
                  width: 120,
                }}
              >
                <View
                  style={{
                    backgroundColor: getBackgrounColor(data.status),
                    borderRadius: 12,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 0,
                    width: 60,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      color: COLOR_CONSTANT.WHITE,
                      fontSize: 9,
                      paddingVertical: 7,
                    }}
                  >
                    {checkSwitch(data.status)}
                  </Text>
                </View>
              </View>
            </View> */}
          </View>
        </View>
      )}
    </>
  );
};

export default ListItem;
