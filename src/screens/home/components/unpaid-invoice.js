// @ts-nocheck
import React from 'react';
import moment from 'moment';
import { Text, View, TouchableOpacity } from 'react-native';
import CopySvg from '../../../assets/icons/copy.svg';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';
import LoadingView from '../../../components/LoadingView';

const UnpaidInvoice = ({ navigation, invoice }) => {
  const { data, loading } = invoice;

  const splice = data?.splice(0, 3);

  const card = (id, text1, text2, Icon) => {
    return (
      <View
        key={id}
        style={[
          {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: COLOR_CONSTANT.WHITE,
            paddingVertical: 8,
            paddingRight: 6,
            paddingLeft: 12,
            marginBottom: 12,
            flexWrap: 'wrap',
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
        <View>
          <View style={{ flexDirection: 'row' }}>
            <Text
              style={{
                fontFamily: 'Montserrat_700Bold',
                fontSize: 14,
                color: COLOR_CONSTANT.BLACK,
                marginRight: 8,
              }}
            >
              {text1}
            </Text>
            {Icon && <Icon />}
          </View>
          <Text
            style={{
              fontFamily: 'Montserrat_400Regular',
              fontSize: 11,
              marginTop: 6,
              color: COLOR_CONSTANT.BLACK,
            }}
          >
            {text2}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={{ marginBottom: 12 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 20,
              color: COLOR_CONSTANT.DARKBLUE,
            }}
          >
            Unpaid Invoice
          </Text>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Invoices')}>
          <View>
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                fontSize: 11,
                color: COLOR_CONSTANT.DARKBLUE,
              }}
            >
              See All
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {data && (
        <>
          {loading ? (
            <View
              style={{
                padding: 24,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <LoadingView />
            </View>
          ) : (
            <>
              {splice.map((q) => {
                return card(
                  q.invoice_id,
                  `${q.nomor_invoice_no == null ? '-' : q.nomor_invoice_no}`,
                  <Text>
                    IDR{' '}
                    {parseInt(q.nilai_kontrak, 10)
                      .toString()
                      .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}
                    | Due Date:{' '}
                    <Text style={{ color: COLOR_CONSTANT.RED }}>
                      {moment(q.tgl_jatuh_tempo).format('DD MMMM yyyy')}
                    </Text>
                  </Text>,
                  CopySvg
                );
              })}
            </>
          )}
        </>
      )}
    </>
  );
};

export default UnpaidInvoice;
