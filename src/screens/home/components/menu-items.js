/* eslint-disable eqeqeq */
// @ts-nocheck
import React, { useState } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Modal,
  Pressable,
  StyleSheet,
} from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';
import { withNavigation } from 'react-navigation';
// import BannerSvg from '../../assets/sample/banner.svg';
import BuildingSvg from '../../../assets/icons/building.svg';
import CartSvg from '../../../assets/icons/cart.svg';
import CertificateSvg from '../../../assets/icons/certificate.svg';
import InvoiceSvg from '../../../assets/icons/invoice.svg';
import PaymentSvg from '../../../assets/icons/payment-method.svg';
import RateSvg from '../../../assets/icons/rate.svg';
import UserSvg from '../../../assets/icons/user.svg';
import VesselSvg from '../../../assets/icons/vessel.svg';
import SurveySvg from '../../../assets/icons/survey.svg';
import CloseSvg from '../../../assets/icons/close.svg';
import { COLOR_CONSTANT } from '../../../constants/style-constant';

const MenuItems = ({ navigation, surveyCrm, getKepuasan }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0, 0.6)',
    },
    modalView: {
      margin: 5,
      backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
      borderRadius: 20,
      padding: 25,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
    },
    buttonCancel: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
      marginRight: 20,
      width: 80,
    },
    buttonSubmit: {
      borderRadius: 5,
      padding: 10,
      // elevation: 2,
      width: '100%',
    },
    buttonClose: {
      backgroundColor: '#0073C3',
    },
    buttonModalClose: {
      backgroundColor: COLOR_CONSTANT.RED,
    },
    textStyle: {
      color: 'white',
      fontWeight: '500',
      textAlign: 'center',
      fontSize: 20,
    },
    modalText: {
      marginBottom: 15,
      textAlign: 'center',
    },
  });

  const menuItem = (label, Avg, route) => {
    return (
      <TouchableOpacity
        onPress={route ? () => navigation.navigate(route) : () => {}}
      >
        <View style={{ alignItems: 'center' }}>
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.ORANGE,
              width: 72,
              height: 72,
              borderRadius: 36,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {Avg && <Avg style={{ color: 'white' }} />}
          </View>
          <Text
            style={{
              fontFamily: 'Montserrat_600SemiBold',
              fontSize: 11,
              color: COLOR_CONSTANT.DARKBLUE,
              marginVertical: 12,
            }}
          >
            {label}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const satisfactionItem = (label, Avg) => {
    return (
      <TouchableOpacity
        onPress={() => {
          getKepuasan(
            surveyCrm &&
              surveyCrm.data &&
              surveyCrm.data[0] &&
              surveyCrm.data[0].surveyid
          );
          setModalVisible(!modalVisible);
        }}
      >
        <View style={{ alignItems: 'center' }}>
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.ORANGE,
              width: 72,
              height: 72,
              borderRadius: 36,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {Avg && <Avg style={{ color: 'white' }} />}
          </View>
          <Text
            style={{
              fontFamily: 'Montserrat_600SemiBold',
              fontSize: 11,
              color: COLOR_CONSTANT.DARKBLUE,
              marginVertical: 12,
            }}
          >
            {label}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Grid>
      <Row>
        <Col>{menuItem('Vessels', VesselSvg, 'Vessels')}</Col>
        <Col>{menuItem('Certificates', CertificateSvg, 'Certificates')}</Col>
        <Col>{menuItem('Invoice', InvoiceSvg, 'Invoices')}</Col>
        <Col>{menuItem('Payment', PaymentSvg, 'Payments')}</Col>
      </Row>
      <Row>
        <Col>{menuItem('Orders', CartSvg, 'Orders')}</Col>
        <Col>{menuItem('Quotation', InvoiceSvg, 'Quotation')}</Col>
        <Col>{menuItem('Company', BuildingSvg, 'MyCompany')}</Col>
        <Col>{menuItem('Profile', UserSvg, 'Profile')}</Col>
      </Row>
      <Row>
        {surveyCrm?.data != false && (
          <Col>{satisfactionItem('Satisfaction', RateSvg)}</Col>
        )}

        <Modal
          animationType="slide"
          transparent
          visible={modalVisible}
          backgroundColor={COLOR_CONSTANT.LIGHTGRAY}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              {CloseSvg && (
                <View
                  style={{
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      setModalVisible(!modalVisible);
                    }}
                  >
                    <CloseSvg
                      style={{
                        color: 'white',
                      }}
                    />
                  </TouchableOpacity>
                </View>
              )}
              {SurveySvg && (
                <View
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                  <SurveySvg style={{ color: 'white' }} />
                </View>
              )}

              <View
                style={{
                  marginBottom: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 20,
                    fontWeight: '500',
                    marginBottom: 5,
                    marginTop: 24,
                  }}
                >
                  Survey Kepuasan
                </Text>
              </View>

              <View
                style={{
                  marginBottom: 10,
                }}
              >
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 14,
                    fontWeight: '400',
                    marginBottom: 5,
                    marginTop: 10,
                    textAlign: 'center',
                  }}
                >
                  Survey ini diadakan untuk mengetahui tingkat kepuasan pengguna
                  myBKI
                </Text>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 14,
                    fontWeight: '400',
                    marginBottom: 5,
                    marginTop: 10,
                    textAlign: 'center',
                  }}
                >
                  Apakah Anda bersedia untuk ikut serta dalam survey ini?
                </Text>
              </View>
              <View
                style={{
                  marginTop: 20,
                }}
              >
                <Pressable
                  style={[styles.buttonSubmit, styles.buttonClose]}
                  onPress={
                    'Satisfaction'
                      ? () => navigation.navigate('Satisfaction')
                      : () => {}
                  }
                >
                  <Text style={styles.textStyle}>Ikuti Survey</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </Modal>
      </Row>
    </Grid>
  );
};

export default withNavigation(MenuItems);
