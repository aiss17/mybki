import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';

const MoreInformation = () => {
  const card = () => {
    return (
      <View
        style={[
          {
            marginRight: 12,
            marginBottom: 32,
            borderRadius: 5,
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
        <View
          style={{
            width: 257,
            overflow: 'hidden',
            borderRadius: 5,
            backgroundColor: COLOR_CONSTANT.WHITE,
          }}
        >
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.DARKORANGE,
              height: 140,
              width: '100%',
            }}
          />
          <View style={{ padding: 12 }}>
            <Text
              style={{
                fontFamily: 'Montserrat_700Bold',
                fontSize: 20,
                color: COLOR_CONSTANT.BLACK,
              }}
            >
              Knowledge Base 1
            </Text>
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                fontSize: 16,
                marginTop: 6,
              }}
            >
              List of news about unknown information
            </Text>
          </View>
        </View>
      </View>
    );
  };
  return (
    <>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ marginBottom: 12 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 20,
              color: COLOR_CONSTANT.DARKBLUE,
            }}
          >
            More Information
          </Text>
        </View>
      </View>
      <ScrollView horizontal>
        {card()}
        {card()}
      </ScrollView>
    </>
  );
};

export default MoreInformation;
