/* eslint-disable no-nested-ternary */
import React from 'react';
import moment from 'moment';
import { Text, View, TouchableOpacity } from 'react-native';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';
import LoadingView from '../../../components/LoadingView';

const MyOrders = ({ navigation, list }) => {
  const { data, loading } = list;
  const splice = data?.splice(0, 3);

  const card = (id, text1, text2, text3, color) => {
    return (
      <View
        key={id}
        style={[
          {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: COLOR_CONSTANT.WHITE,
            paddingVertical: 8,
            paddingRight: 6,
            paddingLeft: 12,
            marginBottom: 12,
            flexWrap: 'wrap',
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
        <View>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 14,
              color: COLOR_CONSTANT.BLACK,
            }}
          >
            {text1}
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat_400Regular',
              fontSize: 11,
              marginTop: 6,
              color: COLOR_CONSTANT.BLACK,
            }}
          >
            {text2}
          </Text>
        </View>
        <View>
          <View
            style={{
              backgroundColor: color,
              width: 97,
              height: 27,
              borderRadius: 14,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_700Bold',
                fontSize: 12,
                color: COLOR_CONSTANT.WHITE,
              }}
            >
              {text3}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={{ marginBottom: 12 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_700Bold',
              fontSize: 20,
              color: COLOR_CONSTANT.DARKBLUE,
            }}
          >
            My Orders
          </Text>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Orders')}>
          <View>
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                fontSize: 11,
                color: COLOR_CONSTANT.DARKBLUE,
              }}
              onPress={() => navigation.navigate('Orders')}
            >
              See All
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {data && (
        <>
          {loading ? (
            <View
              style={{
                padding: 24,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <LoadingView />
            </View>
          ) : (
            <>
              {splice.map((q, i) => {
                return card(
                  i,
                  `${q.orderno == null ? '-' : q.orderno}`,
                  `${q.service_group} | order date: ${moment(q.tglorder).format(
                    'DD MMMM yyyy'
                  )}`,
                  `${
                    q.quot == null && q.orderno == null
                      ? 'RFQ'
                      : q.quot != null && q.orderno == null
                      ? 'Quotation'
                      : 'On Going'
                  }`,
                  q.quot == null && q.orderno == null
                    ? COLOR_CONSTANT.ORANGE
                    : q.quot != null && q.orderno == null
                    ? COLOR_CONSTANT.DARKBLUE
                    : COLOR_CONSTANT.CYAN
                );
              })}
            </>
          )}
        </>
      )}
    </>
  );
};

export default MyOrders;
