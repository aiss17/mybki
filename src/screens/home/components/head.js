// @ts-nocheck
import React from 'react';
import { Image, Text, View } from 'react-native';
import OptionsMenu from 'react-native-option-menu';
import { Avatar } from 'react-native-paper';
// import { useSafeAreaInsets } from 'react-native-safe-area-context';
// import BannerSvg from '../../assets/sample/banner.svg';
import { withNavigation } from 'react-navigation';
import { COLOR_CONSTANT } from '../../../constants/style-constant';

const logo = require('../../../assets/logos/logo-color.png');
const avatarImage = require('../../../assets/sample/profile.png');

const Head = ({ currentUser, navigation, logoutAction }) => {
  // const insets = useSafeAreaInsets();
  return (
    <>
      <View
        style={{
          height: 100,
          marginTop: '6%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Image
          source={logo}
          resizeMode="contain"
          style={{ height: '100%', width: '100%' }}
        />
        <View style={{ position: 'absolute', right: 15, top: 0 }}>
          <OptionsMenu
            customButton={
              <Avatar.Image
                size={44}
                source={
                  currentUser && currentUser.avatar
                    ? {
                        uri: currentUser.avatar,
                      }
                    : avatarImage
                }
              />
            }
            destructiveIndex={1}
            options={['Logout', 'Cancel']}
            actions={[() => logoutAction(navigation)]}
          />
        </View>
      </View>
      <View
        style={{
          height: 40,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text
          style={{
            color: COLOR_CONSTANT.DARKBLUE,
            fontSize: 16,
            fontFamily: 'Montserrat_400Regular',
          }}
        >
          Selamat Datang,{' '}
          <Text style={{ fontFamily: 'Montserrat_700Bold' }}>
            {currentUser
              ? `${currentUser.first_name} ${currentUser.last_name}`.trim()
              : ''}
          </Text>
          !
        </Text>
      </View>
    </>
  );
};

export default withNavigation(Head);
