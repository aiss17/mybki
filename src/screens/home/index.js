// @ts-nocheck
import React, { useEffect } from 'react';
import { Image, ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
// import BannerSvg from '../../assets/sample/banner.svg';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import * as auth from '../../redux/auth/actions';
import Head from './components/head';
import MenuItems from './components/menu-items';
import MoreInformation from './components/more-information';
import MyOrders from './components/my-orders';
import Search from './components/search';
import UnpaidInvoice from './components/unpaid-invoice';
import { loadOrders } from '../../redux/order/action';
import { loadInvoices } from '../../redux/invoice/action';
import {
  getSurveyidCrm,
  getSurveyKepuasanCrm,
} from '../../redux/satisfaction/action';

const banner = require('../../assets/sample/banner.png');

const Home = ({
  navigation,
  currentUser,
  logoutAction,
  loadOrdersAction,
  loadInvoicesAction,
  getSurveyidCrmAction,
  getSurveyKepuasanCrmAction,
  list,
  invoice,
  surveyCrm,
}) => {
  useEffect(() => {
    loadOrdersAction();
    loadInvoicesAction();
    getSurveyidCrmAction(currentUser?.company_id);
  }, [loadOrdersAction, loadInvoicesAction, getSurveyidCrmAction]);

  const getKepuasan = (id) => {
    getSurveyKepuasanCrmAction(id);
  };

  return (
    <>
      <View style={{ flex: 1, backgroundColor: COLOR_CONSTANT.WHITE }}>
        <ScrollView style={{ flex: 1 }}>
          <Head currentUser={currentUser} logoutAction={logoutAction} />
          <View
            style={{
              alignItems: 'center',
              height: 150,
              marginHorizontal: 8,
            }}
          >
            {/* <BannerSvg /> */}
            <Image
              source={banner}
              resizeMode="contain"
              style={{ height: '100%', width: '100%' }}
            />
          </View>
          <View style={{ height: 12 }} />
          <View
            style={{
              marginHorizontal: 12,
            }}
          >
            <Search />
          </View>
          <View style={{ marginTop: 16, marginHorizontal: 12 }}>
            <MenuItems surveyCrm={surveyCrm} getKepuasan={getKepuasan} />
          </View>
          <View
            style={{
              paddingVertical: 16,
              paddingHorizontal: 21,
              backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
            }}
          >
            <MyOrders list={list} navigation={navigation} />
          </View>
          <View
            style={{
              paddingBottom: 16,
              paddingHorizontal: 21,
              backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
            }}
          >
            <UnpaidInvoice invoice={invoice} navigation={navigation} />
          </View>
          <View
            style={{
              paddingBottom: 16,
              paddingHorizontal: 21,
              backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
            }}
          >
            <MoreInformation />
          </View>
        </ScrollView>
      </View>
    </>
  );
};
const mapStateToProps = ({
  authApp,
  satisfactionApp,
  orderApp,
  invoiceApp,
}) => {
  const { currentUser } = authApp;
  const { list } = orderApp;
  const { surveyCrm } = satisfactionApp;

  return {
    currentUser,
    list,
    surveyCrm,
    invoice: invoiceApp.list,
  };
};
export default connect(mapStateToProps, {
  logoutAction: auth.logoutUser,
  loadOrdersAction: loadOrders,
  loadInvoicesAction: loadInvoices,
  getSurveyidCrmAction: getSurveyidCrm,
  getSurveyKepuasanCrmAction: getSurveyKepuasanCrm,
})(Home);
