/* eslint-disable eqeqeq */
import React from 'react';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT } from '../../../constants/style-constant';

const DetailsItem = ({ data }) => {
  return (
    <>
      <View
        style={{
          padding: 10,
          margin: 10,
          backgroundColor: COLOR_CONSTANT.WHITE,
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            margin: 10,
          }}
        >
          <View>
            <Text
              style={{
                fontFamily: 'Montserrat_500Medium',
                fontSize: 14,
                fontWeight: '500',
              }}
            >
              Currency
            </Text>
            <View
              style={{
                backgroundColor: '#E9E9E9',
                minWidth: 140,
                minHeight: 48,
                marginRight: 20,
                marginTop: 5,
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_400Regular',
                  color: '#111215',
                  fontSize: 16,
                  padding: 15,
                }}
              >
                {data?.currency}
              </Text>
            </View>
          </View>
          <View>
            <Text
              style={{
                fontFamily: 'Montserrat_500Medium',
                fontSize: 14,
                fontWeight: '500',
              }}
            >
              Code
            </Text>
            <View
              style={{
                backgroundColor: '#E9E9E9',
                minWidth: 140,
                minHeight: 48,
                marginTop: 5,
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_400Regular',
                  color: '#111215',
                  fontSize: 16,
                  padding: 15,
                }}
              >
                {data?.code}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Subsidiary
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.is_subsidiary === 'f' ? 'FALSE' : 'TRUE'}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            VAT Number
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.vat}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Default Language
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.default_language == 'indonesia' ? 'Indonesia' : 'English'}
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};
export default DetailsItem;
