/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
import React from 'react';
import moment from 'moment';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT } from '../../../constants/style-constant';

const CompanyItem = ({ data }) => {
  return (
    <>
      <View
        style={{
          padding: 10,
          margin: 10,
          backgroundColor: COLOR_CONSTANT.WHITE,
        }}
      >
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Type
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.customer_type}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Priority
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.priority_company == '1' ? 'Premium' : 'Non Premium'}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Class
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.company_class == 1
                ? 'Pemerintah'
                : data?.company_class == 2
                ? 'BUMN'
                : data?.company_class == 3
                ? 'Swasta'
                : data?.company_class == 4
                ? 'Perorangan'
                : ''}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Receive Message Via
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.message_received_with == 1
                ? 'Whatsapp'
                : data?.company_class == 2
                ? 'Email'
                : data?.company_class == 3
                ? 'Both'
                : ''}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            margin: 10,
          }}
        >
          <View>
            <Text
              style={{
                fontFamily: 'Montserrat_500Medium',
                fontSize: 14,
                fontWeight: '500',
              }}
            >
              Establish Date
            </Text>
            <View
              style={{
                backgroundColor: '#E9E9E9',
                minWidth: 140,
                minHeight: 48,
                marginRight: 20,
                marginTop: 5,
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_400Regular',
                  color: '#111215',
                  fontSize: 16,
                  padding: 15,
                }}
              >
                {moment(data?.establishment_date).format('DD MMM yyyy')}
              </Text>
            </View>
          </View>
          <View>
            <Text
              style={{
                fontFamily: 'Montserrat_500Medium',
                fontSize: 14,
                fontWeight: '500',
              }}
            >
              CEO Birth Date
            </Text>
            <View
              style={{
                backgroundColor: '#E9E9E9',
                minWidth: 140,
                minHeight: 48,
                marginTop: 5,
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_400Regular',
                  color: '#111215',
                  fontSize: 16,
                  padding: 15,
                }}
              >
                {moment(data?.ceo_dateofbirth).format('DD MMM yyyy')}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};
export default CompanyItem;
