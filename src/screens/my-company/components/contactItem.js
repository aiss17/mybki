import React from 'react';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT } from '../../../constants/style-constant';

const ContactItem = ({ data }) => {
  return (
    <>
      <View
        style={{
          padding: 10,
          margin: 10,
          backgroundColor: COLOR_CONSTANT.WHITE,
        }}
      >
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Phone No.
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.phonenumber}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Whatsapp No.
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.wa_number}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Website
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.website}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Address
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.address}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            City
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.city}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            State
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.state}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Country
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.country}
            </Text>
          </View>
        </View>
        <View style={{ margin: 10 }}>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '500',
            }}
          >
            Zip Code
          </Text>
          <View
            style={{
              backgroundColor: '#E9E9E9',
              minWidth: 140,
              minHeight: 48,
              marginTop: 5,
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                color: '#111215',
                fontSize: 16,
                padding: 15,
              }}
            >
              {data?.zip}
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};
export default ContactItem;
