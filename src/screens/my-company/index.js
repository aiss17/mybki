/* eslint-disable eqeqeq */
// @ts-nocheck
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import CompanySvg from '../../assets/icons/company.svg';
import CommonHeader from '../../components/common/common-header';
// import { COLOR_CONSTANT } from '../../constants/style-constant';
import LoadingView from '../../components/LoadingView';
import { loadCompany } from '../../redux/company/action';
import DetailsItem from './components/detailsItem';
import CompanyItem from './components/companyItem';
import ContactItem from './components/contactItem';
import { COLOR_CONSTANT } from '../../constants/style-constant';

const MyCompany = ({ navigation, loadCompanyAction, list, currentUser }) => {
  const { data, loading } = list;

  const [activeTab, setActiveTab] = useState('details');

  const handleTabChange = (val) => {
    setActiveTab(val);
  };

  useEffect(() => {
    loadCompanyAction(currentUser.company_id);
  }, [loadCompanyAction]);

  return (
    <>
      <CommonHeader
        title="My Company"
        onBackPressed={() => navigation.goBack()}
      />
      <ScrollView style={{ backgroundColor: COLOR_CONSTANT.GRAY }}>
        {CompanySvg && (
          <View
            style={{
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <CompanySvg style={{ color: 'white' }} />
          </View>
        )}
        {loading ? (
          <View
            style={{
              padding: 24,
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <LoadingView />
          </View>
        ) : (
          <View>
            {[data].map((q) => (
              <View key={data} style={{ backgroundColor: COLOR_CONSTANT.GRAY }}>
                <View
                  style={{
                    flex: 1,
                    marginTop: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 20,
                      fontWeight: '500',
                    }}
                  >
                    {data?.company}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 52,
                    marginTop: 40,
                    marginHorizontal: 8,
                    borderRadius: 5,
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      backgroundColor:
                        activeTab == 'details' ? '#1C4469' : '#0073C3',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 10,
                      borderRadius: 5,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => handleTabChange('details')}
                    >
                      <View>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_500Medium',
                            fontSize: 16,
                            color: '#FFFFFF',
                          }}
                        >
                          Details
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      backgroundColor:
                        activeTab == 'company' ? '#1C4469' : '#0073C3',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 10,
                      borderRadius: 5,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => handleTabChange('company')}
                    >
                      <View>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_500Medium',
                            fontSize: 16,
                            color: '#FFFFFF',
                          }}
                        >
                          Company
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      backgroundColor:
                        activeTab == 'contact' ? '#1C4469' : '#0073C3',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 5,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => handleTabChange('contact')}
                    >
                      <View>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_500Medium',
                            fontSize: 16,
                            color: '#FFFFFF',
                          }}
                        >
                          Contact
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
                {(activeTab == 'details' && <DetailsItem data={q} />) ||
                  (activeTab == 'company' && <CompanyItem data={q} />) ||
                  (activeTab == 'contact' && <ContactItem data={q} />)}
              </View>
            ))}
          </View>
        )}
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ companyApp, authApp }) => {
  const { list } = companyApp;
  const { currentUser } = authApp;

  return {
    list,
    currentUser,
  };
};

export default connect(mapStateToProps, {
  loadCompanyAction: loadCompany,
})(MyCompany);
