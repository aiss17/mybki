// @ts-nocheck
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Text, View, ScrollView } from 'react-native';
import { Button } from 'react-native-paper';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import CommonHeader from '../../components/common/common-header';
import SurveySvg from '../../assets/icons/survey.svg';
import LoadingView from '../../components/LoadingView';
import ListItem from './components/list-item';
import { addNewNotification } from '../../redux/satisfaction/action';

const Satisfaction = ({
  navigation,
  kepuasan,
  currentUser,
  surveyCrm,
  notif,
  addNewNotificationAction,
}) => {
  const { data, loading } = kepuasan;
  const [submit, setSubmit] = useState(false);

  useEffect(() => {
    if (notif?.message === 'Success') {
      navigation.goBack();
    }
  }, [notif]);

  return (
    <>
      <CommonHeader
        title="Satisfaction"
        onBackPressed={() => navigation.goBack()}
      />

      <ScrollView style={{ flex: 1, padding: 20 }}>
        {SurveySvg && (
          <View
            style={{
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <SurveySvg style={{ color: 'white' }} />
          </View>
        )}

        <View
          style={{
            marginBottom: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 20,
              fontWeight: '500',
              marginBottom: 5,
              marginTop: 24,
            }}
          >
            Survey Kepuasan
          </Text>
        </View>

        <View
          style={{
            marginBottom: 40,
          }}
        >
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '400',
              marginBottom: 5,
              marginTop: 10,
              textAlign: 'center',
            }}
          >
            Survey ini diadakan untuk mengetahui tingkat kepuasan pengguna myBKI
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat_500Medium',
              fontSize: 14,
              fontWeight: '400',
              marginBottom: 5,
              marginTop: 10,
              textAlign: 'center',
            }}
          >
            Silakan isi kuisioner dibawah ini!
          </Text>
        </View>

        {loading ? (
          <View
            style={{
              padding: 24,
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <LoadingView />
          </View>
        ) : (
          <>
            {data && (
              <>
                {data?.group.map((q) => {
                  return (
                    <ListItem
                      key={q.group_id}
                      data={q}
                      submit={submit}
                      setSubmit={setSubmit}
                      currentUser={currentUser}
                      surveyCrm={surveyCrm}
                      addNewNotificationAction={addNewNotificationAction}
                    />
                  );
                })}
              </>
            )}
          </>
        )}
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 40,
          }}
        >
          <Button
            onPress={() => setSubmit(true)}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 5,
              borderWidth: 1,
              borderColor: COLOR_CONSTANT.WHITE,
              backgroundColor: '#1C4469',
              width: 280,
              height: 52,
            }}
          >
            <Text
              style={{
                color: COLOR_CONSTANT.WHITE,
                fontSize: 20,
                fontFamily: 'Montserrat_500Medium',
                textAlign: 'center',
              }}
            >
              Kirim
            </Text>
          </Button>
        </View>
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ satisfactionApp, authApp }) => {
  const { kepuasan, surveyCrm, notif } = satisfactionApp;
  const { currentUser } = authApp;

  return {
    kepuasan,
    currentUser,
    surveyCrm,
    notif,
  };
};

export default connect(mapStateToProps, {
  addNewNotificationAction: addNewNotification,
})(Satisfaction);
