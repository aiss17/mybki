/* eslint-disable dot-notation */
/* eslint-disable no-restricted-syntax */
/* eslint-disable eqeqeq */
// @ts-nocheck
import React, { useState, useEffect } from 'react';
import moment from 'moment';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import { RadioButton, Checkbox } from 'react-native-paper';
import { COLOR_CONSTANT } from '../../../constants/style-constant';
import ArrowRightSvg from '../../../assets/icons/chevron-right.svg';
import ArrowDownSvg from '../../../assets/icons/chevron-down.svg';

const ListItem = ({
  data,
  submit,
  setSubmit,
  currentUser,
  surveyCrm,
  addNewNotificationAction,
}) => {
  const [expanded, setExpanded] = useState(false);
  const [checkId, setCheckId] = useState();
  const [checkbox, setCheckbox] = useState(null);

  const onChangeCheck = (id, question, type = 'checkbox') => {
    setCheckbox((prev) => {
      if (prev?.[question]) {
        if (type == 'checkbox') {
          const isExist = prev[question].some((w) => w == id);
          if (!isExist) {
            return {
              ...prev,
              [question]: [...prev[question], id],
            };
          }
          const replace = prev[question].filter((q) => q != id);
          return {
            ...prev,
            [question]: replace,
          };
        }
        return {
          ...prev,
          [question]: [id],
        };
      }
      if (type == 'input' || type == 'textarea') {
        return {
          ...prev,
          [question]: id,
        };
      }
      return {
        ...prev,
        [question]: [id],
      };
    });
  };

  const onSubmit = () => {
    const row = {
      survey: {
        clientid: currentUser?.company_id,
        surveyid:
          surveyCrm &&
          surveyCrm.data &&
          surveyCrm.data[0] &&
          surveyCrm.data[0].surveyid,
        date: moment().format('YYYY-MM-DD HH:mm:ss'),
        ip: '127.0.0.1',
        useragent:
          'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0',
      },
      selectable: null,
      question: null,
    };

    const keys = Object.keys(checkbox);
    let validation = null;
    data?.question_form.forEach((quest) => {
      const find = keys.find((key) => key === quest.questionid);

      validation = keys?.length == data?.question_form?.length;

      if (find) {
        if (quest.boxtype === 'checkbox' || quest.boxtype === 'radio') {
          if (row.selectable === null) {
            row.selectable = {
              [quest.boxid]: {
                [quest.questionid]:
                  typeof checkbox[find] === 'string'
                    ? [checkbox[find]]
                    : checkbox[find],
              },
            };
          } else {
            row.selectable = {
              ...row.selectable,
              [quest.boxid]: {
                [quest.questionid]:
                  typeof checkbox[find] === 'string'
                    ? [checkbox[find]]
                    : checkbox[find],
              },
            };
          }
        } else if (quest.boxtype === 'input' || quest.boxtype === 'textarea') {
          if (row.question === null) {
            row.question = {
              [quest.questionid]: checkbox[find],
            };
          } else {
            row.question = {
              ...row.question,
              [quest.questionid]: checkbox[find],
            };
          }
        }
      }
    });
    if (validation == true) {
      addNewNotificationAction(row);
    } else if (validation == false) {
      alert('Please answer all of the question');
      setSubmit(false);
    }
  };

  useEffect(() => {
    if (submit === true) {
      onSubmit();
    }
  }, [submit]);

  const renderInfo = (form) => {
    return (
      <ScrollView style={{ flex: 1, marginBottom: 10 }}>
        {form.map((q) => {
          return (
            <View key={q.questionid} style={{ marginBottom: 10 }}>
              <View style={{ marginBottom: 10 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 15,
                    // marginVertical: 2,
                  }}
                >
                  {q.question}
                </Text>
              </View>
              <View style={{ marginBottom: 5 }}>
                {(q.boxtype == 'input' && (
                  <TextInput
                    placeholder="Respon Anda…"
                    onChangeText={(e) => {
                      onChangeCheck(e, q.questionid, 'input');
                    }}
                    value={checkbox?.[q.questionid]?.[0]}
                    multiline
                    editable
                    // defaultValue={!checkScope ? '' : finalScope && finalScope[0].name}
                    style={{
                      backgroundColor: COLOR_CONSTANT.GRAY,
                      minHeight: 48,
                    }}
                  />
                )) ||
                  (q.boxtype == 'textarea' && (
                    <TextInput
                      placeholder="Respon Anda…"
                      onChangeText={(e) => {
                        onChangeCheck(e, q.questionid, 'textarea');
                      }}
                      value={checkbox?.[q.questionid]?.[0]}
                      numberOfLines={10}
                      maxLength={100}
                      multiline
                      editable
                      style={{
                        backgroundColor: COLOR_CONSTANT.GRAY,
                        minHeight: 48,
                      }}
                    />
                  )) ||
                  (q.boxtype == 'radio' && (
                    <>
                      {data?.box_descriptions?.radio.map((x) => {
                        return (
                          <View
                            key={x.questionboxdescriptionid}
                            style={{ flexDirection: 'row' }}
                          >
                            {q.boxid == x.boxid && (
                              <>
                                <RadioButton
                                  key={x.questionboxdescriptionid}
                                  testID="test"
                                  value={x.questionboxdescriptionid}
                                  status={
                                    checkbox?.[x.questionid]?.find(
                                      (u) => u === x.questionboxdescriptionid
                                    )
                                      ? 'checked'
                                      : 'unchecked'
                                  }
                                  onPress={() =>
                                    onChangeCheck(
                                      x.questionboxdescriptionid,
                                      x.questionid,
                                      'radio'
                                    )
                                  }
                                  style={{ paddingRight: 10 }}
                                />
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_500Medium',
                                    fontSize: 16,
                                    fontWeight: '500',
                                    marginTop: 6,
                                  }}
                                >
                                  {x.description}
                                </Text>
                              </>
                            )}
                          </View>
                        );
                      })}
                    </>
                  )) ||
                  (q.boxtype == 'checkbox' && (
                    <>
                      {data?.box_descriptions?.checkbox.map((x) => {
                        return (
                          <View
                            key={x.questionboxdescriptionid}
                            style={{ flexDirection: 'row' }}
                          >
                            {q.boxid == x.boxid && (
                              <>
                                <Checkbox
                                  status={
                                    checkbox?.[x.questionid].find(
                                      (u) => u === x.questionboxdescriptionid
                                    )
                                      ? 'checked'
                                      : 'unchecked'
                                  }
                                  onPress={() =>
                                    onChangeCheck(
                                      x.questionboxdescriptionid,
                                      x.questionid
                                    )
                                  }
                                />
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_500Medium',
                                    fontSize: 16,
                                    fontWeight: '500',
                                    marginTop: 6,
                                  }}
                                >
                                  {x.description}
                                </Text>
                              </>
                            )}
                          </View>
                        );
                      })}
                    </>
                  ))}
              </View>
            </View>
          );
        })}
      </ScrollView>
    );
  };
  return (
    <>
      {data && (
        <ScrollView style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity
              onPress={() => {
                setCheckId(data.group_id);
                setExpanded(!expanded);
              }}
            >
              <View
                style={{
                  borderColor: COLOR_CONSTANT.WHITE,
                  borderWidth: 1,
                  backgroundColor:
                    checkId == data.group_id && expanded
                      ? '#1C4469'
                      : '#0073C3',
                  flexDirection: 'row',
                  marginBottom: 20,
                  borderRadius: 5,
                }}
              >
                <View
                  style={{
                    flex: 1,
                    borderColor: COLOR_CONSTANT.WHITE,
                    padding: 10,
                    minHeight: 48,
                    minWidth: 250,
                  }}
                >
                  <Text
                    style={{
                      color: COLOR_CONSTANT.WHITE,
                      paddingLeft: 20,
                      fontSize: 18,
                      fontFamily: 'Montserrat_600SemiBold',
                    }}
                  >
                    {data.group_name}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    borderColor: COLOR_CONSTANT.WHITE,
                    paddingTop: 10,
                    paddingRight: 5,
                  }}
                >
                  {(checkId == data.group_id && expanded && (
                    <ArrowDownSvg
                      style={{
                        color: COLOR_CONSTANT.WHITE,
                      }}
                    />
                  )) || (
                    <ArrowRightSvg
                      style={{
                        color: COLOR_CONSTANT.WHITE,
                      }}
                    />
                  )}
                </View>
              </View>
            </TouchableOpacity>
            {checkId == data.group_id && expanded && (
              <>{renderInfo(data.question_form)}</>
            )}
          </View>
        </ScrollView>
      )}
    </>
  );
};

export default ListItem;
