import React from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';

const Loading = () => {
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Text>Loading</Text>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default Loading;
