// @ts-nocheck
import { connect } from 'react-redux';
// import { connect } from 'formik';
import React, { useEffect } from 'react';
import { ScrollView, TouchableOpacity, RefreshControl } from 'react-native';
// import { withNavigation } from 'react-navigation';
import CommonHeader from '../../components/common/common-header';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import { loadFleets } from '../../redux/fleet/action';
import ListItem from './components/list-item';

const Vessels = ({
  navigation,
  loadFleetsAction,
  list,
}) => {
  const { data, loading } = list;
  useEffect(() => {
    loadFleetsAction();
  }, [loadFleetsAction]);
  return (
    <>
      <CommonHeader title="Vessels" onBackPressed={() => navigation.goBack()} />
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}
        refreshControl={
          <RefreshControl
            refreshing={data == null && loading}
            onRefresh={() => loadFleetsAction()}
          />
        }
      >
        {data && (
          <>
            {data.map((t) => {
              return (
                <ListItem
                key={t.id}
                data={t}
                navigation={navigation}
                />
                );
            })}
          </>
        )}
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ fleetsApp }) => {
  const { list } = fleetsApp;

  return {
    list,
  };
};

export default connect(mapStateToProps, {
  loadFleetsAction: loadFleets,
})(Vessels);
