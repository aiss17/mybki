import React, { useState } from 'react';
import { View } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { withNavigation } from 'react-navigation';
import CommonHeader from '../../../../components/common/common-header';
import ListItem from '../list-item';

const Detail = ({ navigation }) => {
  const insets = useSafeAreaInsets();
  // const [region, setRegion] = useState({
  //   latitude: 37.78825,
  //   longitude: -122.4324,
  //   latitudeDelta: 0.0922,
  //   longitudeDelta: 0.0421,
  // });

  // const onRegionChange = (region) => {
  //   setRegion(region);
  // }
  return (
    <>
      <CommonHeader
        title={navigation.state.params.fleet_name}
        onBackPressed={() => navigation.goBack()}
      />
      <View style={{ flex: 1, backgroundColor: 'red' }}>
        <MapView
          initialRegion={{
            latitude: -6.105644892807503,
            longitude: 106.89119393246123,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          style={{ flex: 1 }}
        >
          <Marker
            // key={index}
            coordinate={{
              latitude: -6.105644892807503,
              longitude: 106.89119393246123
            }}
            title='test'
            description='test'
          />
        </MapView>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            paddingHorizontal: 12,
            paddingBottom: 12,
            bottom: insets.bottom + 12,
          }}
        >
          <ListItem
            data={navigation.state.params}
            navigation={navigation}
          />
        </View>
      </View>
    </>
  );
};

export default withNavigation(Detail);
