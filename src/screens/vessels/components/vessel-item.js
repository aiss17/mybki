import React from 'react';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT } from '../../../constants/style-constant';

const COLORS = [
  COLOR_CONSTANT.RED,
  COLOR_CONSTANT.CYAN,
  COLOR_CONSTANT.DARKBLUE,
  COLOR_CONSTANT.ORANGE,
];

const VesselItem = ({data}) => {
  return (
    
    <View style={{ flexDirection: 'row', marginVertical: 3 }}>
      <View
        style={{
          width: 5,
          backgroundColor: COLORS[Math.floor(Math.random() * COLORS.length)],
        }}
        />
          <Text
            style={{
              fontFamily: 'Montserrat_400Regular',
              fontSize: 12,
              marginLeft: 5,
            }}
          > 
            {data.vessel_name === null ? '-' : data.vessel_name}
          </Text>
    </View>
  );
};

export default VesselItem;
