import React from 'react';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';
import VesselItem from './vessel-item';

const ListItem = ({ data, navigation }) => {

  return (
    <>
      {data && (
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 12,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.WHITE,
              height: 180,
              borderRadius: 5,
              overflow: 'hidden',
            }}
          >
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View
                style={{
                  width: '30%',
                  backgroundColor: COLOR_CONSTANT.DARKBLUE,
                }}
              >
                <View
                  style={{
                    flex: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 46,
                      color: COLOR_CONSTANT.WHITE,
                    }}
                  >
                    {data.attributes.vessels.length}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 16,
                      color: COLOR_CONSTANT.WHITE,
                    }}
                  >
                    Fleet
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1, paddingVertical: 8 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => navigation.navigate('VesselDetail', data)}
                  >
                    <View style={{ paddingHorizontal: 16 }}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_400Regular',
                          fontSize: 16,
                        }}
                      >
                        {data.fleet_name === null ? '-' : data.fleet_name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('ListVessel', data)}
                  >
                    <View
                      style={{
                        borderColor: COLOR_CONSTANT.DARKBLUE,
                        borderWidth: 1,
                        borderRadius: 15,
                        paddingHorizontal: 12,
                        paddingVertical: 4,
                        marginRight: 8,
                      }}
                    >
                      <Text
                        style={{ color: COLOR_CONSTANT.DARKBLUE, fontSize: 12 }}
                      >
                        Detail Fleet
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    height: 2,
                    borderBottomWidth: 1,
                    borderBottomColor: COLOR_CONSTANT.BLACK,
                    marginTop: 8,
                    marginHorizontal: 16,
                  }}
                />
                <View style={{ paddingHorizontal: 16, marginTop: 8 }}>
                  {data.attributes.vessels.map((t) => {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          navigation.navigate('VesselDetail', data)
                        }
                        key={t.vessel_code}
                      >
                        <View>
                          <VesselItem data={t} />
                        </View>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            </View>
          </View>
        </View>
      )}
    </>
  );
};

export default ListItem;
