import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';

const ItemSurvey = ({ data }) => {
  const [expanded, setExpanded] = useState(false);
  const renderInfo = (text1, text2) => {
    return (
      <View style={{ marginVertical: 4 }}>
        <Text style={{ fontFamily: 'Montserrat_700Bold', fontSize: 11 }}>
          {text2}
        </Text>
        <Text
          style={{
            fontFamily: 'Montserrat_500Medium',
            fontSize: 9,
          }}
        >
          {text1}
        </Text>
      </View>
    );
  };
  const [color, setColor] = useState('red');
  useEffect(() => {
    const today = moment().valueOf();
    if (data) {
      const toDate = moment(data.to_date).valueOf();
      const sixMonthLater = moment().add(6, 'months');
      const afterSixMonth = sixMonthLater.valueOf();

      // overdue
      if (
        today > toDate &&
        data.is_done === false &&
        data.request_date === null
      ) {
        setColor('#dc3545');
      }

      // overdue within 180
      if (
        today < toDate &&
        afterSixMonth > toDate &&
        data.is_done === false &&
        data.request_date === null
      ) {
        setColor('#ffc107');
      }

      // overdue after 180
      if (
        today < toDate &&
        toDate > afterSixMonth &&
        data.is_done === false &&
        data.request_date === null
      ) {
        setColor('#28a745');
      }

      // in progress
      if (data.is_done === false && data.request_date !== null) {
        setColor('blue');
      }

      // commenced
      if (data.is_done === true) {
        setColor('#ebebeb');
      }
    }
  }, [data]);
  return (
      <View
        style={[
          {
            borderRadius: 5,
            marginBottom: 12,
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
        <View
          style={{
            backgroundColor: COLOR_CONSTANT.WHITE,
            borderRadius: 5,
            overflow: 'hidden',
            flexDirection: 'row',
          }}
        >
          <View style={{ backgroundColor: color, width: 4 }}></View>
          <View
            style={{
              flex: 1,
              marginLeft: 16,
              marginRight: 8,
              marginVertical: 8,
            }}
          >
            {renderInfo(data?.time_period, `Item Survey`)}
            {renderInfo(data.due_date ? moment(data.due_date).format('DD MMMM YYYY') : '-', `Due Date`)}
            {renderInfo(data.start_date && data.to_date ? `${moment(data.start_date).format('DD MMMM YYYY')}` + ' - ' + `${moment(data.to_date).format('DD MMMM YYYY')}`: '-', `Range Date`)}
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <View
                style={{
                  borderColor: COLOR_CONSTANT.DARKBLUE,
                  borderWidth: 1,
                  borderRadius: 15,
                  paddingHorizontal: 12,
                  paddingVertical: 4,
                }}
              >
                <Text style={{ color: COLOR_CONSTANT.DARKBLUE, fontSize: 12 }}>
                  Requirement
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
  );
};

export default ItemSurvey;