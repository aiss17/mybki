import React, { useEffect, useState } from 'react';
import { ScrollView, View, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from 'accordion-collapse-react-native';
import CommonHeader from '../../../../components/common/common-header';
import TabItem from '../../../../components/common/tab-item';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';
import { loadSurveyAudit, loadVessel } from '../../../../redux/fleet/action';

import ItemSurvey from './item-survey';

const DetailVessel = ({
  navigation,
  loadVesselAction,
  vessel,
  survey,
  loadSurveyAction,
}) => {
  const { data } = vessel;
  useEffect(() => {
    loadVesselAction(navigation.state.params.vessel_code);
    loadSurveyAction(navigation.state.params.vessel_code);
    // console.log(navigation.state.params.vessel_code)
  }, []);
  const [activeTab, setActiveTab] = useState('general');
  const handleTabChange = (val) => {
    setActiveTab(val);
  };

  const renderInfo = (text1, text2) => {
    return (
      <View style={{ marginVertical: 4 }}>
        <Text style={{ fontFamily: 'Montserrat_700Bold', fontSize: 11 }}>
          {text2}
        </Text>
        <Text
          style={{
            fontFamily: 'Montserrat_500Medium',
            fontSize: 9,
          }}
        >
          {text1}
        </Text>
      </View>
    );
  };
  return (
    <>
      <CommonHeader
        title="Detail Vessel"
        onBackPressed={() => navigation.goBack()}
      />
      <View
        style={[
          {
            borderRadius: 5,
            marginHorizontal: 16,
            marginTop: 16,
            marginBottom: 8,
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
        <View
          style={[
            {
              backgroundColor: '#EFF1F3',
              borderRadius: 5,
              flexDirection: 'row',
            },
          ]}
        >
          <TabItem
            label="General Information"
            isActive={activeTab === 'general'}
            onPressed={() => handleTabChange('general')}
          />
          <TabItem
            label="Status Survey Audit"
            isActive={activeTab === 'status'}
            onPressed={() => handleTabChange('status')}
          />
        </View>
      </View>
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}
      >
        {activeTab === 'general' && (
          <View
            style={[
              {
                borderRadius: 5,
                marginBottom: 12,
              },
              STYLE_CONSTANT.SHADOW2,
            ]}
          >
            <View
              style={{
                backgroundColor: COLOR_CONSTANT.WHITE,
                borderRadius: 5,
                overflow: 'hidden',
                flexDirection: 'row',
              }}
            >
              <View style={{ backgroundColor: 'red', width: 4 }}>
                <View
                  style={{
                    flex: 1,
                    marginLeft: 16,
                    marginRight: 8,
                    marginVertical: 8,
                  }}
                >
                  {renderInfo(data?.name, 'Vessel Name')}
                  {renderInfo(data?.register, 'No Register')}
                  {renderInfo(data?.imo, 'No IMO')}
                  {renderInfo(data?.flag, 'Flag')}
                  {renderInfo(data?.call_sign, 'Call Sign')}
                </View>
              </View>
            </View>
          </View>
        )}
        {activeTab === 'status' && (
          <>
            <View
              style={{
                paddingVertical: 5,
              }}
            >
              <Collapse>
                <CollapseHeader>
                  <View
                    style={{
                      backgroundColor: COLOR_CONSTANT.DARKBLUE,
                      height: 40,
                    }}
                  >
                    <View
                      style={{ paddingVertical: 10, paddingHorizontal: 10 }}
                    >
                      <Text
                        style={{
                          fontFamily: 'Montserrat_400Regular',
                          fontSize: 16,
                          color: 'white',
                        }}
                      >
                        Classification
                      </Text>
                    </View>
                  </View>
                </CollapseHeader>
                <CollapseBody>
                  <View
                    style={{
                      backgroundColor: COLOR_CONSTANT.WHITE,
                      borderRadius: 5,
                      overflow: 'hidden',
                      flexDirection: 'row',
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        marginLeft: 16,
                        marginRight: 8,
                        marginVertical: 8,
                      }}
                    >
                      <>
                        {survey?.data?.class.map((t) => {
                          return <ItemSurvey key={t.id} data={t} />;
                        })}
                      </>
                    </View>
                  </View>
                </CollapseBody>
              </Collapse>
            </View>
            <View>
              <Collapse>
                <CollapseHeader>
                  <View
                    style={{
                      backgroundColor: COLOR_CONSTANT.DARKBLUE,
                      height: 40,
                    }}
                  >
                    <View
                      style={{ paddingVertical: 10, paddingHorizontal: 10 }}
                    >
                      <Text
                        style={{
                          fontFamily: 'Montserrat_400Regular',
                          fontSize: 16,
                          color: 'white',
                        }}
                      >
                        Statutory
                      </Text>
                    </View>
                  </View>
                </CollapseHeader>
                <CollapseBody>
                  <View
                    style={{
                      backgroundColor: COLOR_CONSTANT.WHITE,
                      borderRadius: 5,
                      overflow: 'hidden',
                      flexDirection: 'row',
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        marginLeft: 16,
                        marginRight: 8,
                        marginVertical: 8,
                      }}
                    >
                      <>
                        {survey?.data?.satutori.map((t) => {
                          return <ItemSurvey key={t.id} data={t} />;
                        })}
                      </>
                    </View>
                  </View>
                </CollapseBody>
              </Collapse>
            </View>
          </>
        )}
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ fleetsApp }) => {
  const { vessel, survey } = fleetsApp;

  return {
    vessel,
    survey,
  };
};

export default connect(mapStateToProps, {
  loadVesselAction: loadVessel,
  loadSurveyAction: loadSurveyAudit,
})(DetailVessel);
