import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';

const ListItem = ({ navigation, data }) => {
  // console.log(data)
  const renderInfo = (text1, text2) => {
    return (
      <View style={{ marginVertical: 4 }}>
        <Text style={{ fontFamily: 'Montserrat_700Bold', fontSize: 11 }}>
          {text2}
        </Text>
        <Text
          style={{
            fontFamily: 'Montserrat_500Medium',
            fontSize: 9,
          }}
        >
          {text1}
        </Text>
      </View>
    );
  };
  return (
    <View
      style={[
        {
          borderRadius: 5,
          marginBottom: 12,
        },
        STYLE_CONSTANT.SHADOW2,
      ]}
    >
      <View
        style={{
          backgroundColor: COLOR_CONSTANT.WHITE,
          borderRadius: 5,
          overflow: 'hidden',
          flexDirection: 'row',
        }}
        key={data.imo}
      >
        <View style={{ backgroundColor: 'red', width: 4 }}></View>

        <View
          style={{
            flex: 1,
            marginLeft: 16,
            marginRight: 8,
            marginVertical: 8,
          }}
        >
          {renderInfo(data.vessel_name, `Vessel Name`)}
          {renderInfo(data.vessel_code, 'No Register')}
          {renderInfo(data.imo, 'No IMO')}
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('DetailVessel', data);
              }}
            >
              <View
                style={{
                  borderColor: COLOR_CONSTANT.DARKBLUE,
                  borderWidth: 1,
                  borderRadius: 15,
                  paddingHorizontal: 12,
                  paddingVertical: 4,
                }}
              >
                <Text style={{ color: COLOR_CONSTANT.DARKBLUE, fontSize: 12 }}>
                  Detail
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ListItem;
