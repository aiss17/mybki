import React from 'react';
import { ScrollView, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import CommonHeader from '../../../../components/common/common-header';
import ListItem from './list-vessel';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';

const ListVessel = ({
  navigation
}) => {
  return (
    <>
      <CommonHeader
        title={navigation.state.params.fleet_name}
        onBackPressed={() => navigation.goBack()}
      />
      <View
        style={[
          {
            position: 'absolute',
            width: '100%',
            paddingHorizontal: 12,
            paddingBottom: 12,
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
      </View>
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}
      >
        {navigation.state.params.vessels.map((t) => (
          <View key={t.imo}>
            <ListItem
              data={t}
              navigation={navigation}
            />
          </View>
        ))}
      </ScrollView>
    </>
  );
};

export default withNavigation(ListVessel);
