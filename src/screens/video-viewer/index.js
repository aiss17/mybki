import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, { useEffect } from 'react';
import { View } from 'react-native';
import OptionsMenu from 'react-native-option-menu';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import WebView from 'react-native-webview';
import { withNavigation } from 'react-navigation';

const VideoViewer = ({ navigation }) => {
  const insets = useSafeAreaInsets();
  useEffect(() => {
    if (
      !(
        navigation &&
        navigation.state &&
        navigation.state.params &&
        navigation.state.params.data
      )
    ) {
      navigation.goBack();
    }

    // navigation.goBack();
    // WebBrowser.openBrowserAsync(navigation.state.params.data.url, {
    //   showTitle: false,
    // });
  }, []);
  return (
    <>
      <View
        style={{
          width: '100%',
          height: '100%',
        }}
      >
        <WebView
          javaScriptEnabled
          scrollEnabled={false}
          allowsFullscreenVideo
          source={{
            uri: navigation.state.params.data.url,
            // uri: 'https://iframe.dacast.com/live/02bdde95-e7f9-3a4b-84bd-be0de30d1a22/d641d7e4-1d42-4c33-fdd8-6cce1d939439',
          }}
        />
        <View style={{ position: 'absolute', top: insets.top + 12, right: 12 }}>
          <OptionsMenu
            customButton={
              <FontAwesomeIcon icon="bars" size={22} color="white" />
            }
            destructiveIndex={1}
            options={['Exit', 'Cancel']}
            actions={[() => navigation.goBack()]}
          />
        </View>
      </View>
    </>
  );
};

export default withNavigation(VideoViewer);
