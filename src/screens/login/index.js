import { Formik } from 'formik';
import React from 'react';
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { Banner } from 'react-native-paper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import BottomSvg from '../../assets/backgrounds/pattern-bottom.svg';
import TopSvg from '../../assets/backgrounds/pattern-top.svg';
import EmailSvg from '../../assets/icons/email.svg';
import LockSvg from '../../assets/icons/lock.svg';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import * as action from '../../redux/auth/actions';

const logo = require('../../assets/logos/logo-color.png');

const Login = ({
  navigation,
  loading,
  error,
  loginUserAction,
  loginUserSuccessAction,
}) => {
  const insets = useSafeAreaInsets();
  return (
    <>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <TouchableWithoutFeedback
          onPress={Keyboard.dismiss}
          style={{ flex: 1 }}
        >
          <Formik
            initialValues={{
              username: 'admin@admin.com',
              password: 'secret',
            }}
            onSubmit={(values) => {
              if (!loading) {
                loginUserAction(
                  {
                    email: values.username,
                    password: values.password,
                  },
                  navigation
                );
              }
            }}
          >
            {({ handleChange, handleBlur, handleSubmit, values }) => (
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  backgroundColor: COLOR_CONSTANT.GRAY,
                }}
              >
                <View
                  style={{ position: 'absolute', width: '100%', bottom: 0 }}
                >
                  <BottomSvg width="100%" />
                </View>
                <View
                  style={{ backgroundColor: '#0073c3', height: insets.top }}
                />
                <View style={{ height: 100 }}>
                  <TopSvg width="100%" />
                </View>
                <View
                  style={{
                    width: '100%',
                    alignItems: 'center',
                  }}
                >
                  <Image
                    source={logo}
                    resizeMode="contain"
                    style={{ height: 164, width: '80%' }}
                  />
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    marginHorizontal: 28,
                    marginTop: 18,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      fontSize: 32,
                      color: '#0073C3',
                    }}
                  >
                    Welcome!
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 16,
                      color: COLOR_CONSTANT.BLACK,
                    }}
                  >
                    Please sign In to continue
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: COLOR_CONSTANT.WHITE,
                      paddingVertical: 18,
                      paddingLeft: 18,
                      borderRadius: 12,
                      marginTop: 18,
                    }}
                  >
                    <EmailSvg
                      style={{ color: COLOR_CONSTANT.BLACK }}
                      width={22}
                      height={15}
                    />
                    <TextInput
                      style={{
                        flex: 1,
                        fontFamily: 'Montserrat_400Regular',
                        fontSize: 22,
                        marginLeft: 12,
                      }}
                      placeholderTextColor={COLOR_CONSTANT.BLACK}
                      placeholder="Email"
                      onChangeText={handleChange('username')}
                      onBlur={handleBlur('username')}
                      autoCapitalize="none"
                      autoCorrect={false}
                      autoComplete={false}
                      editable={!loading}
                      selectTextOnFocus={!loading}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: COLOR_CONSTANT.WHITE,
                      paddingVertical: 18,
                      paddingLeft: 18,
                      borderRadius: 12,
                      marginTop: 18,
                    }}
                  >
                    <LockSvg
                      style={{ color: COLOR_CONSTANT.BLACK }}
                      width={16}
                      height={23}
                    />
                    <TextInput
                      style={{
                        flex: 1,
                        fontFamily: 'Montserrat_400Regular',
                        fontSize: 22,
                        marginLeft: 12,
                      }}
                      placeholderTextColor={COLOR_CONSTANT.BLACK}
                      placeholder="Password"
                      onChangeText={handleChange('password')}
                      onBlur={handleBlur('password')}
                      autoCapitalize="none"
                      editable={!loading}
                      selectTextOnFocus={!loading}
                      secureTextEntry
                    />
                  </View>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 16,
                      color: '#0073C3',
                      width: '100%',
                      textAlign: 'right',
                      marginTop: 16,
                    }}
                  >
                    Forget Password?
                  </Text>
                  <TouchableOpacity
                    style={{ width: '100%' }}
                    onPress={handleSubmit}
                    disabled={loading}
                  >
                    <View
                      style={{
                        backgroundColor: '#0073C3',
                        width: '100%',
                        alignItems: 'center',
                        paddingVertical: 9,
                        borderRadius: 32,
                        marginTop: 24,
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: 'Montserrat_700Bold',
                          fontSize: 24,
                          color: COLOR_CONSTANT.WHITE,
                        }}
                      >
                        Sign In
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 16,
                      color: COLOR_CONSTANT.BLACK,
                      marginTop: 42,
                    }}
                  >
                    New User?{' '}
                    <Text
                      style={{
                        fontFamily: 'Montserrat_700Bold',
                        fontSize: 16,
                        color: '#0073C3',
                      }}
                    >
                      Sign Up
                    </Text>
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    position: 'absolute',
                    top: insets.top + 20,
                    alignItems: 'center',
                  }}
                >
                  <Banner
                    visible={!!error}
                    style={{ width: '80%' }}
                    actions={[
                      {
                        label: 'OK',
                        onPress: () => {
                          loginUserSuccessAction(null);
                        },
                      },
                    ]}
                  >
                    {error}
                  </Banner>
                </View>
              </View>
            )}
          </Formik>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </>
  );
};

const mapStateToProps = ({ authApp }) => {
  const { currentUser, history, loading, error, currentToken } = authApp;

  return {
    currentUser,
    history,
    loading,
    error,
    currentToken,
  };
};
export default connect(mapStateToProps, {
  loginUserAction: action.loginUser,
  loginUserSuccessAction: action.loginUserSuccess,
})(Login);
