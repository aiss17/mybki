// @ts-nocheck
import React, { useEffect } from 'react';
import { SearchBar } from 'react-native-elements';
import { useActionSheet } from '@expo/react-native-action-sheet';
import { connect } from 'react-redux';
import {
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
// import { withNavigation } from 'react-navigation';
import CommonHeader from '../../components/common/common-header';
// import { COLOR_CONSTANT } from '../../constants/style-constant';
import ListItem from './components/list-item';
import {
  loadQuotations,
  selectedQuotation,
} from '../../redux/quotation/action';
import LoadingView from '../../components/LoadingView';

const Quotation = ({
  navigation,
  loadQuotationsAction,
  selectedQuotationAction,
  list,
}) => {
  const { data, page, loading } = list;

  const { showActionSheetWithOptions } = useActionSheet();

  useEffect(() => {
    loadQuotationsAction();
  }, [loadQuotationsAction]);

  return (
    <>
      <CommonHeader
        title="Quotation"
        onBackPressed={() => navigation.goBack()}
      />

      <SearchBar
        inputContainerStyle={{
          backgroundColor: '#E5E5E5',
          borderColor: '#8E8E8E',
          borderWidth: 1,
        }}
        round
        containerStyle={{
          backgroundColor: 'white',
        }}
        searchIcon={{ size: 24 }}
        placeholder="Type order number..."
        style={{ fontSize: 14, fontStyle: 'italic' }}
      />
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={data == null && loading}
            onRefresh={() => loadQuotationsAction()}
          />
        }
      >
        {data && (
          <>
            {data.map((t) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    selectedQuotationAction(t);
                    navigation.navigate('QuotationDetail');
                  }}
                  key={t.rfq_id}
                >
                  <ListItem
                    showActionSheetWithOptions={showActionSheetWithOptions}
                    data={t}
                  />
                </TouchableOpacity>
              );
            })}
            {loading ? (
              <View
                style={{
                  padding: 24,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
              >
                <LoadingView />
              </View>
            ) : (
              <>
                {page.currentPage < page.totalPage && (
                  <View
                    style={{
                      padding: 24,
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        loadQuotationsAction({
                          selectedPage: page.currentPage + 1,
                        })
                      }
                    >
                      <Text
                        style={{
                          color: 'gray',
                          fontSize: 16,
                        }}
                      >
                        Load More
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </>
            )}
          </>
        )}
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ quotationApp }) => {
  const { list } = quotationApp;

  return {
    list,
  };
};

export default connect(mapStateToProps, {
  loadQuotationsAction: loadQuotations,
  selectedQuotationAction: selectedQuotation,
})(Quotation);
