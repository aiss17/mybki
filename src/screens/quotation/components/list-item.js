/* eslint-disable no-nested-ternary */
import React from 'react';
import moment from 'moment';
import { Text, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';

const ListItem = ({ data }) => {
  const quotationStatus = (param) => {
    switch (param) {
      case 'Approved':
        return COLOR_CONSTANT.GREEN;
      case 'Proses':
        return COLOR_CONSTANT.DARKBLUE;
      default:
        return COLOR_CONSTANT.RED;
    }
  };
  return (
    <>
      {data && (
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 1,
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.WHITE,
              height: 85,
              borderRadius: 5,
              overflow: 'hidden',
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <View
              style={{ width: 60, justifyContent: 'center', marginRight: 5 }}
            >
              <View
                style={{
                  backgroundColor: COLOR_CONSTANT.WHITE,
                  margin: 0,
                  borderRadius: 4,
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 85,
                  width: 65,
                }}
              >
                <FontAwesomeIcon
                  icon={faFile}
                  size={40}
                  style={{ color: COLOR_CONSTANT.DARKBLUE }}
                />
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', margin: 4 }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 4,
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Quotation Number
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 12,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {data.quotation_no == null ? '-' : data.quotation_no}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    RFQ Number
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 11,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {data.rfq_no == null ? '-' : data.rfq_no}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Quotation Date
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_700Bold',
                      textAlign: 'right',
                      fontSize: 10,
                      color: COLOR_CONSTANT.DARKGRAY,
                    }}
                  >
                    {moment(data.quotation_date).format('DD MMMM yyyy')}
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 10,
                      marginVertical: 2,
                    }}
                  >
                    Status
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: quotationStatus(data.status),
                    borderRadius: 12,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 0,
                    marginRight: 0,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      textAlign: 'right',
                      fontSize: 10,
                      marginLeft: 10,
                      marginRight: 10,
                      color: COLOR_CONSTANT.WHITE,
                    }}
                  >
                    {data.status == null ? '-' : data.status}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      )}
    </>
  );
};

export default ListItem;
