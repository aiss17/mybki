import React, { useEffect } from 'react';
import moment from 'moment';
import { Text, View, ScrollView, RefreshControl } from 'react-native';
import { useActionSheet } from '@expo/react-native-action-sheet';
import { connect } from 'react-redux';
import { Avatar, Card, Divider } from 'react-native-paper';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';

import CommonHeader from '../../../../components/common/common-header';
import { loadQuotationsDetail } from '../../../../redux/quotation/action';
import ListItem from './components/list-item';

// var height = Dimensions.get('window').height;
const ListItemDetail = ({
  listDetail,
  navigation,
  loadQuotationsDetailAction,
  selectedQuotation,
}) => {
  const { showActionSheetWithOptions } = useActionSheet();
  useEffect(() => {
    loadQuotationsDetailAction({ id: selectedQuotation.data.quotation_no });
  }, [loadQuotationsDetailAction, selectedQuotation]);

  const { data, loading } = listDetail;
  return (
    <>
      <CommonHeader
        title="Detail Quotation"
        onBackPressed={() => navigation.goBack()}
      />
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={data == null && loading}
            onRefresh={() =>
              loadQuotationsDetailAction({
                id: selectedQuotation.data.quotation_no,
              })
            }
          />
        }
      >
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 12,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
          key={'parent'}
        >
          {data ? (
            <View
              style={[
                {
                  borderRadius: 12,
                  marginBottom: 8,
                  marginTop: 8,
                  marginLeft: 5,
                  marginRight: 5,
                },
                STYLE_CONSTANT.SHADOW2,
              ]}
            >
              {/* '#007AFF' */}
              <Card
                style={{ flex: 1, backgroundColor: COLOR_CONSTANT.DARKBLUE }}
              >
                <Card.Title
                  title={selectedQuotation.data.quotation_no}
                  titleStyle={{ fontSize: 18, color: COLOR_CONSTANT.WHITE }}
                  subtitle={moment(
                    selectedQuotation.data.quotation_date
                  ).format('dddd, d MMM YYYY')}
                  subtitleStyle={{ fontSize: 11, color: COLOR_CONSTANT.WHITE }}
                  left={(props) => (
                    <Avatar.Icon
                      {...props}
                      icon="account-clock-outline"
                      style={{ backgroundColor: COLOR_CONSTANT.WHITE }}
                    />
                  )}
                />
                <Divider style={{ backgroundColor: COLOR_CONSTANT.WHITE }} />
                <Card.Content>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginTop: 4,
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            fontSize: 10,
                            marginVertical: 2,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          Valid Date
                        </Text>
                      </View>
                      <View style={{ flex: 1, marginRight: 5 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            textAlign: 'right',
                            fontSize: 10,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          {selectedQuotation.data.valid_to == null
                            ? '-'
                            : moment(selectedQuotation.data.valid_to).format(
                                'dddd, d MMM YYYY'
                              )}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginTop: 4,
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            fontSize: 10,
                            marginVertical: 2,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          Sub Total
                        </Text>
                      </View>
                      <View style={{ flex: 1, marginRight: 5 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            textAlign: 'right',
                            fontSize: 10,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          {data[0].subtotal == null
                            ? '-'
                            : 'Rp '.concat(
                                parseInt(data[0].subtotal, 10)
                                  .toFixed(2)
                                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                              )}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginTop: 4,
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            fontSize: 10,
                            marginVertical: 2,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          Discount {data[0].discount_percent} %
                        </Text>
                      </View>
                      <View
                        style={{
                          backgroundColor: COLOR_CONSTANT.GREEN,
                          borderRadius: 12,
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginTop: 0,
                          marginRight: 0,
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: 'Montserrat_500Medium',
                            textAlign: 'right',
                            fontSize: 10,
                            marginLeft: 10,
                            marginRight: 10,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          {data[0].discount_total == null
                            ? '0'
                            : 'Rp '.concat(
                                parseInt(data[0].discount_total, 10)
                                  .toFixed(2)
                                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                              )}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginTop: 4,
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            fontSize: 10,
                            marginVertical: 2,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          Tax
                        </Text>
                      </View>
                      <View
                        style={{
                          backgroundColor: COLOR_CONSTANT.DARKORANGE,
                          borderRadius: 10,
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginTop: 0,
                          marginRight: 0,
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: 'Montserrat_500Medium',
                            textAlign: 'right',
                            fontSize: 10,
                            marginLeft: 10,
                            marginRight: 10,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          {data[0].total_tax == null
                            ? '0'
                            : parseInt(data[0].total_tax, 10)
                                .toFixed(2)
                                .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                                .concat(' %')}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginTop: 4,
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            fontSize: 10,
                            marginVertical: 2,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          Adjustment
                        </Text>
                      </View>
                      <View style={{ flex: 1, marginRight: 5 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            textAlign: 'right',
                            fontSize: 10,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          {data[0].adjustment == null
                            ? '-'
                            : 'Rp '.concat(
                                parseInt(data[0].adjustment, 10)
                                  .toFixed(2)
                                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                              )}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginTop: 4,
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            fontSize: 10,
                            marginVertical: 2,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          Total
                        </Text>
                      </View>
                      <View style={{ flex: 1, marginRight: 5 }}>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_700Bold',
                            textAlign: 'right',
                            fontSize: 12,
                            color: COLOR_CONSTANT.WHITE,
                          }}
                        >
                          {data[0].total == null
                            ? '-'
                            : 'Rp '.concat(
                                parseInt(data[0].total, 10)
                                  .toFixed(2)
                                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                              )}
                        </Text>
                      </View>
                    </View>
                  </View>
                </Card.Content>
              </Card>
            </View>
          ) : null}
          {data &&
            data.map((t, i) => {
              return (
                <ListItem
                  // @ts-ignore
                  showActionSheetWithOptions={showActionSheetWithOptions}
                  data={t}
                  key={`${i}`}
                />
              );
            })}
        </View>
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ quotationApp }) => {
  const { listDetail, selectedQuotation } = quotationApp;

  return {
    listDetail,
    selectedQuotation,
  };
};

export default connect(mapStateToProps, {
  loadQuotationsDetailAction: loadQuotationsDetail,
})(ListItemDetail);
