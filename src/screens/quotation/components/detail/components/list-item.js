import React from 'react';
import { Text, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../../constants/style-constant';

const ListItem = ({ data }) => {
  // console.log('dataDetail', data);

  return (
    <>
      <View
        style={[
          {
            borderRadius: 5,
            marginBottom: 5,
            marginLeft: 5,
            marginRight: 5,
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
        <View
          style={{
            backgroundColor: COLOR_CONSTANT.WHITE,
            borderRadius: 5,
            overflow: 'hidden',
            flex: 1,
            flexDirection: 'row',
          }}
        >
          <View style={{ width: 60, justifyContent: 'center' }}>
            <View
              style={{
                backgroundColor: COLOR_CONSTANT.WHITE,
                margin: 0,
                borderRadius: 4,
                justifyContent: 'center',
                alignItems: 'center',
                height: 80,
                width: 65,
              }}
            >
              <FontAwesomeIcon
                icon={faCheckCircle}
                size={40}
                style={{ color: COLOR_CONSTANT.DARKBLUE }}
              />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              marginRight: 5,
              marginBottom: 5,
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 4,
              }}
            >
              <View style={{ flex: 1 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_700Bold',
                    fontSize: 11,
                    color: COLOR_CONSTANT.DARKGRAY,
                  }}
                >
                  {data.description == null ? '-' : data.description}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 4,
              }}
            >
              <View style={{ flex: 1 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 10,
                    marginVertical: 2,
                  }}
                >
                  Quantity
                </Text>
              </View>
              <View style={{ flex: 1, marginRight: 5 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_700Bold',
                    textAlign: 'right',
                    fontSize: 10,
                    color: COLOR_CONSTANT.DARKGRAY,
                  }}
                >
                  {data.qty == null ? '0' : parseInt(data.qty, 10).toFixed(0)}
                </Text>
              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 10,
                    marginVertical: 2,
                  }}
                >
                  Rate
                </Text>
              </View>
              <View style={{ flex: 1, marginRight: 5 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_700Bold',
                    textAlign: 'right',
                    fontSize: 10,
                    color: COLOR_CONSTANT.DARKGRAY,
                  }}
                >
                  {data.rate == null
                    ? '-'
                    : 'Rp '.concat(
                        parseInt(data.rate, 10)
                          .toFixed(2)
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                      )}
                </Text>
              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 10,
                    marginVertical: 2,
                  }}
                >
                  Amount
                </Text>
              </View>
              <View style={{ flex: 1, marginRight: 5 }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_700Bold',
                    textAlign: 'right',
                    fontSize: 12,
                    color: COLOR_CONSTANT.DARKGRAY,
                  }}
                >
                  {data.rate == null
                    ? '-'
                    : 'Rp '.concat(
                        parseInt(data.rate, 10)
                          .toFixed(2)
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')
                      )}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

export default ListItem;
