// @ts-nocheck
/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ScrollView,
  Modal,
  Pressable,
  Alert,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Button } from 'react-native-paper';
// import { useActionSheet } from '@expo/react-native-action-sheet';
import {
  COLOR_CONSTANT,
  // STYLE_CONSTANT,
} from '../../../constants/style-constant';
import CommonHeader from '../../../components/common/common-header';
import LoadingView from '../../../components/LoadingView';
import CreditCard from '../../../assets/icons/creditcard.svg';
import ArrowRight from '../../../assets/icons/chevron-right2.svg';
import AddIcon from '../../../assets/icons/icon-add-small2.svg';
import Checklist from '../../../assets/icons/icon-checklist.svg';
import {
  savePayments,
  loadPayments,
  loadDuitkuMethods,
} from '../../../redux/payment/action';

const AddNew = ({
  navigation,
  invoice,
  add,
  currentUser,
  duitku,
  savePaymentsAction,
  loadPaymentsAction,
  loadDuitkuMethodsAction,
}) => {
  console.log('duitku', duitku);
  const { data, message } = duitku;

  const [modalVisible, setModalVisible] = useState(false);
  const [modalPaymentMethod, setModalPaymentMethod] = useState(false);
  const [modalPaymentDuitku, setModalPaymentDuitku] = useState(false);
  const [method, setMethod] = useState('Metode Pembayaran');
  const [checkedFinal, setCheckedFinal] = useState([]);
  const [countFinal, setCountFinal] = useState(0);

  const [virtualAccounts, setVirtualAccounts] = useState([]);
  const [creditCards, setCreditCards] = useState([]);
  const [eWallets, setEWallets] = useState([]);
  const [retails, setRetails] = useState([]);

  useEffect(() => {
    if (add.message === 'Success') {
      navigation.goBack();
      loadPaymentsAction(currentUser.company_id, 1);
    }
  }, [add]);

  React.useEffect(() => {
    if (message === 'Success') {
      const va = data.filter((item) =>
        item.paymentName.toLowerCase().includes('va')
      );
      const cc = data.filter((item) =>
        item.paymentName.toLowerCase().includes('credit card')
      );
      const ewalets = data.filter((item) => {
        const name = item.paymentName.toLowerCase();
        return (
          name.includes('shopeepay') ||
          name.includes('ovo') ||
          name.includes('linkaja') ||
          name.includes('dana')
        );
      });
      const retail = data.filter((item) => {
        const name = item.paymentName?.toLowerCase();
        return (
          name.includes('retail') ||
          name.includes('indomaret') ||
          name.includes('alfamart')
        );
      });
      setRetails(retail);
      setVirtualAccounts(va);
      setCreditCards(cc);
      setEWallets(ewalets);
    }
  }, [data, message]);
  // const styles = StyleSheet.create({
  //   centeredView: {
  //     flex: 1,
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     marginTop: 5,
  //   },
  //   modalView: {
  //     margin: 5,
  //     backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
  //     borderRadius: 20,
  //     padding: 25,
  //     alignItems: 'center',
  //     shadowColor: '#000',
  //     shadowOffset: {
  //       width: 0,
  //       height: 2,
  //     },
  //     shadowOpacity: 0.25,
  //     shadowRadius: 4,
  //     elevation: 5,
  //   },
  //   button: {
  //     borderRadius: 20,
  //     padding: 10,
  //     elevation: 2,
  //   },
  //   buttonCancel: {
  //     borderRadius: 20,
  //     padding: 10,
  //     elevation: 2,
  //     marginRight: 20,
  //     width: 80,
  //   },
  //   buttonSubmit: {
  //     borderRadius: 20,
  //     padding: 10,
  //     elevation: 2,
  //     marginLeft: 20,
  //     width: 80,
  //   },
  //   buttonClose: {
  //     backgroundColor: '#2196F3',
  //   },
  //   buttonModalClose: {
  //     backgroundColor: COLOR_CONSTANT.RED,
  //   },
  //   textStyle: {
  //     color: 'white',
  //     fontWeight: 'bold',
  //     textAlign: 'center',
  //   },
  //   modalText: {
  //     marginBottom: 15,
  //     textAlign: 'center',
  //   },
  // });

  const onChangeCheck = (id) => {
    const idList = [id];
    let array = [];
    let count;
    if (checkedFinal.length > 0) {
      array = [...checkedFinal];
      idList.forEach((z) => {
        const obj = {
          addedfrom: z.addedfrom,
          adjustment: z.adjustment,
          adminnote: z.adminnote,
          allowed_payment_modes: z.allowed_payment_modes,
          billing_city: z.billing_city,
          billing_country: z.billing_country,
          billing_state: z.billing_state,
          billing_street: z.billing_street,
          billing_zip: z.billing_zip,
          cancel_overdue_reminders: z.cancel_overdue_reminders,
          clientid: z.clientid,
          clientnote: z.clientnote,
          code: z.code,
          currency: z.currency,
          custom_recurring: z.custom_recurring,
          cycles: z.cycles,
          date: z.date,
          date_inv: z.date_inv,
          datecreated: z.datecreated,
          datesend: z.datesend,
          deleted_customer_name: z.deleted_customer_name,
          discount_percent: z.discount_percent,
          discount_total: z.discount_total,
          discount_type: z.discount_type,
          duedate: z.duedate,
          full_no: z.full_no,
          hash: z.hash,
          id: z.id,
          include_shipping: z.include_shipping,
          invoice_no_sap: z.invoice_no_sap,
          invoice_type: z.invoice_type,
          is_correction: z.is_correction,
          is_recurring_from: z.is_recurring_from,
          is_select: z.is_select,
          job_id: z.job_id,
          job_detail: z.job_detail,
          last_due_reminder: z.last_due_reminder,
          last_overdue_reminder: z.last_overdue_reminder,
          last_recurring_date: z.last_recurring_date,
          number: z.number,
          number_format: z.number_format,
          order_id: z.order_id,
          order_no: z.order_no,
          parameter_id: z.parameter_id,
          payment_id: z.payment_id,
          prefix: z.prefix,
          project_id: z.project_id,
          recurring: z.recurring,
          recurring_type: z.recurring_type,
          sale_agent: z.sale_agent,
          sent: z.sent,
          shipping_city: z.shipping_city,
          shipping_country: z.shipping_country,
          shipping_state: z.shipping_state,
          shipping_street: z.shipping_street,
          shipping_zip: z.shipping_zip,
          short_link: z.short_link,
          show_quantity_as: z.show_quantity_as,
          show_shipping_on_invoice: z.show_shipping_on_invoice,
          status: z.status,
          status_inv: z.status_inv,
          subscription_id: z.subscription_id,
          subtotal: z.subtotal,
          terms: z.terms,
          token: z.token,
          total: z.total,
          total_cycles: z.total_cycles,
          total_tax: z.total_tax,
        };
        const obj1 = z.total;

        const isExist = checkedFinal.some((w) => w.id == obj.id);

        if (!isExist) {
          array.push(obj);
          count = countFinal + parseInt(obj1, 10);
        } else if (isExist) {
          const replace = checkedFinal.filter((q) => q.id != obj.id);
          array = replace;
          count = countFinal - parseInt(obj1, 10);
        }
      });
    } else {
      array = idList.map((q) => {
        return {
          addedfrom: q.addedfrom,
          adjustment: q.adjustment,
          adminnote: q.adminnote,
          allowed_payment_modes: q.allowed_payment_modes,
          billing_city: q.billing_city,
          billing_country: q.billing_country,
          billing_state: q.billing_state,
          billing_street: q.billing_street,
          billing_zip: q.billing_zip,
          cancel_overdue_reminders: q.cancel_overdue_reminders,
          clientid: q.clientid,
          clientnote: q.clientnote,
          code: q.code,
          currency: q.currency,
          custom_recurring: q.custom_recurring,
          cycles: q.cycles,
          date: q.date,
          date_inv: q.date_inv,
          datecreated: q.datecreated,
          datesend: q.datesend,
          deleted_customer_name: q.deleted_customer_name,
          discount_percent: q.discount_percent,
          discount_total: q.discount_total,
          discount_type: q.discount_type,
          duedate: q.duedate,
          full_no: q.full_no,
          hash: q.hash,
          id: q.id,
          include_shipping: q.include_shipping,
          invoice_no_sap: q.invoice_no_sap,
          invoice_type: q.invoice_type,
          is_correction: q.is_correction,
          is_recurring_from: q.is_recurring_from,
          is_select: q.is_select,
          job_id: q.job_id,
          job_detail: q.job_detail,
          last_due_reminder: q.last_due_reminder,
          last_overdue_reminder: q.last_overdue_reminder,
          last_recurring_date: q.last_recurring_date,
          number: q.number,
          number_format: q.number_format,
          order_id: q.order_id,
          order_no: q.order_no,
          parameter_id: q.parameter_id,
          payment_id: q.payment_id,
          prefix: q.prefix,
          project_id: q.project_id,
          recurring: q.recurring,
          recurring_type: q.recurring_type,
          sale_agent: q.sale_agent,
          sent: q.sent,
          shipping_city: q.shipping_city,
          shipping_country: q.shipping_country,
          shipping_state: q.shipping_state,
          shipping_street: q.shipping_street,
          shipping_zip: q.shipping_zip,
          short_link: q.short_link,
          show_quantity_as: q.show_quantity_as,
          show_shipping_on_invoice: q.show_shipping_on_invoice,
          status: q.status,
          status_inv: q.status_inv,
          subscription_id: q.subscription_id,
          subtotal: q.subtotal,
          terms: q.terms,
          token: q.token,
          total: q.total,
          total_cycles: q.total_cycles,
          total_tax: q.total_tax,
        };
      });
      count = parseInt(idList[0].total, 10);
    }
    setCheckedFinal(array);
    setCountFinal(count);
  };

  const paymentMethod = (val) => {
    setMethod(val);
    setModalPaymentMethod(!modalPaymentMethod);
  };

  const onSubmit = () => {
    const row = {
      invoices: checkedFinal,
      method:
        method == 'DUITKU'
          ? 'duitku'
          : method == 'VA BNI'
          ? 'vabni'
          : method == 'Manual'
          ? 'manual'
          : method,
      total: countFinal,
    };
    savePaymentsAction(row);
  };

  const virtualTransfer = [
    {
      label: 'Duitku',
      value: 'DUITKU',
      status: 'Konfirmasi Pembayaran Otomatis',
    },
    {
      label: 'Virtual Account BNI',
      value: 'VA BNI',
      status: 'Konfirmasi Pembayaran Otomatis',
    },
  ];

  const test = false;
  return (
    <>
      <CommonHeader title="Add New" onBackPressed={() => navigation.goBack()} />
      <ScrollView>
        <View
          style={{
            padding: 20,
          }}
          key="parent"
        >
          <Text style={{ fontFamily: 'Montserrat_600SemiBold', fontSize: 20 }}>
            Invoice
          </Text>
          <ScrollView>
            {checkedFinal.length > 0 &&
              checkedFinal.map((x) => {
                const convertInt = parseInt(x.total, 10);
                const convertCurrency = convertInt
                  .toString()
                  .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
                return (
                  <View key={x.id}>
                    <View
                      style={{
                        backgroundColor: COLOR_CONSTANT.WHITE,
                        borderRadius: 5,
                        overflow: 'hidden',
                        flexDirection: 'row',
                        shadowColor: '#000',
                        shadowOffset: {
                          width: 0,
                          height: 0,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 1,
                        elevation: 5,
                        marginBottom: 10,
                        marginTop: 20,
                      }}
                    >
                      <View
                        style={{
                          backgroundColor: '#0073C3',
                          width: 5,
                        }}
                      />

                      <View
                        style={{
                          justifyContent: 'flex-start',
                          padding: 20,
                        }}
                      >
                        <View style={{ flexDirection: 'row' }}>
                          <View
                            style={{
                              backgroundColor: COLOR_CONSTANT.WHITE,
                              borderRadius: 4,
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'Montserrat_400Regular',
                                fontSize: 11,
                              }}
                            >
                              Order No
                            </Text>
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',
                                fontSize: 17,
                              }}
                            >
                              {x.full_no == null ? '-' : x.full_no}
                            </Text>
                          </View>
                          <View style={{ width: '30%' }} />
                          <View
                            style={{
                              backgroundColor: COLOR_CONSTANT.WHITE,
                              borderRadius: 4,
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'Montserrat_400Regular',

                                fontSize: 11,
                              }}
                            >
                              Invoice No
                            </Text>
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',

                                fontSize: 17,
                              }}
                            >
                              {x.invoice_no_sap == null
                                ? '-'
                                : x.invoice_no_sap}
                            </Text>
                          </View>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 12 }}>
                          <View
                            style={{
                              backgroundColor: COLOR_CONSTANT.WHITE,
                              borderRadius: 4,
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',
                                fontSize: 28,
                              }}
                            >
                              Rp {x.total == null ? '-' : convertCurrency}
                            </Text>
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',
                                fontSize: 12,
                              }}
                            >
                              tax incl.
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              })}
          </ScrollView>

          <TouchableOpacity onPress={() => setModalVisible(true)}>
            <View
              style={{
                marginTop: 20,
                minHeight: 40,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: '#8A8A8A',
                borderStyle: 'dashed',
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 14,
                }}
              >
                <AddIcon />
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat_400Regular',
                    fontSize: 14,
                    textAlign: 'center',
                  }}
                >
                  Pilih Invoice
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          {/* <Card
            style={{
              flex: 1,
              marginTop: 20,
              backgroundColor: COLOR_CONSTANT.WHITE,
            }}
          >
            <Card.Title
              title=""
              left={(props) => (
                <Button
                  {...props}
                  onPress={() => setModalVisible(true)}
                  style={{
                    borderRadius: 10,
                    borderWidth: 1,
                    borderColor: COLOR_CONSTANT.WHITE,
                    backgroundColor: COLOR_CONSTANT.GREEN,
                    width: 130,
                  }}
                >
                  <Text style={{ color: COLOR_CONSTANT.WHITE, fontSize: 12 }}>
                    Pilih Invoice
                  </Text>
                </Button>
              )}
              right={(props) => (
                <>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}
                  >
                    <Pressable onPress={() => paymentMethod()}>
                      <Button
                        {...props}
                        style={{
                          borderRadius: 10,
                          borderWidth: 1,
                          borderColor: COLOR_CONSTANT.WHITE,
                          backgroundColor: COLOR_CONSTANT.CYAN,
                        }}
                      >
                        <Text
                          style={{ color: COLOR_CONSTANT.WHITE, fontSize: 12 }}
                        >
                          Pilih Payment
                        </Text>
                      </Button>
                    </Pressable>

                    <Text
                      style={{
                        fontFamily: 'Montserrat_700Bold',
                        alignItems: 'center',
                        textAlign: 'center',
                        fontSize: 12,
                        color: COLOR_CONSTANT.WHITE,
                        padding: 5,
                      }}
                    >
                      -
                    </Text>
                  </View>
                </>
              )}
            />

            <Divider style={{ backgroundColor: COLOR_CONSTANT.BLACK }} />
            <Card.Content>
              {checkedFinal.length > 0 && (
                <>
                  {checkedFinal.map((q) => {
                    const convertInt = parseInt(q.total, 10);
                    const convertCurrency = convertInt
                      .toString()
                      .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
                    return (
                      <View
                        key={q.id}
                        style={{
                          backgroundColor: COLOR_CONSTANT.WHITE,
                          height: 150,
                          borderRadius: 5,
                          overflow: 'hidden',
                          flex: 1,
                          flexDirection: 'row',
                          padding: 15,
                        }}
                      >
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: 'row',
                              marginTop: 4,
                            }}
                          >
                            <View style={{ flex: 1 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_500Medium',
                                  fontSize: 13,
                                }}
                              >
                                Invoice No
                              </Text>
                            </View>
                            <View style={{ flex: 1, marginRight: 5 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_700Bold',
                                  textAlign: 'right',
                                  fontSize: 13,
                                  color: COLOR_CONSTANT.DARKGRAY,
                                }}
                              >
                                {q.invoice_no_sap || '-'}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: 'row',
                              marginTop: 4,
                            }}
                          >
                            <View style={{ flex: 1 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_500Medium',
                                  fontSize: 13,
                                }}
                              >
                                Job ID
                              </Text>
                            </View>
                            <View style={{ flex: 1, marginRight: 5 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_700Bold',
                                  textAlign: 'right',
                                  fontSize: 13,
                                  color: COLOR_CONSTANT.DARKGRAY,
                                }}
                              >
                                {q.job_id == ''
                                  ? '-'
                                  : q.job_id == null
                                  ? '-'
                                  : q.job_id}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: 'row',
                              marginTop: 4,
                            }}
                          >
                            <View style={{ flex: 1 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_500Medium',
                                  fontSize: 13,
                                }}
                              >
                                Job Detail
                              </Text>
                            </View>
                            <View style={{ flex: 1, marginRight: 5 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_700Bold',
                                  textAlign: 'right',
                                  fontSize: 13,
                                  color: COLOR_CONSTANT.DARKGRAY,
                                }}
                              >
                                {q.job_detail == ''
                                  ? '-'
                                  : q.job_detail == null
                                  ? '-'
                                  : q.job_detail}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: 'row',
                              marginTop: 4,
                            }}
                          >
                            <View style={{ flex: 1 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_500Medium',
                                  fontSize: 13,
                                }}
                              >
                                Nilai Invoice
                              </Text>
                            </View>
                            <View style={{ flex: 1, marginRight: 5 }}>
                              <Text
                                style={{
                                  fontFamily: 'Montserrat_700Bold',
                                  textAlign: 'right',
                                  fontSize: 13,
                                  color: COLOR_CONSTANT.DARKGRAY,
                                }}
                              >
                                IDR {q.total == null ? '-' : convertCurrency}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    );
                  })}
                </>
              )}
              <Divider style={{ backgroundColor: COLOR_CONSTANT.BLACK }} />

              <View
                style={{
                  backgroundColor: COLOR_CONSTANT.WHITE,
                  height: 100,
                  borderRadius: 5,
                  overflow: 'hidden',
                  flex: 1,
                  flexDirection: 'row',
                  padding: 15,
                }}
              >
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_800ExtraBold',
                          fontSize: 14,
                        }}
                      >
                        Total Payment
                      </Text>
                    </View>
                    <View style={{ flex: 1, marginRight: 5 }}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_800ExtraBold',
                          textAlign: 'right',
                          fontSize: 13,
                          color: COLOR_CONSTANT.DARKGRAY,
                        }}
                      >
                        IDR{' '}
                        {countFinal
                          .toString()
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_800ExtraBold',
                          fontSize: 14,
                        }}
                      >
                        Payment Method
                      </Text>
                    </View>
                    <View style={{ flex: 1, marginRight: 5 }}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_800ExtraBold',
                          textAlign: 'right',
                          fontSize: 13,
                          color: COLOR_CONSTANT.DARKGRAY,
                        }}
                      >
                        {method}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Button
                  onPress={() => onSubmit()}
                  style={{
                    alignItems: 'center',
                    borderRadius: 10,
                    borderWidth: 1,
                    borderColor: COLOR_CONSTANT.WHITE,
                    backgroundColor: COLOR_CONSTANT.BLUE,
                    width: 270,
                    height: 40,
                  }}
                >
                  <Text style={{ color: COLOR_CONSTANT.WHITE }}>
                    Lanjutkan Pembayaran
                  </Text>
                </Button>
              </View>
            </Card.Content>
          </Card> */}
        </View>
      </ScrollView>

      <View
        style={{
          // flex: 1,
          // minHeight: '20%',
          backgroundColor: COLOR_CONSTANT.WHITE,
        }}
      >
        <View
          style={{
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 14,
          }}
        >
          <Text style={{ fontFamily: 'Montserrat_400Regular', fontSize: 12 }}>
            Total Nominal Payment
          </Text>
          <View
            style={{
              backgroundColor: '#E4EFF6',
              marginTop: 5,
              minHeight: 72,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: '#0073C3',
              borderStyle: 'dashed',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_500Medium',
                fontSize: 24,
                textAlign: 'center',
              }}
            >
              Rp{' '}
              {countFinal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}
            </Text>
          </View>
          <View
            style={{
              backgroundColor: '#FFFFFF',
              marginTop: 15,
              minHeight: 52,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: '#E9E9E9',
              justifyContent: 'center',
            }}
          >
            <TouchableOpacity onPress={() => setModalPaymentMethod(true)}>
              <View style={{ flexDirection: 'row', padding: 15 }}>
                <View style={{ textAlign: 'center', marginRight: 10 }}>
                  <CreditCard />
                </View>
                <View style={{ justifyContent: 'center', width: '80%' }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 16,
                      textAlign: 'center',
                    }}
                  >
                    {method == 'DUITKU'
                      ? 'Duitku'
                      : method == 'VA BNI'
                      ? 'Virtual Account BNI'
                      : method == 'Manual'
                      ? 'Transfer Bank'
                      : method}
                  </Text>
                </View>
                <View
                  style={{
                    textAlign: 'center',
                    // marginLeft: '20%',
                    paddingTop: 4,
                  }}
                >
                  <ArrowRight />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <Button
            onPress={() => onSubmit()}
            style={{
              backgroundColor: '#0073C3',
              borderRadius: 5,
              minHeight: 52,
              marginTop: 24,
              justifyContent: 'center',
            }}
            disabled={test == true}
          >
            <Text
              style={{
                textAlign: 'center',
                color: COLOR_CONSTANT.WHITE,
                fontFamily: 'Montserrat_500Medium',
                fontSize: 20,
              }}
            >
              Simpan
            </Text>
          </Button>
        </View>
      </View>

      <Modal
        animationType="slide"
        transparent
        visible={modalPaymentMethod}
        onRequestClose={() => {
          Alert('Modal has been closed.');
          setModalPaymentMethod(!modalPaymentMethod);
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 5,
            padding: 20,
          }}
        >
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.WHITE,
              padding: 20,
              // alignItems: 'center',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
              elevation: 5,
            }}
          >
            <View
              style={{
                marginBottom: 10,
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_600SemiBold',
                  fontSize: 20,
                }}
              >
                Pilih Invoice
              </Text>
            </View>
            <View
              style={{
                marginBottom: 10,
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_500Medium',
                  fontSize: 12,
                }}
              >
                Virtual Transfer
              </Text>
            </View>
            {virtualTransfer.map((q) => (
              <View key={q.value}>
                <Pressable
                  onPress={() => {
                    if (q.value === 'DUITKU') {
                      loadDuitkuMethodsAction(countFinal);
                      setModalPaymentDuitku(true);
                      setModalPaymentMethod(false);
                      paymentMethod(q.value);
                    } else {
                      paymentMethod(q.value);
                    }
                  }}
                >
                  <View
                    style={{
                      backgroundColor: COLOR_CONSTANT.WHITE,
                      borderRadius: 5,
                      overflow: 'hidden',
                      flexDirection: 'row',
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 0,
                      },
                      shadowOpacity: 0.25,
                      shadowRadius: 1,
                      elevation: 5,
                      marginBottom: 10,
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: '#0073C3',
                        width: 5,
                      }}
                    />

                    <View style={{ justifyContent: 'flex-start', padding: 20 }}>
                      <View
                        style={{
                          backgroundColor: COLOR_CONSTANT.WHITE,
                          borderRadius: 4,
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: 'Montserrat_600SemiBold',
                            fontSize: 24,
                          }}
                        >
                          {q.label}
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Montserrat_400Regular',
                            fontSize: 12,
                          }}
                        >
                          {q.status}
                        </Text>
                      </View>
                    </View>
                  </View>
                </Pressable>
              </View>
            ))}
            <View
              style={{
                marginBottom: 10,
                marginTop: 10,
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_500Medium',
                  fontSize: 12,
                }}
              >
                Transfer Bank
              </Text>
            </View>

            <Pressable onPress={() => paymentMethod('Manual')}>
              <View
                style={{
                  backgroundColor: COLOR_CONSTANT.WHITE,
                  borderRadius: 5,
                  overflow: 'hidden',
                  flexDirection: 'row',
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 0,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 1,
                  elevation: 5,
                }}
              >
                <View
                  style={{
                    backgroundColor: '#0073C3',
                    width: 5,
                  }}
                />

                <View style={{ justifyContent: 'flex-start', padding: 20 }}>
                  <View
                    style={{
                      backgroundColor: COLOR_CONSTANT.WHITE,
                      borderRadius: 4,
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'Montserrat_600SemiBold',
                        fontSize: 24,
                      }}
                    >
                      Transfer Bank
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'Montserrat_400Regular',
                        fontSize: 12,
                      }}
                    >
                      Pengguna harus mengunggah bukti pembayaran dan bukti
                      potong pajak secara manual
                    </Text>
                  </View>
                </View>
              </View>
            </Pressable>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 5,
            padding: 20,
          }}
        >
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.WHITE,
              padding: 20,
              // alignItems: 'center',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
              elevation: 5,
            }}
          >
            <View
              style={{
                marginBottom: 10,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <Text
                  style={{
                    fontFamily: 'Montserrat_600SemiBold',
                    fontSize: 20,
                  }}
                >
                  Pilih Invoice
                </Text>
                {checkedFinal.length > 0 && (
                  <View
                    style={{ alignContent: 'center', justifyContent: 'center' }}
                  >
                    <Text
                      style={{
                        fontFamily: 'Montserrat_600SemiBold',
                        fontSize: 12,
                        color: '#59C318',
                      }}
                    >
                      {checkedFinal.length} invoice dipilih
                    </Text>
                  </View>
                )}
              </View>
            </View>

            {invoice.loading ? (
              <View
                style={{
                  padding: 24,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
              >
                <LoadingView />
              </View>
            ) : (
              <>
                <ScrollView>
                  {invoice?.data.map((q) => {
                    const convertInt = parseInt(q.total, 10);
                    const convertCurrency = convertInt
                      .toString()
                      .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
                    return (
                      <Pressable key={q.hash} onPress={() => onChangeCheck(q)}>
                        <View
                          style={{
                            backgroundColor: COLOR_CONSTANT.WHITE,
                            borderRadius: 5,
                            overflow: 'hidden',
                            flexDirection: 'row',
                            shadowColor: '#000',
                            shadowOffset: {
                              width: 0,
                              height: 0,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 1,
                            elevation: 5,
                            marginBottom: 10,
                          }}
                        >
                          <View
                            style={{
                              backgroundColor: '#0073C3',
                              width: 5,
                            }}
                          />

                          <View
                            style={{
                              justifyContent: 'flex-start',
                              padding: 20,
                            }}
                          >
                            <View style={{ flexDirection: 'row' }}>
                              <View
                                style={{
                                  backgroundColor: COLOR_CONSTANT.WHITE,
                                  borderRadius: 4,
                                }}
                              >
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_400Regular',
                                    fontSize: 11,
                                  }}
                                >
                                  Order No
                                </Text>
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_600SemiBold',
                                    fontSize: 17,
                                  }}
                                >
                                  {q.full_no == null ? '-' : q.full_no}
                                </Text>
                              </View>
                              <View style={{ width: '30%' }} />
                              <View
                                style={{
                                  backgroundColor: COLOR_CONSTANT.WHITE,
                                  borderRadius: 4,
                                }}
                              >
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_400Regular',

                                    fontSize: 11,
                                  }}
                                >
                                  Invoice No
                                </Text>
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_600SemiBold',

                                    fontSize: 17,
                                  }}
                                >
                                  {q.invoice_no_sap == null
                                    ? '-'
                                    : q.invoice_no_sap}
                                </Text>
                              </View>
                            </View>

                            <View
                              style={{ flexDirection: 'row', marginTop: 12 }}
                            >
                              <View
                                style={{
                                  backgroundColor: COLOR_CONSTANT.WHITE,
                                  borderRadius: 4,
                                }}
                              >
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_600SemiBold',
                                    fontSize: 28,
                                  }}
                                >
                                  Rp {q.total == null ? '-' : convertCurrency}
                                </Text>
                                <Text
                                  style={{
                                    fontFamily: 'Montserrat_600SemiBold',
                                    fontSize: 12,
                                  }}
                                >
                                  tax incl.
                                </Text>
                              </View>
                              <View style={{ minWidth: '10%' }} />
                              {checkedFinal.some((x) => x.id == q.id) && (
                                <Checklist />
                              )}
                            </View>
                          </View>
                        </View>
                      </Pressable>
                    );
                  })}
                </ScrollView>
              </>
            )}
            <Button
              onPress={() => setModalVisible(!modalVisible)}
              style={{
                backgroundColor: '#0073C3',
                borderRadius: 5,
                minHeight: 52,
                marginTop: 24,
                justifyContent: 'center',
              }}
              disabled={checkedFinal.length < 1}
            >
              <Text
                style={{
                  textAlign: 'center',
                  color: COLOR_CONSTANT.WHITE,
                  fontFamily: 'Montserrat_500Medium',
                  fontSize: 20,
                }}
              >
                Pilih
              </Text>
            </Button>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent
        visible={modalPaymentDuitku}
        onRequestClose={() => {
          Alert('Modal has been closed.');
          setModalPaymentDuitku(!modalPaymentDuitku);
        }}
      >
        <ScrollView>
          <View
            style={{
              flex: 1,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 45,
              paddingBottom: 41,
              backgroundColor: COLOR_CONSTANT.WHITE,
            }}
          >
            <View>
              <Text
                style={{
                  fontFamily: 'Montserrat_600SemiBold',
                  fontSize: 20,
                }}
              >
                Pilih Metode Pembayaran
              </Text>
            </View>

            {creditCards.length > 0 && (
              <View style={{ marginTop: 20 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 12,
                    }}
                  >
                    Credit Card
                  </Text>
                </View>
                {creditCards.length > 0 &&
                  creditCards.map((q) => {
                    return (
                      <View
                        key={q.paymentMethod}
                        style={{
                          backgroundColor: COLOR_CONSTANT.WHITE,
                          borderRadius: 5,
                          overflow: 'hidden',
                          flexDirection: 'row',
                          shadowColor: '#000',
                          shadowOffset: {
                            width: 0,
                            height: 0,
                          },
                          shadowOpacity: 0.25,
                          shadowRadius: 1,
                          elevation: 5,
                          marginTop: 5,
                          marginBottom: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: '#0073C3',
                            width: 5,
                          }}
                        />
                        <View
                          style={{ justifyContent: 'flex-start', padding: 20 }}
                        >
                          <Image
                            style={{ width: 68, height: 44 }}
                            source={{
                              uri: `${q.paymentImage}`,
                            }}
                          />
                        </View>

                        <View
                          style={{ justifyContent: 'flex-start', padding: 15 }}
                        >
                          <View
                            style={{
                              backgroundColor: COLOR_CONSTANT.WHITE,
                              borderRadius: 4,
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',
                                fontSize: 24,
                              }}
                            >
                              {q.paymentName}
                            </Text>
                            <Text
                              style={{
                                fontFamily: 'Montserrat_400Regular',
                                fontSize: 12,
                              }}
                            >
                              Rp {q.totalFee}-,
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  })}
              </View>
            )}

            {virtualAccounts.length > 0 && (
              <View style={{ marginTop: 10 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 12,
                    }}
                  >
                    Virtual Account
                  </Text>
                </View>
                {virtualAccounts.length > 0 &&
                  virtualAccounts.map((q) => {
                    return (
                      <View
                        key={q.paymentMethod}
                        style={{
                          backgroundColor: COLOR_CONSTANT.WHITE,
                          borderRadius: 5,
                          overflow: 'hidden',
                          flexDirection: 'row',
                          shadowColor: '#000',
                          shadowOffset: {
                            width: 0,
                            height: 0,
                          },
                          shadowOpacity: 0.25,
                          shadowRadius: 1,
                          elevation: 5,
                          marginTop: 5,
                          marginBottom: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: '#0073C3',
                            width: 5,
                          }}
                        />
                        <View
                          style={{ justifyContent: 'flex-start', padding: 20 }}
                        >
                          <Image
                            style={{ width: 68, height: 44 }}
                            source={{
                              uri: `${q.paymentImage}`,
                            }}
                          />
                        </View>

                        <View
                          style={{ justifyContent: 'flex-start', padding: 15 }}
                        >
                          <View
                            style={{
                              backgroundColor: COLOR_CONSTANT.WHITE,
                              borderRadius: 4,
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',
                                fontSize: 24,
                              }}
                            >
                              {q.paymentName}
                            </Text>
                            <Text
                              style={{
                                fontFamily: 'Montserrat_400Regular',
                                fontSize: 12,
                              }}
                            >
                              Rp {q.totalFee}-,
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  })}
              </View>
            )}

            {eWallets.length > 0 && (
              <View style={{ marginTop: 10 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 12,
                    }}
                  >
                    E-Wallets
                  </Text>
                </View>
                {eWallets.length > 0 &&
                  eWallets.map((q) => {
                    return (
                      <View
                        key={q.paymentMethod}
                        style={{
                          backgroundColor: COLOR_CONSTANT.WHITE,
                          borderRadius: 5,
                          overflow: 'hidden',
                          flexDirection: 'row',
                          shadowColor: '#000',
                          shadowOffset: {
                            width: 0,
                            height: 0,
                          },
                          shadowOpacity: 0.25,
                          shadowRadius: 1,
                          elevation: 5,
                          marginTop: 5,
                          marginBottom: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: '#0073C3',
                            width: 5,
                          }}
                        />
                        <View
                          style={{ justifyContent: 'flex-start', padding: 20 }}
                        >
                          <Image
                            style={{ width: 68, height: 44 }}
                            source={{
                              uri: `${q.paymentImage}`,
                            }}
                          />
                        </View>

                        <View
                          style={{ justifyContent: 'flex-start', padding: 15 }}
                        >
                          <View
                            style={{
                              backgroundColor: COLOR_CONSTANT.WHITE,
                              borderRadius: 4,
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',
                                fontSize: 24,
                              }}
                            >
                              {q.paymentName}
                            </Text>
                            <Text
                              style={{
                                fontFamily: 'Montserrat_400Regular',
                                fontSize: 12,
                              }}
                            >
                              Rp {q.totalFee}-,
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  })}
              </View>
            )}

            {retails.length > 0 && (
              <View style={{ marginTop: 10 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 12,
                    }}
                  >
                    Retails
                  </Text>
                </View>
                {retails.length > 0 &&
                  retails.map((q) => {
                    return (
                      <View
                        key={q.paymentMethod}
                        style={{
                          backgroundColor: COLOR_CONSTANT.WHITE,
                          borderRadius: 5,
                          overflow: 'hidden',
                          flexDirection: 'row',
                          shadowColor: '#000',
                          shadowOffset: {
                            width: 0,
                            height: 0,
                          },
                          shadowOpacity: 0.25,
                          shadowRadius: 1,
                          elevation: 5,
                          marginTop: 5,
                          marginBottom: 10,
                        }}
                      >
                        <View
                          style={{
                            backgroundColor: '#0073C3',
                            width: 5,
                          }}
                        />
                        <View
                          style={{ justifyContent: 'flex-start', padding: 20 }}
                        >
                          <Image
                            style={{ width: 68, height: 44 }}
                            source={{
                              uri: `${q.paymentImage}`,
                            }}
                          />
                        </View>

                        <View
                          style={{ justifyContent: 'flex-start', padding: 15 }}
                        >
                          <View
                            style={{
                              backgroundColor: COLOR_CONSTANT.WHITE,
                              borderRadius: 4,
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'Montserrat_600SemiBold',
                                fontSize: 24,
                              }}
                            >
                              {q.paymentName}
                            </Text>
                            <Text
                              style={{
                                fontFamily: 'Montserrat_400Regular',
                                fontSize: 12,
                              }}
                            >
                              Rp {q.totalFee}-,
                            </Text>
                          </View>
                        </View>
                      </View>
                    );
                  })}
              </View>
            )}
          </View>
        </ScrollView>
      </Modal>
    </>
  );
};

const mapStateToProps = ({ paymentApp, authApp }) => {
  const { invoice, add, duitku } = paymentApp;
  const { currentUser } = authApp;

  return {
    invoice,
    add,
    duitku,
    currentUser,
  };
};

export default connect(mapStateToProps, {
  savePaymentsAction: savePayments,
  loadPaymentsAction: loadPayments,
  loadDuitkuMethodsAction: loadDuitkuMethods,
})(AddNew);
