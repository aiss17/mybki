// @ts-nocheck
/* eslint-disable eqeqeq */
/* eslint-disable no-nested-ternary */
import moment from 'moment';
import React from 'react';
// import moment from 'moment';

import { Text, View } from 'react-native';
import PaymentSmall from '../../../assets/icons/icon-payment-bg.svg';
// import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
// import {
//   faMoneyCheck,
//   faCheckCircle,
//   faCheckDouble,
// } from '@fortawesome/free-solid-svg-icons';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';

const ListItem = ({ data }) => {
  // const paymentStatusColor = (param) => {
  //   switch (param) {
  //     case '1':
  //       return COLOR_CONSTANT.DARKORANGE;
  //     case '2':
  //       return COLOR_CONSTANT.RED;
  //     default:
  //       return COLOR_CONSTANT.GREEN;
  //   }
  // };
  const paymentStatusLabel = (param) => {
    switch (param) {
      case '1':
        return 'Pending';
      case '2':
        return 'Unpaid';
      default:
        return 'Paid';
    }
  };
  // const confirmStatusColor = (param) => {
  //   switch (param) {
  //     case '1':
  //       return COLOR_CONSTANT.DARKORANGE;
  //     case '2':
  //       return COLOR_CONSTANT.GREEN;
  //     default:
  //       return COLOR_CONSTANT.DARKBLUE;
  //   }
  // };
  const confirmStatusLabel = (param) => {
    switch (param) {
      case '1':
        return 'Waiting Confirmation';
      case '2':
        return 'Review Confirmation';
      default:
        return 'Confirmed';
    }
  };
  // const paymentMethodColor = (param) => {
  //   switch (param) {
  //     case 'vabni':
  //       return COLOR_CONSTANT.DARKORANGE;
  //     case 'duitku':
  //       return COLOR_CONSTANT.GREEN;
  //     default:
  //       return COLOR_CONSTANT.BLUE;
  //   }
  // };
  const paymentMethodLabel = (param) => {
    switch (param) {
      case 'vabni':
        return 'VA BNI';
      case 'duitku':
        return 'Duitku';
      default:
        return 'Manual Transfer';
    }
  };
  return (
    <View
      style={[
        {
          borderRadius: 5,
          marginBottom: 10,
          marginTop: 10,
          marginLeft: 20,
          marginRight: 20,
        },
        STYLE_CONSTANT.SHADOW2,
      ]}
    >
      <View
        style={{
          backgroundColor: COLOR_CONSTANT.WHITE,
          height: 110,
          borderRadius: 5,
          overflow: 'hidden',
          flex: 1,
          flexDirection: 'row',
        }}
      >
        <View
          style={{
            backgroundColor: data.status == 1 ? '#DF5C35' : '#59C318',
            width: 5,
          }}
        />

        <View style={{ justifyContent: 'center', marginRight: 5 }}>
          <View
            style={{
              backgroundColor: COLOR_CONSTANT.WHITE,
              margin: 0,
              borderRadius: 4,
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: 20,
            }}
          >
            <Text style={{ fontFamily: 'Montserrat_400Regular', fontSize: 12 }}>
              {data.paymentdate == null
                ? '-'
                : moment(data.paymentdate).format('DD MMMM YYYY')}
            </Text>
            <Text
              style={{
                fontFamily: 'Montserrat_600SemiBold',
                fontSize: 18,
                paddingBottom: 7,
              }}
            >
              {data.paymentcode == null ? '-' : data.paymentcode}
            </Text>
            <View
              style={{
                borderRadius: 10,
                padding: 3,
                backgroundColor: data.status == 1 ? '#DF5C35' : '#59C318',
              }}
            >
              <Text
                style={{
                  fontFamily: 'Montserrat_600SemiBold',
                  fontSize: 12,
                  marginLeft: 10,
                  marginRight: 10,
                  color: COLOR_CONSTANT.WHITE,
                }}
              >
                {data.status == null ? '-' : paymentStatusLabel(data.status)}
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            marginLeft: 30,
            minWidth: '40%',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}
          >
            <PaymentSmall />
          </View>
          <View
            style={{
              justifyContent: 'flex-start',
            }}
          >
            <Text
              style={{
                fontFamily: 'Montserrat_400Regular',
                fontSize: 12,
                color: data.confirmation_status == 1 ? '#DF5C35' : '#59C318',
              }}
            >
              {data.confirmation_status == null
                ? '-'
                : confirmStatusLabel(data.confirmation_status)}
            </Text>
            <Text
              style={{
                fontFamily: 'Montserrat_600SemiBold',
                fontSize: 16,
                color: COLOR_CONSTANT.BLACK,
              }}
            >
              {paymentMethodLabel(data.methodname)}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ListItem;
