/* eslint-disable no-nested-ternary */
// @ts-nocheck
import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Text, View, ScrollView } from 'react-native';
import { Button } from 'react-native-paper';
import CommonHeader from '../../../../components/common/common-header';
import LoadingView from '../../../../components/LoadingView';
import IconKonfirmasi from '../../../../assets/icons/konfirmasi-bayar.svg';
import { COLOR_CONSTANT } from '../../../../constants/style-constant';
import { loadPaymentsDetail } from '../../../../redux/payment/action';

const NonManual = ({ navigation, detail, loadPaymentsDetailAction }) => {
  const { data, message } = detail;
  return (
    <>
      <CommonHeader
        title="Payment Confirmation"
        onBackPressed={() => navigation.goBack()}
      />
      {message == null ? (
        <View
          style={{
            padding: 24,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <LoadingView />
        </View>
      ) : (
        message === 'Success' && (
          <ScrollView>
            <View
              style={{
                flex: 1,
                overflow: 'hidden',
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 20,
                paddingTop: 55,
                backgroundColor: '#E9E9E9',
              }}
            >
              <View style={{ justifyContent: 'center' }}>
                <View style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_600SemiBold',
                      fontSize: 28,
                    }}
                  >
                    Konfirmasi
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_600SemiBold',
                      fontSize: 28,
                    }}
                  >
                    Pembayaran
                  </Text>
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 27,
                  }}
                >
                  <IconKonfirmasi />
                </View>
                <View
                  style={{
                    backgroundColor: COLOR_CONSTANT.WHITE,
                    marginTop: 27,
                    paddingLeft: 20,
                    paddingRight: 20,
                    justifyContent: 'center',
                  }}
                >
                  <View style={{ marginTop: 16, alignItems: 'center' }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat_400Regular',
                        fontSize: 12,
                      }}
                    >
                      {moment(data?.paymentdate).format('DD-MMM-yyyy')}
                    </Text>
                  </View>
                  <View style={{ alignItems: 'center' }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat_600SemiBold',
                        fontSize: 28,
                      }}
                    >
                      {data?.paymentcode}
                    </Text>
                  </View>
                  <View style={{ marginTop: 20, alignItems: 'center' }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat_400Regular',
                        fontSize: 12,
                      }}
                    >
                      Nominal
                    </Text>
                  </View>
                  <View
                    style={{
                      marginTop: 5,
                      backgroundColor: 'rgba(62, 159, 227, 0.1)',
                      borderWidth: 1,
                      borderStyle: 'dashed',
                      borderColor: '#0073C3',
                      height: 72,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 5,
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'Montserrat_500Medium',
                        fontSize: 24,
                      }}
                    >
                      Rp{' '}
                      {detail?.data?.total == null
                        ? '-'
                        : detail?.data?.total
                            .toString()
                            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}
                      -,
                    </Text>
                  </View>
                  <View style={{ marginTop: 20, justifyContent: 'flex-start' }}>
                    <View>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_400Regular',
                          fontSize: 12,
                        }}
                      >
                        Nama Rekening Pengirim
                      </Text>
                    </View>
                    <View style={{ marginTop: 2 }}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_600SemiBold',
                          fontSize: 18,
                        }}
                      >
                        {detail?.data?.sender}
                      </Text>
                    </View>
                  </View>
                  <View style={{ marginTop: 10, justifyContent: 'flex-start' }}>
                    <View>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_400Regular',
                          fontSize: 12,
                        }}
                      >
                        Metode Pembayaran
                      </Text>
                    </View>
                    <View style={{ marginTop: 2, marginBottom: 27 }}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat_600SemiBold',
                          fontSize: 18,
                        }}
                      >
                        {detail?.data?.methodname === 'vabni'
                          ? 'VA BNI'
                          : detail?.data?.methodname === 'duitku'
                          ? 'Duitku'
                          : 'Transfer Bank'}
                      </Text>
                    </View>
                  </View>
                </View>
                <Button
                  onPress={() => {
                    loadPaymentsDetailAction(data?.id);
                    navigation.navigate('PaymentConfirmationNonManual');
                  }}
                  style={{
                    backgroundColor: '#1C4469',
                    borderRadius: 5,
                    minHeight: 52,
                    marginTop: 62,
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    style={{
                      textAlign: 'center',
                      color: COLOR_CONSTANT.WHITE,
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 20,
                    }}
                  >
                    Konfirmasi
                  </Text>
                </Button>
              </View>
            </View>
          </ScrollView>
        )
      )}
    </>
  );
};

const mapStateToProps = ({ paymentApp, authApp }) => {
  const { detail } = paymentApp;
  const { currentUser } = authApp;

  return {
    detail,
    currentUser,
  };
};

export default connect(mapStateToProps, {
  loadPaymentsDetailAction: loadPaymentsDetail,
})(NonManual);
