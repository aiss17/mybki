/* eslint-disable eqeqeq */
// @ts-nocheck
/* eslint-disable no-useless-escape */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { useActionSheet } from '@expo/react-native-action-sheet';
import { connect } from 'react-redux';
import { Button } from 'react-native-paper';
// import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
// import { faMoneyCheck } from '@fortawesome/free-solid-svg-icons';
import * as mime from 'mime';
import {
  takePicture,
  pickPictureFromGallery,
} from '../../../../helpers/cameraHelper';
import {
  getInfoAsync,
  readAsStringAsync,
} from '../../../../helpers/fileHelper';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../../constants/style-constant';
import CommonHeader from '../../../../components/common/common-header';
import {
  loadPayments,
  savePaymentsDetail,
} from '../../../../redux/payment/action';
import LoadingView from '../../../../components/LoadingView';
import IconCredit from '../../../../assets/icons/icon-payment-large.svg';
import IconAdd from '../../../../assets/icons/icon-add.svg';
import IconChecklist from '../../../../assets/icons/icon-checklist-large.svg';

const Manual = ({
  navigation,
  selectedPayment,
  detail,
  currentUser,
  savePaymentsDetailAction,
  loadPaymentsAction,
}) => {
  const { showActionSheetWithOptions } = useActionSheet();
  useEffect(() => {}, [selectedPayment]);

  const [namePembayaran, setNamePembayaran] = useState();
  const [loading, setLoading] = useState(false);
  const [namePajak, setNamePajak] = useState();
  const [proofPayment, setProofPayment] = useState();
  const [taxSlip, setTaxSlip] = useState();

  const doUpload = async (imageUri, status) => {
    if (status === 'Pembayaran') {
      if (imageUri != null) {
        await getInfoAsync(imageUri).then(async () => {
          await readAsStringAsync(imageUri, {
            encoding: 'base64',
          }).then((base64) => {
            const fileName = imageUri.replace(/^.*[\\\/]/, '');
            const contentType = mime.getType(fileName);
            const payment = `data:${contentType};base64,${base64}`;
            setNamePembayaran(fileName);
            setProofPayment(payment);
          });
        });
      }
    } else if (status === 'Pajak') {
      if (imageUri != null) {
        await getInfoAsync(imageUri).then(async () => {
          await readAsStringAsync(imageUri, {
            encoding: 'base64',
          }).then((base64) => {
            const fileName = imageUri.replace(/^.*[\\\/]/, '');
            const contentType = mime.getType(fileName);
            const tax = `data:${contentType};base64,${base64}`;
            setNamePajak(fileName);
            setTaxSlip(tax);
          });
        });
      }
    }
  };

  const openCameraOrCalleryChooser = (status) => {
    const options = ['Take Photo', 'Choose Photo', 'Cancel'];
    const cancelButtonIndex = 2;

    showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
      },
      async (buttonIndex) => {
        if (buttonIndex === 0) {
          takePicture((uri) => doUpload(uri, status));
        } else if (buttonIndex === 1) {
          pickPictureFromGallery((uri) => doUpload(uri, status));
        }
      }
    );
  };

  const onSubmit = () => {
    const datapost = {
      proof_of_payment: proofPayment,
      tax_slip: taxSlip,
      id: detail?.data?.id,
    };
    savePaymentsDetailAction(datapost);
  };

  return (
    <>
      <CommonHeader
        title="Payment Confirmation"
        onBackPressed={() => navigation.goBack()}
      />
      {detail?.message == null ? (
        <View
          style={{
            padding: 24,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <LoadingView />
        </View>
      ) : (
        detail?.message === 'Success' && (
          <ScrollView>
            <View
              style={{
                flex: 1,
                overflow: 'hidden',
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 55,
                paddingBottom: 30,
                backgroundColor: COLOR_CONSTANT.WHITE,
              }}
            >
              <View style={{ justifyContent: 'center' }}>
                <View style={{ alignItems: 'center' }}>
                  <IconCredit />
                </View>
                <View style={{ alignItems: 'center', marginTop: 10 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    {moment(detail?.data?.paymentdate).format('DD-MMM-yyyy')}
                  </Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_600SemiBold',
                      fontSize: 28,
                    }}
                  >
                    {detail?.data?.paymentcode}
                  </Text>
                </View>
                <View style={{ alignItems: 'center', marginTop: 20 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    Nominal
                  </Text>
                </View>
                <View
                  style={{
                    marginTop: 5,
                    borderWidth: 1,
                    borderColor: '#0073C3',
                    borderRadius: 5,
                    borderStyle: 'dashed',
                    backgroundColor: '#E4EFF6',
                    minHeight: 72,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 24,
                    }}
                  >
                    Rp{' '}
                    {detail?.data?.total == null
                      ? '-'
                      : detail?.data?.total
                          .toString()
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.')}
                    -,
                  </Text>
                </View>
              </View>
              <View style={{ marginTop: 20 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    Nama Rekening Pengirim
                  </Text>
                </View>
                <View style={{ marginTop: 2 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_600SemiBold',
                      fontSize: 18,
                    }}
                  >
                    {detail?.data?.sender}
                  </Text>
                </View>
              </View>
              <View style={{ marginTop: 10 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    Metode Pembayaran
                  </Text>
                </View>
                <View style={{ marginTop: 2 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_600SemiBold',
                      fontSize: 18,
                    }}
                  >
                    {detail?.data?.methodname === 'vabni'
                      ? 'VA BNI'
                      : detail?.data?.methodname === 'duiktu'
                      ? 'Duitku'
                      : 'Transfer Bank'}
                  </Text>
                </View>
              </View>
              <View style={{ justifyContent: 'center', marginTop: 25 }}>
                <View style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 16,
                      color: '#1C4469',
                    }}
                  >
                    Konfirmasi Pembayaran
                  </Text>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginTop: 8,
                }}
              >
                <TouchableOpacity
                  onPress={() => openCameraOrCalleryChooser('Pembayaran')}
                >
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: 'rgba(233, 233, 233, 0.5)',
                      borderWidth: 1,
                      borderColor: 'rgba(138, 138, 138, 0.5)',
                      borderStyle: 'dashed',
                      width: 145,
                      height: 120,
                      borderRadius: 5,
                    }}
                  >
                    {(namePembayaran != null && (
                      <Text
                        style={{
                          alignItems: 'center',
                          textAlign: 'center',
                          fontSize: 15,
                          marginVertical: 2,
                        }}
                      >
                        {namePembayaran}
                      </Text>
                    )) || <IconAdd />}
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => openCameraOrCalleryChooser('Pajak')}
                >
                  <View
                    style={{
                      backgroundColor: 'rgba(233, 233, 233, 0.5)',
                      borderWidth: 1,
                      borderColor: 'rgba(138, 138, 138, 0.5)',
                      borderStyle: 'dashed',
                      height: 120,
                      width: 145,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 5,
                    }}
                  >
                    {(namePajak != null && (
                      <Text
                        style={{
                          alignItems: 'center',
                          textAlign: 'center',
                          fontSize: 15,
                          marginVertical: 2,
                        }}
                      >
                        {namePajak}
                      </Text>
                    )) || <IconAdd />}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginTop: 5,
                }}
              >
                <View style={{ paddingLeft: 20 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    Bukti Pembayaran
                  </Text>
                </View>

                <View style={{ paddingRight: 12 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    Bukti Potong Pajak
                  </Text>
                </View>
              </View>
              <Button
                onPress={() => {
                  onSubmit();
                  setLoading(true);
                }}
                style={{
                  backgroundColor: '#1C4469',
                  borderRadius: 5,
                  minHeight: 52,
                  marginTop: 38,
                  justifyContent: 'center',
                }}
                disabled={proofPayment == null || taxSlip == null}
              >
                <Text
                  style={{
                    textAlign: 'center',
                    color: COLOR_CONSTANT.WHITE,
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 20,
                  }}
                >
                  Kirim
                </Text>
              </Button>
            </View>
          </ScrollView>
        )
      )}
      {loading === true && detail?.save == null ? (
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 12,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <View
              style={{
                padding: 24,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <Text>Please wait!</Text>
            </View>
            <View
              style={{
                padding: 24,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <Text>Submit Process...</Text>
            </View>
            <View
              style={{
                padding: 24,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <LoadingView />
            </View>
          </View>
        </View>
      ) : (
        detail?.save === 'Success' && (
          <ScrollView>
            <View
              style={{
                flex: 1,
                overflow: 'hidden',
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 55,
                paddingBottom: 30,
                backgroundColor: COLOR_CONSTANT.WHITE,
              }}
            >
              <View style={{ justifyContent: 'center' }}>
                <View style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_600SemiBold',
                      fontSize: 28,
                    }}
                  >
                    Pembayaran Terkonfirmasi
                  </Text>
                </View>
                <View style={{ alignItems: 'center', marginTop: 22 }}>
                  <IconChecklist />
                </View>
                <View style={{ alignItems: 'center', marginTop: 34 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    {moment(detail?.data?.paymentdate).format('DD-MMM-yyyy')}
                  </Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_600SemiBold',
                      fontSize: 28,
                    }}
                  >
                    {detail?.data?.paymentcode}
                  </Text>
                </View>
              </View>
              <View style={{ justifyContent: 'center', marginTop: 25 }}>
                <View style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_500Medium',
                      fontSize: 16,
                      color: '#1C4469',
                    }}
                  >
                    Konfirmasi Pembayaran
                  </Text>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginTop: 8,
                }}
              >
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'rgba(233, 233, 233, 0.5)',
                    borderWidth: 1,
                    borderColor: 'rgba(138, 138, 138, 0.5)',
                    borderStyle: 'dashed',
                    width: 145,
                    height: 120,
                    borderRadius: 5,
                  }}
                >
                  <Text
                    style={{
                      alignItems: 'center',
                      textAlign: 'center',
                      fontSize: 15,
                      marginVertical: 2,
                    }}
                  >
                    {namePembayaran}
                  </Text>
                </View>

                <View
                  style={{
                    backgroundColor: 'rgba(233, 233, 233, 0.5)',
                    borderWidth: 1,
                    borderColor: 'rgba(138, 138, 138, 0.5)',
                    borderStyle: 'dashed',
                    height: 120,
                    width: 145,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                  }}
                >
                  <Text
                    style={{
                      alignItems: 'center',
                      textAlign: 'center',
                      fontSize: 15,
                      marginVertical: 2,
                    }}
                  >
                    {namePajak}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginTop: 5,
                }}
              >
                <View style={{ paddingLeft: 20 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    Bukti Pembayaran
                  </Text>
                </View>

                <View style={{ paddingRight: 12 }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat_400Regular',
                      fontSize: 12,
                    }}
                  >
                    Bukti Potong Pajak
                  </Text>
                </View>
              </View>
              <Button
                onPress={() => {
                  navigation.goBack();
                  loadPaymentsAction(currentUser.company_id, 1);
                }}
                style={{
                  backgroundColor: '#1C4469',
                  borderRadius: 5,
                  minHeight: 52,
                  marginTop: 38,
                  justifyContent: 'center',
                }}
              >
                <Text
                  style={{
                    textAlign: 'center',
                    color: COLOR_CONSTANT.WHITE,
                    fontFamily: 'Montserrat_500Medium',
                    fontSize: 20,
                  }}
                >
                  Selesai
                </Text>
              </Button>
            </View>
          </ScrollView>
        )
      )}
    </>
  );
};

const mapStateToProps = ({ paymentApp, authApp }) => {
  const { selectedPayment, detail } = paymentApp;
  const { currentUser } = authApp;

  return {
    selectedPayment,
    detail,
    currentUser,
  };
};

export default connect(mapStateToProps, {
  savePaymentsDetailAction: savePaymentsDetail,
  loadPaymentsAction: loadPayments,
})(Manual);
