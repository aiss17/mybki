/* eslint-disable eqeqeq */
// @ts-nocheck
import React, { useEffect } from 'react';
import { SearchBar } from 'react-native-elements';
import { useActionSheet } from '@expo/react-native-action-sheet';
import { connect } from 'react-redux';
import {
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import CommonHeader from '../../components/common/common-header';
// import { COLOR_CONSTANT } from '../../constants/style-constant';
import ListItem from './components/list-item';
import {
  loadPayments,
  selectedPayment,
  loadPaymentsDetail,
  loadPaymentsInvoice,
} from '../../redux/payment/action';
import LoadingView from '../../components/LoadingView';
import IconAddSmall from '../../assets/icons/icon-add-small.svg';

const Payments = ({
  navigation,
  loadPaymentsAction,
  loadPaymentsDetailAction,
  loadPaymentsInvoiceAction,
  currentUser,
  invoice,
  list,
}) => {
  const { data, page, loading } = list;
  const { showActionSheetWithOptions } = useActionSheet();
  useEffect(() => {
    loadPaymentsAction(currentUser?.company_id, 1);
    loadPaymentsInvoiceAction(currentUser?.company_id);
  }, [loadPaymentsAction]);

  return (
    <>
      <CommonHeader
        title="Payments"
        onBackPressed={() => navigation.goBack()}
      />
      <SearchBar
        inputContainerStyle={{
          backgroundColor: '#E5E5E5',
          borderColor: '#8E8E8E',
          borderWidth: 1,
          width: '100%',
        }}
        round
        containerStyle={{
          backgroundColor: 'white',
        }}
        searchIcon={{ size: 24 }}
        placeholder="Type payment ID..."
        style={{ fontSize: 14, fontStyle: 'italic' }}
      />
      <ScrollView
        style={{ paddingTop: 5 }}
        refreshControl={
          <RefreshControl
            refreshing={data == null && loading}
            onRefresh={() => loadPaymentsAction(currentUser.company_id, 1)}
          />
        }
      >
        {data && (
          <>
            {data.map((t) => {
              console.log('data t', t);
              return (
                <TouchableOpacity
                  onPress={() => {
                    if (
                      t.confirmation_status == 1 &&
                      t.methodname == 'manual'
                    ) {
                      loadPaymentsDetailAction(t.id);
                      navigation.navigate('PaymentConfirmation');
                    } else if (
                      t.confirmation_status == 1 &&
                      t.methodname == 'duitku'
                    ) {
                      loadPaymentsDetailAction(t.id);
                      navigation.navigate('PaymentConfirmationNonManual');
                    } else if (
                      t.confirmation_status == 1 &&
                      t.methodname == 'vabni'
                    ) {
                      loadPaymentsDetailAction(t.id);
                      navigation.navigate('PaymentConfirmationNonManual');
                    }
                  }}
                  key={t.id}
                >
                  <ListItem
                    showActionSheetWithOptions={showActionSheetWithOptions}
                    data={t}
                  />
                </TouchableOpacity>
              );
            })}

            {loading ? (
              <View
                style={{
                  padding: 24,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
              >
                <LoadingView />
              </View>
            ) : (
              <>
                {page.currentPage < page.totalPage && (
                  <View
                    style={{
                      padding: 24,
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        loadPaymentsAction(
                          currentUser.company_id,
                          parseInt(page.currentPage, 10) + 1
                        );
                      }}
                    >
                      <Text
                        style={{
                          color: 'gray',
                          fontSize: 16,
                        }}
                      >
                        Load More
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </>
            )}
          </>
        )}
      </ScrollView>

      <View
        style={{
          width: 60,
          height: 60,
          borderRadius: 30,
          backgroundColor: '#1C4469',
          position: 'absolute',
          bottom: 10,
          right: 10,
          alignContent: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          color: '#FFFFFF',
        }}
      >
        <TouchableOpacity
          onPress={() => {
            if (invoice?.data?.length > 0) {
              navigation.navigate('AddNewPayments');
            } else {
              alert(
                'You Cannot Access this Feature because your Invoice is empty!!'
              );
            }
          }}
        >
          <IconAddSmall
            style={{
              color: '#FFFFFF',
            }}
          />
        </TouchableOpacity>
      </View>
    </>
  );
};

const mapStateToProps = ({ paymentApp, authApp }) => {
  const { list, invoice } = paymentApp;
  const { currentUser } = authApp;

  return {
    list,
    currentUser,
    invoice,
  };
};

export default connect(mapStateToProps, {
  loadPaymentsAction: loadPayments,
  selectedPaymentAction: selectedPayment,
  loadPaymentsDetailAction: loadPaymentsDetail,
  loadPaymentsInvoiceAction: loadPaymentsInvoice,
})(Payments);
