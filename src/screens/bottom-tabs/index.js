// @ts-nocheck
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Text, View } from 'react-native';
import CogSvg from '../../assets/icons/cog.svg';
import EmailSvg from '../../assets/icons/email.svg';
import HelmetSvg from '../../assets/icons/helmet.svg';
import HomeSvg from '../../assets/icons/home.svg';
import { COLOR_CONSTANT, STYLE_CONSTANT } from '../../constants/style-constant';
import '../../helpers/UserAgent';
import { navigationRef } from '../../RootNavigation';
import Home from '../home';
import Services from '../services';
import ServiceDetail from '../services/components/detail';
import CartDetail from '../services/components/cart';
import Orders from '../orders';
import Invoices from '../invoices';
import UnderConstruction from '../under-construction';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const BottomTabs = () => {
  const MenuItem = (IconAvg, label, isActive) => {
    return (
      <View style={{ flexDirection: 'column', alignItems: 'center' }}>
        <View
          style={{
            height: 30,
            justifyContent: 'center',
          }}
        >
          <IconAvg
            style={[
              {
                color: COLOR_CONSTANT.DARKGRAY,
              },
              isActive ? { color: COLOR_CONSTANT.DARKBLUE } : {},
            ]}
          />
        </View>
        {label && (
          <Text
            style={[
              {
                fontFamily: 'Montserrat_700Bold',
                fontSize: 8,
                color: COLOR_CONSTANT.DARKGRAY,
              },
              isActive ? { color: COLOR_CONSTANT.DARKBLUE } : {},
            ]}
          >
            {label}
          </Text>
        )}
      </View>
    );
  };

  const ServicesStack = () => {
    return (
      <Stack.Navigator
        initialRouteName="Services"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Services" component={Services} />
        <Stack.Screen name="ServiceDetail" component={ServiceDetail} />
        <Stack.Screen name="CartDetail" component={CartDetail} />
      </Stack.Navigator>
    );
  };

  const HomeStack = () => {
    return (
      <Stack.Navigator
        initialRouteName="Homes"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Homes" component={Home} />
        <Stack.Screen name="Orders" component={Orders} />
        <Stack.Screen name="Invoices" component={Invoices} />
      </Stack.Navigator>
    );
  };

  return (
    <NavigationContainer ref={navigationRef}>
      <Tab.Navigator
        screenOptions={{
          headerTitleAlign: 'center',
          tabBarShowLabel: false,
        }}
      >
        <Tab.Screen
          name="Home"
          component={HomeStack}
          options={{
            headerShown: false,
            lazy: true,
            tabBarLabelStyle: [STYLE_CONSTANT.FONT_SHADOW1],
            tabBarIcon: ({ focused }) => MenuItem(HomeSvg, 'Homes', focused),
          }}
        />
        <Tab.Screen
          name="Service"
          component={ServicesStack}
          options={{
            headerShown: false,
            lazy: true,
            tabBarLabelStyle: [STYLE_CONSTANT.FONT_SHADOW1],
            tabBarIcon: ({ focused }) =>
              MenuItem(HelmetSvg, 'Services', focused),
          }}
        />
        <Tab.Screen
          name="Notification"
          component={UnderConstruction}
          options={{
            headerShown: false,
            lazy: true,
            tabBarLabelStyle: [STYLE_CONSTANT.FONT_SHADOW1],
            tabBarIcon: ({ focused }) =>
              MenuItem(EmailSvg, 'Notification', focused),
          }}
        />
        <Tab.Screen
          name="Setting"
          component={UnderConstruction}
          options={{
            headerShown: false,
            lazy: true,
            tabBarLabelStyle: [STYLE_CONSTANT.FONT_SHADOW1],
            tabBarIcon: ({ focused }) => MenuItem(CogSvg, 'Setting', focused),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default BottomTabs;
