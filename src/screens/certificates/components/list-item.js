/* eslint-disable eqeqeq */
// @ts-nocheck
import React, { useState } from 'react';
import moment from 'moment';
import { Text, TouchableOpacity, View } from 'react-native';
import {
  COLOR_CONSTANT,
  STYLE_CONSTANT,
} from '../../../constants/style-constant';
import CertificateBlue from '../../../assets/icons/certificate-blue.svg';
import CertificateRed from '../../../assets/icons/certificate-red.svg';

const ListItem = ({ data, activeTab }) => {
  const [expanded, setExpanded] = useState(false);
  const renderInfo = (text1, text2, text3, text4, text5, text6, text7) => {
    return (
      <View style={{ flexDirection: 'row' }}>
        <View style={{ marginVertical: 4, width: '60%' }}>
          <View style={{ marginBottom: 5 }}>
            <Text style={{ fontFamily: 'Montserrat_400Regular', fontSize: 10 }}>
              {text1}
            </Text>
            <Text
              style={{
                fontFamily: 'Montserrat_600SemiBold',
                fontSize: 12,
              }}
            >
              {text2}
            </Text>
          </View>

          <Text style={{ fontFamily: 'Montserrat_400Regular', fontSize: 10 }}>
            {text6}
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat_600SemiBold',
              fontSize: 12,
            }}
          >
            {text7}
          </Text>
        </View>
        <View style={{ marginVertical: 4 }}>
          <View style={{ marginBottom: 5 }}>
            <Text
              style={{
                fontFamily: 'Montserrat_600SemiBold',
                fontSize: 12,
                fontWeight: '600',
              }}
            >
              {text3}
            </Text>
          </View>

          <Text
            style={{
              fontFamily: 'Montserrat_400Regular',
              fontSize: 10,
            }}
          >
            {text4}
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat_600SemiBold',
              fontSize: 12,
            }}
          >
            {text5}
          </Text>
        </View>
      </View>
    );
  };
  return (
    <TouchableOpacity onPress={() => setExpanded(!expanded)}>
      <View
        style={[
          {
            borderRadius: 5,
            marginBottom: 12,
          },
          STYLE_CONSTANT.SHADOW2,
        ]}
      >
        <View
          style={{
            backgroundColor: COLOR_CONSTANT.WHITE,
            borderRadius: 5,
            overflow: 'hidden',
            flexDirection: 'row',
          }}
        >
          <View style={{ backgroundColor: '#DF5C35', width: 5 }} />
          <View
            style={{
              flex: 1,
              marginLeft: 16,
              marginRight: 8,
              marginVertical: 8,
            }}
          >
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <View style={{ marginVertical: 4, width: '80%' }}>
                <Text
                  style={{ fontFamily: 'Montserrat_400Regular', fontSize: 12 }}
                >
                  Class Certificate
                </Text>
                <Text
                  style={{
                    fontFamily: 'Montserrat_600SemiBold',
                    fontSize: 16,
                  }}
                >
                  {data?.nama_sertifikat}
                </Text>
              </View>
              <View style={{ marginVertical: 4 }}>
                {(activeTab === 'class' && <CertificateRed />) ||
                  (activeTab === 'statutory' && <CertificateBlue />)}
              </View>
            </View>
            {expanded && (
              <>
                {renderInfo(
                  'No Sertifikat',
                  data?.no_sertifikat,
                  data?.type == 1 ? 'FULL TERM' : 'SHORT TERM',
                  'Expired Until',
                  moment(data?.tgl_exp).format('DD-MMM-yyyy'),
                  'Tanggal Sertifikat',
                  moment(data?.tgl_sertifikat).format('DD-MMM-yyyy')
                )}
              </>
            )}
          </View>
        </View>
      </View>
      {/* </View> */}
    </TouchableOpacity>
  );
};

export default ListItem;
