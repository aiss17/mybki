/* eslint-disable eqeqeq */
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { ScrollView, View } from 'react-native';
import CommonHeader from '../../components/common/common-header';
import TabItem from '../../components/common/tab-item';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import ListItem from './components/list-item';
import { loadCertificateBki } from '../../redux/certificate/action';

const Certificates = ({
  navigation,
  loadCertificateBkiAction,
  list,
  currentUser,
  currentToken,
}) => {
  const [activeTab, setActiveTab] = useState('class');
  const handleTabChange = (val) => {
    setActiveTab(val);
  };

  useEffect(() => {
    const keyCertif = currentToken?.key_bki;
    const row = {
      key: keyCertif,
      id_customer: currentUser?.company_id,
    };
    loadCertificateBkiAction(row);
  }, []);
  return (
    <>
      <CommonHeader
        title="Certificates"
        onBackPressed={() => navigation.goBack()}
      />
      <View
        style={{
          height: 60,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 5,
          paddingTop: 20,
        }}
      >
        <TabItem
          label="Class Certification"
          isActive={activeTab === 'class'}
          onPressed={() => handleTabChange('class')}
        />
        <View style={{ padding: 5 }} />
        <TabItem
          label="Statutory Classification"
          isActive={activeTab === 'statutory'}
          onPressed={() => handleTabChange('statutory')}
        />
      </View>
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
          paddingHorizontal: 16,
          paddingVertical: 25,
        }}
      >
        {activeTab === 'class' && (
          <>
            {list?.data
              ?.filter((x) => x.category == 1)
              .map((t) => {
                return (
                  <View key={t.id}>
                    <ListItem data={t} activeTab={activeTab} />
                  </View>
                );
              })}
          </>
        )}

        {activeTab === 'statutory' && (
          <>
            {list?.data
              ?.filter((x) => x.category == 2)
              .map((t) => {
                return (
                  <View key={t.id}>
                    <ListItem data={t} activeTab={activeTab} />
                  </View>
                );
              })}
          </>
        )}
      </ScrollView>
    </>
  );
};

const mapStateToProps = ({ certificateApp, authApp }) => {
  const { list } = certificateApp;
  const { currentUser, currentToken } = authApp;

  return {
    list,
    currentUser,
    currentToken,
  };
};

export default connect(mapStateToProps, {
  loadCertificateBkiAction: loadCertificateBki,
})(Certificates);
