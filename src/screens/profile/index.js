// @ts-nocheck
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
// import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
// import { faClipboardList } from '@fortawesome/free-solid-svg-icons';
import { TextInput } from 'react-native-paper';
import CommonHeader from '../../components/common/common-header';
import { COLOR_CONSTANT, STYLE_CONSTANT } from '../../constants/style-constant';
// import { withNavigation } from 'react-navigation';
// import { COLOR_CONSTANT } from '../../constants/style-constant';
// import ListItem from './components/list-item';

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'white',
  },
  headerContent: {
    padding: 30,
    alignItems: 'center',
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 73,
    borderWidth: 4,
    borderColor: 'white',
    marginBottom: 10,
  },
  name: {
    fontSize: 16,
    color: COLOR_CONSTANT.BLACK,
    fontFamily: 'Montserrat_500Medium',
  },
  userInfo1: {
    fontSize: 12,
    color: COLOR_CONSTANT.DARKGRAY,
    fontFamily: 'Montserrat_500Medium',
  },
  userInfo2: {
    marginTop: 10,
    fontSize: 13,
    color: COLOR_CONSTANT.DARKGRAY,
    fontFamily: 'Montserrat_500Medium',
  },
  body: {
    backgroundColor: 'white',
    height: 500,
    alignItems: 'center',
  },
  item: {
    flexDirection: 'row',
  },
  infoContent: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 5,
  },
  iconContent: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 20,
    color: 'black',
  },
  container: {
    flex: 1,
    marginTop: 5,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 32,
  },
});

const Profile = ({ navigation }) => {
  useEffect(() => {});

  return (
    <>
      <CommonHeader
        title="My Profile"
        onBackPressed={() => navigation.goBack()}
      />
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <Image
              style={styles.avatar}
              source={{
                uri: 'https://bootdey.com/img/Content/avatar/avatar6.png',
              }}
            />

            <Text style={styles.name}>Janwar Iskandar </Text>
            <Text style={styles.userInfo1}>admin@gmail.com </Text>
            <Text style={styles.userInfo2}>Owner (exp at 09 April 2023) </Text>
            <TouchableOpacity onPress={() => null}>
              <View
                style={{
                  backgroundColor: '#506CEB',
                  width: 300,
                  height: 40,
                  borderRadius: 18,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 15,
                }}
              >
                <Text
                  style={{
                    fontFamily: 'Montserrat_700Bold',
                    fontSize: 16,
                    color: COLOR_CONSTANT.WHITE,
                  }}
                >
                  Edit Profile
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 1,
              marginLeft: 15,
              marginRight: 15,
              marginTop: 5,
              flexDirection: 'row',
              height: 50,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View style={{ flex: 0.3, marginTop: 15, marginStart: 10 }}>
            <TextInput.Icon
              name="account-circle-outline"
              size={28}
              color="#506CEB"
            />
          </View>
          <View style={{ flex: 1.3, marginTop: 15, marginStart: 5 }}>
            <Text
              style={{
                color: COLOR_CONSTANT.DARKGRAY,
                fontFamily: 'Montserrat_500Medium',
              }}
            >
              Personal Information
            </Text>
          </View>
          <View
            style={{
              flex: 0.2,
              alignItems: 'flex-end',
              marginTop: 15,
              marginStart: 5,
            }}
          >
            <TextInput.Icon
              name="chevron-right"
              size={28}
              color={COLOR_CONSTANT.DARKGRAY}
            />
          </View>
        </View>
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 1,
              marginLeft: 15,
              marginRight: 15,
              marginTop: 15,
              flexDirection: 'row',
              height: 50,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View style={{ flex: 0.3, marginTop: 15, marginStart: 10 }}>
            <TextInput.Icon name="domain" size={28} color="#506CEB" />
          </View>
          <View style={{ flex: 1.3, marginTop: 15, marginStart: 5 }}>
            <Text
              style={{
                color: COLOR_CONSTANT.DARKGRAY,
                fontFamily: 'Montserrat_500Medium',
              }}
            >
              My Company
            </Text>
          </View>
          <View
            style={{
              flex: 0.2,
              alignItems: 'flex-end',
              marginTop: 15,
              marginStart: 5,
            }}
          >
            <TextInput.Icon
              name="chevron-right"
              size={28}
              color={COLOR_CONSTANT.DARKGRAY}
            />
          </View>
        </View>
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 1,
              marginLeft: 15,
              marginRight: 15,
              marginTop: 15,
              flexDirection: 'row',
              height: 50,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View style={{ flex: 0.3, marginTop: 15, marginStart: 10 }}>
            <TextInput.Icon
              name="car-shift-pattern"
              size={28}
              color="#506CEB"
            />
          </View>
          <View style={{ flex: 1.3, marginTop: 15, marginStart: 5 }}>
            <Text
              style={{
                color: COLOR_CONSTANT.DARKGRAY,
                fontFamily: 'Montserrat_500Medium',
              }}
            >
              My Fleet Information
            </Text>
          </View>
          <View
            style={{
              flex: 0.2,
              alignItems: 'flex-end',
              marginTop: 15,
              marginStart: 5,
            }}
          >
            <TextInput.Icon
              name="chevron-right"
              size={28}
              color={COLOR_CONSTANT.DARKGRAY}
            />
          </View>
        </View>
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 1,
              marginLeft: 15,
              marginRight: 15,
              marginTop: 15,
              flexDirection: 'row',
              height: 50,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View style={{ flex: 0.3, marginTop: 15, marginStart: 10 }}>
            <TextInput.Icon name="bell-outline" size={28} color="#506CEB" />
          </View>
          <View style={{ flex: 1.3, marginTop: 15, marginStart: 5 }}>
            <Text
              style={{
                color: COLOR_CONSTANT.DARKGRAY,
                fontFamily: 'Montserrat_500Medium',
              }}
            >
              Notifications
            </Text>
          </View>
          <View
            style={{
              flex: 0.2,
              alignItems: 'flex-end',
              marginTop: 15,
              marginStart: 5,
            }}
          >
            <TextInput.Icon
              name="chevron-right"
              size={28}
              color={COLOR_CONSTANT.DARKGRAY}
            />
          </View>
        </View>
        <View
          style={[
            {
              borderRadius: 5,
              marginBottom: 1,
              marginLeft: 15,
              marginRight: 15,
              marginTop: 15,
              flexDirection: 'row',
              height: 50,
            },
            STYLE_CONSTANT.SHADOW2,
          ]}
        >
          <View style={{ flex: 0.3, marginTop: 15, marginStart: 10 }}>
            <TextInput.Icon name="history" size={28} color="#506CEB" />
          </View>
          <View style={{ flex: 1.3, marginTop: 15, marginStart: 5 }}>
            <Text
              style={{
                color: COLOR_CONSTANT.DARKGRAY,
                fontFamily: 'Montserrat_500Medium',
              }}
            >
              History
            </Text>
          </View>
          <View
            style={{
              flex: 0.2,
              alignItems: 'flex-end',
              marginTop: 15,
              marginStart: 5,
            }}
          >
            <TextInput.Icon
              name="chevron-right"
              size={28}
              color={COLOR_CONSTANT.DARKGRAY}
            />
          </View>
        </View>
      </View>
    </>
  );
};

const mapStateToProps = ({ orderApp }) => {
  const { list } = orderApp;

  return {
    list,
  };
};

export default connect(mapStateToProps, {})(Profile);
