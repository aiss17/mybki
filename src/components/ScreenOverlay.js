import React from 'react';
import { View } from 'react-native';
import { COLOR_CONSTANT } from '../constants/style-constant';

const ScreenOverlay = ({ content }) => {
  return (
    <>
      <View
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          backgroundColor: COLOR_CONSTANT.WHITE,
          opacity: 0.8,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {content}
      </View>
    </>
  );
};

export default ScreenOverlay;
