import React from 'react';
import { View } from 'react-native';

const Dot = ({ width, color }) => {
  return (
    <>
      <View
        style={{
          width,
          height: width,
          borderRadius: width / 2,
          backgroundColor: color,
        }}
      />
    </>
  );
};

export default Dot;
