import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { AutoSizeText, ResizeTextMode } from 'react-native-auto-size-text';
import { withNavigation } from 'react-navigation';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import ImageWithLoading from '../images/image-with-loading';

const skpRightTop = require('../../assets/icons/skp-ribbon-righttop-2.png');

const ContentPaper1 = ({
  username,
  numlikes,
  imageUri,
  title,
  description,
  liked,
  date,
  navigation,
  kontributor,
  skp_point,
  commentPressed = () => {},
  likeDoublePressed = () => {},
  detailRouteName = null,
  detailRouteParam = null,
  disableShare = false,
  disableComment = false,
  disableLike = false,
}) => {
  return (
    <>
      <TouchableOpacity
        onPress={() => {
          if (detailRouteName)
            navigation.navigate(detailRouteName, { data: detailRouteParam });
        }}
      >
        <View style={{ width: '100%', height: 120, flexDirection: 'row' }}>
          <View
            style={{
              flex: 1,
              backgroundColor: COLOR_CONSTANT.DARKGRAY,
              borderRadius: 5,
              overflow: 'hidden',
            }}
          >
            <ImageWithLoading
              imageUri={imageUri}
              style={{ position: 'absolute', width: '100%', height: '100%' }}
            />
            {skp_point > 0 && (
              <View
                style={{
                  width: 60,
                  height: 25,
                  top: -2,
                  right: -3,
                  position: 'absolute',
                }}
              >
                <Image
                  source={skpRightTop}
                  style={{ width: '100%', height: '100%' }}
                  resizeMode="stretch"
                />
                <View
                  style={{
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'flex-start',
                    paddingTop: 4,
                    paddingRight: 6,
                  }}
                >
                  <Text
                    style={{
                      color: COLOR_CONSTANT.WHITE,
                      fontSize: 11,
                      fontWeight: '700',
                    }}
                  >
                    {skp_point} SKP
                  </Text>
                </View>
              </View>
            )}
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'space-between',
              marginLeft: 8,
            }}
          >
            <View style={{ flex: 1 }}>
              <AutoSizeText
                fontSize={11}
                numberOfLines={2}
                style={{ fontWeight: '700' }}
                mode={ResizeTextMode.max_lines}
              >
                {title.length < 70
                  ? `${title}`
                  : `${title.substring(0, 70)}...`}
              </AutoSizeText>
              <Text style={{ fontSize: 9, marginTop: 4 }}>
                {description || ''}
              </Text>
            </View>
            <View>
              <Text style={{ fontSize: 9, color: COLOR_CONSTANT.DARKGRAY }}>
                {date || ''}
              </Text>
              <Text style={{ fontSize: 11, fontWeight: '700' }}>
                {kontributor}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default withNavigation(ContentPaper1);
