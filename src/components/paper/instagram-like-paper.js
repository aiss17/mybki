import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Avatar } from 'react-native-paper';
import { STYLE_CONSTANT } from '../../constants/style-constant';
import ImageWithLoading from '../images/image-with-loading';

const { width } = Dimensions.get('window');
const avatarImage = require('../../assets/demo/avatar.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
  },
  headerText: {
    textAlign: 'center',
    flex: 1,
    fontFamily: 'lobster_regular',
    fontSize: 16,
  },
  inputWrapper: {
    marginTop: 20,
  },
  profileImage: {
    width: width * 0.1,
    height: width * 0.1,
    borderRadius: width * 0.4,
    borderWidth: 1,
  },
  image: {
    width,
    height: width * 0.8,
  },
  infoContainer: {
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingVertical: 10,
    flex: 1,
    alignItems: 'center',
  },
  infoText: {
    fontSize: 14,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
});

const InstagramLikePaper = ({
  username,
  numlikes,
  imageUri,
  title,
  description,
}) => {
  return (
    <>
      <View
        style={[
          STYLE_CONSTANT.SHADOW2,
          { paddingBottom: 12, marginBottom: 12 },
        ]}
      >
        <View style={styles.infoContainer}>
          <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
            <Avatar.Image size={32} source={avatarImage} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={styles.infoText}>{username}</Text>
            </View>
          </View>
          <FontAwesomeIcon icon="ellipsis-v" size={22} color="black" />
        </View>
        <ImageWithLoading imageUri={imageUri} imageStyle={styles.image} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            padding: 12,
          }}
        >
          <TouchableOpacity>
            <FontAwesomeIcon icon="heart" size={22} color="black" />
          </TouchableOpacity>
          <TouchableOpacity style={{ paddingHorizontal: 15 }}>
            <FontAwesomeIcon icon="comment" size={22} color="black" />
          </TouchableOpacity>
          <TouchableOpacity>
            <FontAwesomeIcon icon="paper-plane" size={22} color="black" />
          </TouchableOpacity>
        </View>
        <Text style={[styles.infoText, { paddingBottom: 8 }]}>
          {numlikes + (numlikes !== 1 ? ' likes' : ' like')}
        </Text>
        {title && (
          <View style={{ paddingLeft: 10, paddingBottom: 10 }}>
            <Text style={{ fontWeight: 'bold' }}>{title}</Text>
          </View>
        )}
        {description && (
          <View style={{ paddingLeft: 10 }}>
            <Text>{description}</Text>
          </View>
        )}
      </View>
    </>
  );
};

export default InstagramLikePaper;
