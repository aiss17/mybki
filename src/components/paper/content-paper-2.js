import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { COLOR_CONSTANT } from '../../constants/style-constant';
import MultiTouchableOpacity from '../common/multi-touchable-opacity';
import ImageWithLoading from '../images/image-with-loading';

const ContentPaper2 = ({
  username,
  numlikes,
  imageUri,
  title,
  description,
  liked,
  date,
  navigation,
  kontributor,
  commentPressed = () => {},
  likeDoublePressed = () => {},
  detailRouteName = null,
  detailRouteParam = null,
  disableShare = false,
  disableComment = false,
  disableLike = false,
}) => {
  return (
    <>
      <TouchableOpacity
        onPress={() => {
          if (detailRouteName)
            navigation.navigate(detailRouteName, { data: detailRouteParam });
        }}
      >
        <View
          style={{
            width: '100%',
            height: 250,
            backgroundColor: COLOR_CONSTANT.DARKGRAY,
            borderRadius: 10,
            overflow: 'hidden',
            marginBottom: 18,
          }}
        >
          <ImageWithLoading
            imageUri={imageUri}
            style={{ position: 'absolute', width: '100%', height: '100%' }}
          />
          <View style={{ flexDirection: 'column', height: '100%' }}>
            <View style={{ padding: 8 }}>
              <Text style={{ color: COLOR_CONSTANT.WHITE }}>{date}</Text>
            </View>
            <View
              style={{ backgroundColor: 'rgba(105,105,105,0.5)', padding: 8 }}
            >
              <Text
                style={{
                  color: COLOR_CONSTANT.WHITE,
                  fontWeight: 'bold',
                  fontSize: 14,
                }}
              >
                {title}
              </Text>
            </View>
            <View
              style={{
                padding: 8,
                flex: 1,
                justifyContent: 'flex-end',
                flexDirection: 'column',
              }}
            >
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text
                    style={{ color: COLOR_CONSTANT.WHITE, fontWeight: 'bold' }}
                  >
                    {kontributor || ''}
                  </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {!disableLike && (
                    <TouchableOpacity>
                      <MultiTouchableOpacity onMultiTaps={likeDoublePressed}>
                        <FontAwesomeIcon
                          icon="heart"
                          size={21}
                          color={
                            liked ? COLOR_CONSTANT.RED : COLOR_CONSTANT.WHITE
                          }
                        />
                      </MultiTouchableOpacity>
                    </TouchableOpacity>
                  )}
                  {!disableComment && (
                    <TouchableOpacity
                      style={{ paddingLeft: 18 }}
                      onPress={commentPressed}
                    >
                      <FontAwesomeIcon icon="comment" size={21} color="white" />
                    </TouchableOpacity>
                  )}
                  {!disableShare && (
                    <TouchableOpacity style={{ paddingLeft: 18 }}>
                      <FontAwesomeIcon
                        icon="paper-plane"
                        size={21}
                        color="white"
                      />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default withNavigation(ContentPaper2);
