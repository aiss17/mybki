import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { AutoSizeText, ResizeTextMode } from 'react-native-auto-size-text';
import { withNavigation } from 'react-navigation';
import AwardSvg from '../../assets/icons/award.svg';
import { COLOR_CONSTANT } from '../../constants/v2/style-constant';
import ImageWithLoading from '../images/image-with-loading';

const boxImage = require('../../assets/backgrounds/box.png');
const boxGradientImage = require('../../assets/backgrounds/box-gradient.png');

const ContentPaper3 = ({
  username,
  numlikes,
  imageUri,
  title,
  description,
  liked,
  date,
  navigation,
  text1,
  text2,
  text3,
  text4,
  skp_point,
  commentPressed = () => {},
  likeDoublePressed = () => {},
  detailRouteName = null,
  detailRouteParam = null,
  disableShare = false,
  disableComment = false,
  disableLike = false,
  Icon,
}) => {
  return (
    <>
      <TouchableOpacity
        onPress={() => {
          if (detailRouteName)
            navigation.navigate(detailRouteName, { data: detailRouteParam });
        }}
      >
        <View style={{ width: '100%', height: 144 }}>
          <View style={{ position: 'absolute', width: '100%', height: '100%' }}>
            <Image
              source={boxImage}
              resizeMode="stretch"
              style={{ height: '100%', width: '100%' }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 10,
              marginTop: 6,
              marginBottom: 13,
            }}
          >
            <View style={{ flex: 1 }}>
              <View
                style={{
                  borderRadius: 5,
                  overflow: 'hidden',
                }}
              >
                <ImageWithLoading
                  imageUri={imageUri}
                  style={{ width: '100%', height: '100%' }}
                />
                <Image
                  style={{
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                  }}
                  source={boxGradientImage}
                />
              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 4 }}>
              <View style={{ flex: 1 }}>
                <Text
                  style={{
                    fontFamily: 'Nunito_600SemiBold',
                    color: COLOR_CONSTANT.ORANGE,
                    fontSize: 11,
                  }}
                >
                  {text1.length < 40
                    ? `${text1}`
                    : `${text1.substring(0, 40)}...`}
                </Text>
                <Text
                  style={{
                    fontFamily: 'Nunito_600SemiBold',
                    color: COLOR_CONSTANT.ORANGE,
                    fontSize: 11,
                  }}
                >
                  {text1.length < 40
                    ? `${text2}`
                    : `${text2.substring(0, 40)}...`}
                </Text>
                {skp_point > 0 && (
                  <View style={{ flexDirection: 'row', paddingTop: 8 }}>
                    <View>
                      <AwardSvg />
                    </View>
                    <Text
                      style={{
                        fontFamily: 'Nunito_600SemiBold',
                        fontSize: 11,
                        color: COLOR_CONSTANT.ORANGE,
                        width: 100,
                        lineHeight: 10,
                        paddingTop: 5,
                      }}
                    >
                      {skp_point} SKP Belum Diikuti
                    </Text>
                  </View>
                )}
              </View>
              <View style={{ justifyContent: 'flex-end' }}>
                {Icon && <Icon />}
              </View>
            </View>
          </View>
          <View
            style={{
              position: 'absolute',
              flex: 1,
              bottom: 0,
              padding: 12,
              paddingRight: 42,
            }}
          >
            <Text
              style={{
                fontFamily: 'Nunito_600SemiBold',
                fontSize: 10,
                color: COLOR_CONSTANT.DARKGRAY,
                lineHeight: 8,
                paddingTop: 5,
              }}
            >
              {text3}
            </Text>

            <AutoSizeText
              fontSize={11}
              numberOfLines={3}
              style={{
                fontFamily: 'Nunito_700Bold',
                fontSize: 12,
                color: COLOR_CONSTANT.DARKGRAY,
                lineHeight: 11,
                paddingTop: 4,
              }}
              mode={ResizeTextMode.max_lines}
            >
              {text4.length < 150
                ? `${text4}`
                : `${text4.substring(0, 150)}...`}
            </AutoSizeText>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default withNavigation(ContentPaper3);
