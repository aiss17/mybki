import React from 'react';
import AnimatedLoader from 'react-native-animated-loader';

const loadingAnim = require('../../assets/lottie/24863-loading-square-orange-four.json');

const Loading1 = ({ loading }) => {
  return (
    <>
      <AnimatedLoader
        visible={loading}
        overlayColor="rgba(255,255,255,0.55)"
        source={loadingAnim}
        animationStyle={{
          width: 100,
          height: 100,
        }}
        speed={1}
      />
    </>
  );
};

export default Loading1;
