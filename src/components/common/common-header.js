import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { Header } from 'react-native-elements';
import BackSvg from '../../assets/icons/back.svg';
import { COLOR_CONSTANT } from '../../constants/v2/style-constant';

class CommonHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isSearching: false, timer: null };
    this.searchHeaderRef = React.createRef(null);
    this.commentFieldRef = React.createRef(null);
  }

  componentDidMount() {
    if (this.commentFieldRef && this.commentFieldRef.current) {
      this.commentFieldRef.current.focus();
    }
  }

  changeDelay = (change) => {
    const { timer } = this.state;
    const { onSearch = () => {} } = this.props;
    if (timer) {
      clearTimeout(timer);
      this.setState({ timer: null });
    }
    this.setState({
      timer: setTimeout(() => {
        onSearch(change);
      }, 2000),
    });
  };

  closeFilter() {
    this.setState({ isSearching: false });
  }

  render() {
    const {
      onBackPressed,
      onSearchPressed,
      onMenuPressed,
      onClearSearch = () => {},
      title,
      uri,
      color,
      textColor,
    } = this.props;
    const { isSearching } = this.state;
    return (
      <>
        <Header
          placement="left"
          backgroundColor={color ? color : COLOR_CONSTANT.DARKBLUE}
          style={{ marginTop: -30 }}
          containerStyle={{
            borderBottomColor: '#DF5C35',
            borderBottomWidth: 4,
            height: 80,
            paddingTop: 0,
            paddingBottom: 0,
            alignItems: 'flex-end',
          }}
          statusBarProps={{
            translucent: true,
          }}
          leftComponent={
            onBackPressed && (
              <View
                style={{
                  height: 50,
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingTop: 0,
                  paddingBottom: 0,
                }}
              >
                <TouchableOpacity onPress={() => onBackPressed()}>
                  <BackSvg style={{ color: COLOR_CONSTANT.WHITE }} />
                </TouchableOpacity>
              </View>
            )
          }
          rightComponent={
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                alignItems: 'center',
                height: 44,
                paddingTop: 0,
                paddingBottom: 0,
              }}
            >
              {onSearchPressed && (
                <>
                  {isSearching ? (
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isSearching: false });
                        if (this.searchHeaderRef.current) {
                          this.searchHeaderRef.current.hide();
                          this.searchHeaderRef.current.clear();
                        }
                        onClearSearch();
                      }}
                    >
                      <FontAwesomeIcon icon="times" size={22} color="white" />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isSearching: true });
                        onSearchPressed();
                        if (this.searchHeaderRef.current) {
                          this.searchHeaderRef.current.show();
                        }
                      }}
                    >
                      <Text>Search</Text>
                    </TouchableOpacity>
                  )}
                </>
              )}
              {onMenuPressed && (
                <TouchableOpacity
                  onPress={() => onMenuPressed()}
                  style={{ marginLeft: 18 }}
                >
                  <FontAwesomeIcon icon="bars" size={22} color="white" />
                </TouchableOpacity>
              )}
            </View>
          }
          centerComponent={
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                alignItems: 'center',
                height: 44,
                width: '100%',
                paddingTop: 0,
                paddingBottom: 0,
              }}
            >
              {uri ? (
                <View style={{ flex: 1 }}>
                  <Image
                    source={{
                      uri,
                    }}
                    style={{
                      width: '100%',
                      height: '100%',
                      resizeMode: 'contain',
                    }}
                  />
                </View>
              ) : (
                <Text
                  style={{
                    fontFamily: 'Montserrat_600SemiBold',
                    fontSize: 15,
                    textAlign: 'center',
                    color: textColor ? textColor : COLOR_CONSTANT.WHITE,
                  }}
                >
                  {title}
                </Text>
              )}
            </View>
          }
        />
      </>
    );
  }
}

export default CommonHeader;
