import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT } from '../../constants/style-constant';

const NoData = () => {
  return (
    <>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <FontAwesomeIcon
          icon="exclamation-triangle"
          size={64}
          color={COLOR_CONSTANT.ORANGE}
        />
        <Text style={{ color: COLOR_CONSTANT.ORANGE, marginTop: 18 }}>
          Belum ada data
        </Text>
      </View>
    </>
  );
};

export default NoData;
