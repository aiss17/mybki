import React, { useState } from 'react';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT, STYLE_CONSTANT } from '../../constants/style-constant';
import { selectedTheme } from '../../themes/Theme';
import FlatListSlider from '../images/slider/flat-list-slider';

const slide1 = require('../../assets/demo/home/slide1.png');
const dotsImage = require('../../assets/demo/home/dots.png');

const items = [
  {
    image:
      'https://images.k24klik.com/product/apotek_online_k24klik_201609020551581922_789-Amaryl-Glimepiride-4-mg.jpg',
    description:
      'Amaryl M 2 mg/500 mg mengandung kombinasi Glimepiride (obat anti diabetes oral yang termasuk ke dalam golongan sulfonilurea)',
  },
  {
    image:
      'https://images.unsplash.com/photo-1455620611406-966ca6889d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1130&q=80',
    description:
      'Red fort in India New Delhi is a magnificient masterpeiece of humans',
  },
  {
    image:
      'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
    description:
      'Sample Description below the image for representation purpose only',
  },
  {
    image:
      'https://images.unsplash.com/photo-1568700942090-19dc36fab0c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
    description:
      'Sample Description below the image for representation purpose only',
  },
  {
    image:
      'https://images.unsplash.com/photo-1584271854089-9bb3e5168e32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80',
    description:
      'Sample Description below the image for representation purpose only',
  },
];

const CommonSlider = ({ slides }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  return (
    <>
      <View style={[STYLE_CONSTANT.FLEX_ROW, {}]}>
        <View style={[STYLE_CONSTANT.FLEX_ROW]}>
          {/* <View style={{ height: '100%', justifyContent: 'center' }}>
              <Image style={{ justifyContent: 'center' }} source={dotsImage} />
            </View> */}
          <View style={[STYLE_CONSTANT.FLEX, { alignItems: 'center' }]}>
            <FlatListSlider
              data={slides || items}
              timer={5000}
              onPress={() => {}}
              indicatorContainerStyle={{ position: 'absolute', bottom: 20 }}
              indicatorActiveColor={COLOR_CONSTANT.ORANGE}
              indicatorInActiveColor="#ffffff"
              indicatorActiveWidth={30}
              currentIndexCallback={(i) => setActiveIndex(i)}
              animation
            />
          </View>
          {/* <View style={{ height: '100%', justifyContent: 'center' }}>
              <Image style={{ justifyContent: 'center' }} source={dotsImage} />
            </View> */}
        </View>
      </View>
      <View style={[STYLE_CONSTANT.FLEX, { alignItems: 'center' }]}>
        <Text
          style={{
            color: selectedTheme.primaryColor,
            textAlign: 'center',
          }}
        >
          {(slides || items)[activeIndex].description}
        </Text>
      </View>
    </>
  );
};

export default CommonSlider;
