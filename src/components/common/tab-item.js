import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { COLOR_CONSTANT } from '../../constants/style-constant';

const TabItem = ({ label, isActive, onPressed = () => {} }) => {
  return (
    <View
      style={[
        {
          flex: 1,
          alignItems: 'center',
          margin: 0,
          padding: 10,
          borderRadius: 5,
          backgroundColor: '#0073C3',
        },
        isActive
          ? {
              backgroundColor: '#1C4469',
              borderRadius: 5,
            }
          : {},
      ]}
    >
      <TouchableOpacity onPress={() => onPressed()}>
        <Text
          style={[
            {
              fontSize: 11,
              // padding: 5,
              color: COLOR_CONSTANT.WHITE,
              fontFamily: 'Montserrat_500Medium',
            },
            isActive
              ? {
                  // padding: 5,
                  fontSize: 11,
                  color: COLOR_CONSTANT.WHITE,
                  fontFamily: 'Montserrat_500Medium',
                }
              : {},
          ]}
        >
          {label}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TabItem;
