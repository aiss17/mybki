import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { COLOR_CONSTANT, STYLE_CONSTANT } from '../../constants/style-constant';

const SearchResult = ({
  result,
  loading,
  getText = (row) => (row || '').toString(),
  onItemPressed = () => {},
}) => {
  return (
    <>
      <View
        style={[
          STYLE_CONSTANT.SHADOW1,
          {
            backgroundColor: 'white',
            position: 'absolute',
            width: '100%',
            zIndex: 3, // works on ios
            elevation: 3, // works on android
          },
        ]}
      >
        {result.map((t) => {
          return (
            <View
              style={[
                {
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 8,
                  paddingVertical: 8,
                },
              ]}
              key={t.id}
            >
              <View>
                <FontAwesomeIcon
                  icon="history"
                  size={18}
                  color={COLOR_CONSTANT.GRAY}
                />
              </View>
              <View style={{ flex: 1, marginLeft: 18, paddingVertical: 4 }}>
                <TouchableOpacity onPress={() => onItemPressed(t)}>
                  <Text
                    style={{ color: COLOR_CONSTANT.GRAY }}
                    numberOfLines={1}
                    ellipsizeMode="head"
                  >
                    {getText(t)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <FontAwesomeIcon
                  icon="arrow-right"
                  size={18}
                  color={COLOR_CONSTANT.GRAY}
                />
              </View>
            </View>
          );
        })}
      </View>
    </>
  );
};

export default SearchResult;
