import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, { useEffect, useRef, useState } from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import { Header } from 'react-native-elements';
import { selectedTheme } from '../../themes/Theme';

const navBarBg = require('../../assets/bottom-tabs/nav-bg.png');

const CommonHeader = ({
  title,
  onBackPressed = null,
  onSearchPressed = null,
  onMenuPressed = null,
  onSearch = (change) => {},
  onClearSearch = () => {},
  searchResult,
}) => {
  const [isSearching, setIsSearching] = useState(false);
  const commentFieldRef = useRef(null);
  const searchHeaderRef = useRef(null);
  const [timer, setTimer] = useState(null);
  const changeDelay = (change) => {
    if (timer) {
      clearTimeout(timer);
      setTimer(null);
    }
    setTimer(
      setTimeout(() => {
        onSearch(change);
      }, 2000)
    );
  };

  useEffect(() => {
    if (commentFieldRef.current) {
      commentFieldRef.current.focus();
    }
  }, [commentFieldRef.current]);

  return (
    <>
      <Header
        placement="left"
        backgroundColor="orange"
        backgroundImage={navBarBg}
        leftComponent={
          onBackPressed && (
            <TouchableOpacity onPress={() => onBackPressed()}>
              <FontAwesomeIcon icon="chevron-left" size={22} color="white" />
            </TouchableOpacity>
          )
        }
        rightComponent={
          <View style={{ flexDirection: 'row' }}>
            {onSearchPressed && (
              <>
                {isSearching ? (
                  <TouchableOpacity
                    onPress={() => {
                      setIsSearching(false);
                      if (searchHeaderRef.current) {
                        searchHeaderRef.current.hide();
                        searchHeaderRef.current.clear();
                      }
                      onClearSearch();
                    }}
                  >
                    <FontAwesomeIcon icon="times" size={22} color="white" />
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    onPress={() => {
                      setIsSearching(true);
                      onSearchPressed();
                      if (searchHeaderRef.current) {
                        searchHeaderRef.current.show();
                      }
                    }}
                  >
                    <FontAwesomeIcon icon="search" size={22} color="white" />
                  </TouchableOpacity>
                )}
              </>
            )}
            {onMenuPressed && (
              <TouchableOpacity
                onPress={() => onMenuPressed()}
                style={{ marginLeft: 18 }}
              >
                <FontAwesomeIcon icon="bars" size={22} color="white" />
              </TouchableOpacity>
            )}
          </View>
        }
        centerComponent={
          isSearching ? (
            <View>
              <TextInput
                ref={commentFieldRef}
                onChangeText={changeDelay}
                style={{
                  width: '100%',
                  height: 32,
                  fontSize: 14,
                  borderWidth: 1,
                  borderColor: selectedTheme.primaryColor,
                  backgroundColor: '#fafafa',
                  position: 'absolute',
                  paddingHorizontal: 8,
                  borderRadius: 5,
                  top: -4,
                  left: 0,
                }}
              />
            </View>
          ) : (
            {
              text: title,
              style: { color: '#fff', fontSize: 18 },
            }
          )
        }
      />
    </>
  );
};

export default CommonHeader;
