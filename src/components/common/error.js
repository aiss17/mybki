import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT } from '../../constants/style-constant';

const Error = ({ style = {}, message = null }) => {
  return (
    <>
      <View
        style={[
          {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          },
          {
            ...style,
          },
        ]}
      >
        <FontAwesomeIcon
          icon="exclamation-triangle"
          size={64}
          color={COLOR_CONSTANT.RED}
        />
        <Text style={{ color: COLOR_CONSTANT.RED, marginTop: 18 }}>
          {message || 'Terjadi kendala'}
        </Text>
      </View>
    </>
  );
};

export default Error;
