import React, { ReactNode, useState } from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';

const MultiTouchableOpacity: () => ReactNode = ({
  onLongPress,
  onMultiTaps,
  multiTapCount = 2,
  multiTapDelay = 300,
  children,
}) => {
  const [lastPress, setLastPress] = useState(null);
  const [tapCount, setTapCount] = useState(0);

  const handlePress = () => {
    const now = Date.now();

    setLastPress(now);
    if (now - lastPress <= multiTapDelay) {
      if (tapCount < multiTapCount - 1) {
        setTapCount(tapCount + 1);
      } else {
        setTapCount(0);

        if (onMultiTaps) onMultiTaps();
      }
    } else {
      setTapCount(1);
    }
  };
  const handleLongPress = () => onLongPress && onLongPress();

  return (
    <TouchableOpacity
      delayLongPress={1000}
      activeOpacity={0.8}
      onLongPress={handleLongPress}
      onPress={handlePress}
    >
      {children}
    </TouchableOpacity>
  );
};

export default MultiTouchableOpacity;
