import AnimatedLottieView from 'lottie-react-native';
import React from 'react';
import { View } from 'react-native';

const loadingAnim = require('../../assets/lottie/24863-loading-square-orange-four.json');

const Loading2 = () => {
  return (
    <>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <AnimatedLottieView
          source={loadingAnim}
          style={{ width: 40, height: 40 }}
          autoPlay
          loop
        />
      </View>
    </>
  );
};

export default Loading2;
