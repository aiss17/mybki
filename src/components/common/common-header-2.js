import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import { Header } from 'react-native-elements';
import { selectedTheme } from '../../themes/Theme';

const navBarBg = require('../../assets/bottom-tabs/nav-bg.png');

class CommonHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isSearching: false, timer: null };
    this.searchHeaderRef = React.createRef(null);
    this.commentFieldRef = React.createRef(null);
  }

  componentDidMount() {
    if (this.commentFieldRef && this.commentFieldRef.current) {
      this.commentFieldRef.current.focus();
    }
  }

  changeDelay = (change) => {
    const { timer } = this.state;
    const { onSearch = () => {} } = this.props;
    if (timer) {
      clearTimeout(timer);
      this.setState({ timer: null });
    }
    this.setState({
      timer: setTimeout(() => {
        onSearch(change);
      }, 2000),
    });
  };

  closeFilter() {
    this.setState({ isSearching: false });
  }

  render() {
    const {
      onBackPressed,
      onSearchPressed,
      onMenuPressed,
      onClearSearch = () => {},
      title,
    } = this.props;
    const { isSearching } = this.state;
    return (
      <>
        <Header
          placement="left"
          backgroundColor="orange"
          backgroundImage={navBarBg}
          leftComponent={
            onBackPressed && (
              <TouchableOpacity onPress={() => onBackPressed()}>
                <FontAwesomeIcon icon="chevron-left" size={22} color="white" />
              </TouchableOpacity>
            )
          }
          rightComponent={
            <View style={{ flexDirection: 'row' }}>
              {onSearchPressed && (
                <>
                  {isSearching ? (
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isSearching: false });
                        if (this.searchHeaderRef.current) {
                          this.searchHeaderRef.current.hide();
                          this.searchHeaderRef.current.clear();
                        }
                        onClearSearch();
                      }}
                    >
                      <FontAwesomeIcon icon="times" size={22} color="white" />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isSearching: true });
                        onSearchPressed();
                        if (this.searchHeaderRef.current) {
                          this.searchHeaderRef.current.show();
                        }
                      }}
                    >
                      <FontAwesomeIcon icon="search" size={22} color="white" />
                    </TouchableOpacity>
                  )}
                </>
              )}
              {onMenuPressed && (
                <TouchableOpacity
                  onPress={() => onMenuPressed()}
                  style={{ marginLeft: 18 }}
                >
                  <FontAwesomeIcon icon="bars" size={22} color="white" />
                </TouchableOpacity>
              )}
            </View>
          }
          centerComponent={
            isSearching ? (
              <View>
                <TextInput
                  ref={this.commentFieldRef}
                  onChangeText={this.changeDelay}
                  style={{
                    width: '100%',
                    height: 32,
                    fontSize: 14,
                    borderWidth: 1,
                    borderColor: selectedTheme.primaryColor,
                    backgroundColor: '#fafafa',
                    position: 'absolute',
                    paddingHorizontal: 8,
                    borderRadius: 5,
                    top: -4,
                    left: 0,
                  }}
                />
              </View>
            ) : (
              {
                text: title,
                style: { color: '#fff', fontSize: 18 },
              }
            )
          }
        />
      </>
    );
  }
}

export default CommonHeader;
