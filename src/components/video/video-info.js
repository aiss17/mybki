import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Image, Text, View } from 'react-native';

const VideoInfo = ({
  channelAvatarImageUri,
  videoTitle,
  channelName,
  videoInfo,
}) => {
  return (
    <>
      <View
        style={{
          paddingHorizontal: 15,
          paddingTop: 15,
          paddingBottom: 15,
          flexDirection: 'row',
          marginTop: 5,
        }}
      >
        <View style={{ marginHorizontal: 10 }}>
          <Image
            style={{ width: 40, height: 40, borderRadius: 20, margin: 0 }}
            source={{ uri: channelAvatarImageUri }}
          />
        </View>
        <View style={{ paddingHorizontal: 15, paddingBottom: 15 }}>
          <Text
            style={{
              fontWeight: '600',
              fontSize: 18,
              flex: 1,
              flexWrap: 'wrap',
            }}
          >
            {videoTitle}
          </Text>
          <Text style={{ fontSize: 14, marginTop: 6, color: '#4d4d4d' }}>
            {channelName} · {videoInfo.description.views} ·{' '}
            {videoInfo.description.uploadDate}
          </Text>
        </View>
        <View>
          <FontAwesomeIcon icon="ellipsis-v" size={22} color="black" />
        </View>
      </View>
    </>
  );
};

export default VideoInfo;
