import React from 'react';
import { View } from 'react-native';
import { STYLE_CONSTANT } from '../../constants/style-constant';
import VideoInfo from './video-info';
import VideoThumbnail from './video-thumbnail';

const YoutubeLikePaper = ({ videoInfo, channelInfo }) => {
  return (
    <>
      <View style={[STYLE_CONSTANT.SHADOW2, { marginBottom: 18 }]}>
        <VideoThumbnail
          thumbnailUrl={videoInfo.videoThumbnailUrl}
          videoLength={videoInfo.videoLength}
        />
        <VideoInfo
          videoTitle={videoInfo.title}
          videoInfo={videoInfo}
          channelName={channelInfo.channelName}
          channelAvatarImageUri={channelInfo.channelAvatarImage}
        />
      </View>
    </>
  );
};

export default YoutubeLikePaper;
