import React from 'react';
import { View } from 'react-native';
import ImageWithLoading from '../images/image-with-loading';
import VideoLength from './video-length';

const VideoThumbnail = ({ thumbnailUrl, videoLength }) => {
  return (
    <>
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ flex: 1, width: null, height: 200 }}>
          <ImageWithLoading
            imageUri={thumbnailUrl}
            imageStyle={{
              height: 200,
            }}
          />
          <VideoLength
            style={{
              position: 'absolute',
              right: 20,
              top: -35,
              backgroundColor: 'black',
              color: 'white',
              borderRadius: 3,
              paddingHorizontal: 7,
              textAlign: 'right',
              overflow: 'hidden',
            }}
            videoLength={videoLength}
          />
        </View>
      </View>
    </>
  );
};

export default VideoThumbnail;
