import React from 'react';
import { Text, View } from 'react-native';

const VideoLength = ({ style, videoLength }) => {
  return (
    <>
      <View>
        <Text style={[{ ...style }]}>{videoLength}</Text>
      </View>
    </>
  );
};

export default VideoLength;
