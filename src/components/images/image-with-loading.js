import React from 'react';
import { ActivityIndicator, Image, View } from 'react-native';

const ImageWithLoading = ({
  imageUri,
  style = {},
  imageStyle = {},
  backupImage = null,
}) => {
  const [loading, setLoading] = React.useState(true);
  const [defaultImage, setDefaultImage] = React.useState(null);

  return (
    <>
      <View style={{ ...style }}>
        <Image
          style={[{ width: '100%', height: '100%' }, { ...imageStyle }]}
          source={defaultImage != null ? defaultImage : { uri: imageUri }}
          onLoadEnd={() => setLoading(false)}
          onError={() => {
            setDefaultImage(backupImage);
          }}
        />
        <ActivityIndicator
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
          }}
          animating={loading}
        />
      </View>
    </>
  );
};

export default ImageWithLoading;
