import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
  container: {},
});

const ChildItem = ({
  item,
  style,
  onPress,
  index,
  imageKey,
  local,
  height,
}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={() => onPress(index)}>
      <View
        style={[
          style,
          {
            height,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          },
        ]}
      >
        <Image
          style={[
            {
              flex: 1,
              aspectRatio: 1,
              resizeMode: 'contain',
            },
          ]}
          source={local ? item[imageKey] : { uri: item[imageKey] }}
        />
      </View>
    </TouchableOpacity>
  );
};

export default ChildItem;
