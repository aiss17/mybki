import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Text, View } from 'react-native';
import { COLOR_CONSTANT } from '../../constants/style-constant';

const ContactList = ({
  items = [
    {
      id: 1,
      name: 'Contact 1',
      phone: '02122323',
      email: null,
      address: 'Jakarta',
    },
  ],
}) => {
  return (
    <>
      <View>
        {items.map((t) => {
          return (
            <View style={{ flexDirection: 'row', marginBottom: 26 }} key={t.id}>
              <View style={{ paddingTop: 4 }}>
                <FontAwesomeIcon
                  icon="map-marker"
                  size={18}
                  color={COLOR_CONSTANT.DARKBLUE}
                />
              </View>
              <View style={{ marginLeft: 8 }}>
                <Text
                  style={{
                    color: COLOR_CONSTANT.DARKBLUE,
                    fontWeight: 'bold',
                    fontSize: 18,
                  }}
                >
                  {t.name}
                </Text>
                <Text>Address: {t.address || '-'}</Text>
                <Text>Phone: {t.phone || '-'}</Text>
                <Text>email: {t.email || '-'}</Text>
              </View>
            </View>
          );
        })}
      </View>
    </>
  );
};

export default ContactList;
