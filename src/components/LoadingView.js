import React from 'react';
import { ActivityIndicator, View } from 'react-native';

const LoadingView = ({ size = 'small', color = 'gray' }) => {
  return (
    <>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-around',
          padding: 10,
        }}
      >
        <ActivityIndicator size={size} color={color} />
      </View>
    </>
  );
};

export default LoadingView;
