import React from 'react';
import { View, Text } from 'react-native';

const Badge = ({ count }) => (
  <View
    style={{
      width: 6,
      height: 6,
      borderRadius: 12, //half radius will make it cirlce,
      backgroundColor: 'red',
    }}
  >
    <Text style={{ color: '#FFF', fontSize: 8 }}>{count}</Text>
  </View>
);

export default Badge;
