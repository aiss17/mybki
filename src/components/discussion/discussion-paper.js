import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Text, View } from 'react-native';
import { Avatar } from 'react-native-paper';
import { COLOR_CONSTANT, STYLE_CONSTANT } from '../../constants/style-constant';

const avatarImage = require('../../assets/demo/avatar.png');

const STYLE = {
  TAB_BUTTON: {
    borderRadius: 10,
    backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
    paddingVertical: 6,
    paddingHorizontal: 12,
    marginHorizontal: 4,
  },
  TAB_BUTTON_ACTIVE: {
    backgroundColor: COLOR_CONSTANT.GRAY,
  },
};

const DiscussionPaper = () => {
  return (
    <>
      <View
        style={[
          STYLE_CONSTANT.SHADOW1,
          {
            width: '100%',
            backgroundColor: 'white',
            borderRadius: 5,
          },
        ]}
      >
        <View
          style={{
            flexDirection: 'row',
            padding: 12,
          }}
        >
          <View
            style={{
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                paddingRight: 6,
                fontSize: 11,
                color: COLOR_CONSTANT.GRAY,
              }}
            >
              Admin IDI
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontSize: 11,
                color: COLOR_CONSTANT.GRAY,
                paddingRight: 6,
              }}
            >
              ·
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
            }}
          >
            <Text style={{ fontSize: 11, color: COLOR_CONSTANT.GRAY }}>
              21/04/2020
            </Text>
          </View>
        </View>
        <View style={{ paddingHorizontal: 12 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 14 }}>
            Menunda kehamilan
          </Text>
        </View>
        <View
          style={{
            paddingHorizontal: 12,
            paddingVertical: 12,
            flexDirection: 'row',
          }}
        >
          <Avatar.Image size={18} source={avatarImage} />
          <View style={{ justifyContent: 'center', paddingHorizontal: 8 }}>
            <Text style={{ fontSize: 12 }}>Info Penanya: Ahsan</Text>
          </View>
        </View>
        <View
          style={{
            marginHorizontal: 12,
            height: 1,
            backgroundColor: COLOR_CONSTANT.LIGHTGRAY,
          }}
        />
        <View style={{ padding: 12 }}>
          <Text>
            Assalamualaikum dok Saya baru pertama mengkonsumsi pil kb andalan,
            saya memulai dari yang warna kuning(untuk mens) tapi saya tidak
            dalam keadaan mens dan tidak sesuai hari pada saat itu, saya memulai
            dari hari senin(untuk mens)padahal saat itu hari jumat dan saya
            tidak dalam keadan menstruasi , dan saya tetap mengkonsumsi sesuai
            arah panah , nah jadi pertanyaan saya apakah pil nya tetap bisa
            bekerja mencegah kehamilan? , untuk selanjutnya cara mengkonsumsi
            yang benar bagaimana dok?
          </Text>
        </View>
        <View
          style={{
            marginHorizontal: 12,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            marginBottom: 12,
          }}
        >
          <View style={[STYLE.TAB_BUTTON, { flexDirection: 'row' }]}>
            <FontAwesomeIcon icon="eye" size={10} color="black" />
            <Text style={{ fontSize: 9, paddingLeft: 6 }}>6 Views</Text>
          </View>
          <View style={[STYLE.TAB_BUTTON, { flexDirection: 'row' }]}>
            <FontAwesomeIcon icon="comment" size={10} color="black" />
            <Text style={{ fontSize: 9, paddingLeft: 6 }}>0 Balasan</Text>
          </View>
        </View>
      </View>
    </>
  );
};

export default DiscussionPaper;
