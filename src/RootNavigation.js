import React from 'react';

export const navigationRef = React.createRef();
export const appContainerRef = React.createRef();

export function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}
