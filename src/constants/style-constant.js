export const COLOR_CONSTANT = {
  RED: '#DE4058',
  DARKBLUE: '#0E456B',
  BLUE: '#25AADB',
  GRAY: '#DDDDDD',
  DARKGRAY: '#838383',
  LIGHTGRAY: '#F2F2F2',
  WHITE: 'white',
  BLACK: '#363636',
  ORANGE: '#FDBF0E',
  DARKORANGE: '#E25B25',
  CYAN: '#01A29A',
  GREEN: '#69C33B',
};

/* eslint-disable no-undef */
export const STYLE_CONSTANT = {
  COLOR_LIGHT: {
    color: 'white',
  },
  FLEX: {
    flex: 1,
  },
  FLEX_COLUMN: {
    flex: 1,
    flexDirection: 'column',
  },
  FLEX_ROW: {
    flex: 1,
    flexDirection: 'row',
  },
  FONT_BOLD: {
    fontWeight: 'bold',
  },
  FONT_SHADOW1: {
    textShadowColor: COLOR_CONSTANT.BLACK,
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
  },
  SHADOW1: {
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  SHADOW2: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
  },
  SHADOW3: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.9,
    shadowRadius: 10,
    elevation: 10,
  },
  RADIUS1: {
    borderRadius: 5,
    overflow: 'hidden',
  },
  FLEX_CENTER_VH: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
};
