/* eslint-disable import/prefer-default-export */
export const INIT_PAGE_STATE = {
  currentPage: 0,
  pageSizes: [10, 15, 20],
  offset: 0,
  limit: 5,
  count: 0,
  currentCount: 0,
  hasNext: false,
};

export const Status = {
  Draft: 'Draft',
  Onprogress: 'On Progress',
  Done: 'Done',
  Cancel: 'Cancel',
};

export const StatusVisit = {
  Pending: 'Pending',
  Onprogress: 'On Progress',
  Done: 'Done',
  Cancel: 'Cancel',
};

export const StatusCheckList = {
  NA: 'N/A',
  Progress: 'Progress',
  Finish: 'Finish',
};
