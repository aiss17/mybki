export const COLOR_CONSTANT = {
  RED: '#FF7878',
  BLUE: '#155264',
  GRAY: 'gray',
  DARKGRAY: '#525353',
  LIGHTGRAY: 'lightgray',
  WHITE: '#F3F3F3',
  BLACK: 'black',
  ORANGE: '#F96400',
  DARKBLUE: '#0E456B',
};
