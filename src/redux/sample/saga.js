import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { GET } from '../../helpers/apiHelper';
import * as action from './action';

const fetchAllDataAsync = async () =>
  GET(`https://jsonplaceholder.typicode.com/users`);

function* fetchAllData() {
  try {
    yield call(fetchAllDataAsync);
    yield put(action.fetchAllDataSuccess());
  } catch (error) {
    yield put(action.fetchAllDataError(error));
  }
}

export function* watchFetchAllData() {
  yield takeEvery(action.FETCH_ALL_DATA, fetchAllData);
}

export default function* rootSaga() {
  yield all([fork(watchFetchAllData)]);
}
