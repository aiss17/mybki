export const FETCH_ALL_DATA = 'FETCH_ALL_DATA';
export const FETCH_ALL_DATA_SUCCESS = 'FETCH_ALL_DATA_SUCCESS';
export const FETCH_ALL_DATA_ERROR = 'FETCH_ALL_DATA_ERROR';

export const fetchAllData = () => ({
  type: FETCH_ALL_DATA,
});

export const fetchAllDataSuccess = (data) => ({
  type: FETCH_ALL_DATA_SUCCESS,
  payload: data,
});

export const fetchAllDataError = (error) => ({
  type: FETCH_ALL_DATA_ERROR,
  payload: error,
});
