import {
  FETCH_ALL_DATA,
  FETCH_ALL_DATA_ERROR,
  FETCH_ALL_DATA_SUCCESS,
} from './action';

const INIT_STATE = {
  list: {
    data: [],
    loading: false,
    error: false,
    message: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ALL_DATA:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case FETCH_ALL_DATA_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        error: false,
      };
    case FETCH_ALL_DATA_ERROR:
      return {
        ...state,
        data: [],
        loading: false,
        error: true,
        message: action.payload,
      };
    default:
      return state;
  }
};
