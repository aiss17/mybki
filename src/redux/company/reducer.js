import {
  LOAD_COMPANY,
  LOAD_COMPANY_ERROR,
  LOAD_COMPANY_SUCCESS,
} from './action';

const INIT_STATE = {
  list: {
    loading: false,
    error: null,
    data: null,
    message: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_COMPANY:
      return {
        ...state,
        list: {
          ...state.list,
          loading: true,
          error: null,
          data: null,
        },
      };
    case LOAD_COMPANY_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          error: false,
          message: 'Success',
          data: action.payload.response[0],
        },
      };
    case LOAD_COMPANY_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          error: true,
          data: null,
          message: action.payload,
        },
      };

    default:
      return state;
  }
};
