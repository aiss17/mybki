export const LOAD_COMPANY = 'LOAD_COMPANY';
export const LOAD_COMPANY_SUCCESS = 'LOAD_COMPANY_SUCCESS';
export const LOAD_COMPANY_ERROR = 'LOAD_COMPANY_ERROR';

export const loadCompany = (id) => {
  return {
    type: LOAD_COMPANY,
    payload: id,
  };
};

export const loadCompanySuccess = (data) => {
  return {
    type: LOAD_COMPANY_SUCCESS,
    payload: data,
  };
};

export const loadCompanyError = (error) => {
  return {
    type: LOAD_COMPANY_ERROR,
    payload: error,
  };
};
