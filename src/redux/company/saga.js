import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { CRMPOST } from '../../helpers/apiHelper';
import * as action from './action';

const companyAsync = async (id) => CRMPOST(`api/updateCompany/id/${id}`);

function* loadCompany({ payload }) {
  try {
    const response = yield call(companyAsync, payload);
    yield put(action.loadCompanySuccess(response));
  } catch (error) {
    yield put(action.loadCompanyError(error));
  }
}

export function* watchLoadCompany() {
  yield takeEvery(action.LOAD_COMPANY, loadCompany);
}

export default function* rootSaga() {
  yield all([fork(watchLoadCompany)]);
}
