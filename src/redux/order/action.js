import { INIT_PAGE_STATE } from '../../constants/constants';

export const LOAD_ORDERS = 'LOAD_ORDER';
export const LOAD_ORDERS_SUCCESS = 'LOAD_ORDERS_SUCCESS';
export const LOAD_ORDERS_ERROR = 'LOAD_ORDERS_ERROR';

export const loadOrders = (p) => {
  const param = {
    ...{
      selectedPage: 1,
      filter: null,
      orderBy: 'id',
      sortedBy: 'asc',
      limit: INIT_PAGE_STATE.pageSizes[0],
    },
    ...p,
  };
  return {
    type: LOAD_ORDERS,
    payload: {
      selectedPage: param.selectedPage,
      filter: param.filter,
      orderBy: param.orderBy,
      sortedBy: param.sortedBy,
      limit: param.limit,
    },
  };
};

export const loadOrdersSuccess = (data) => ({
  type: LOAD_ORDERS_SUCCESS,
  payload: data,
});

export const loadOrdersError = (error) => ({
  type: LOAD_ORDERS_ERROR,
  payload: error,
});
