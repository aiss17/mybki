// @ts-nocheck
import { all, call, fork, put, select, takeEvery } from 'redux-saga/effects';
import { GET } from '../../helpers/apiHelper';
import * as action from './action';

export const getListState = (state) => state.orderApp.list;

const loadOrdersAsync = async (query) => GET(`rfq_myorder?${query}`);

function* loadOrders({ payload }) {
  try {
    const list = yield select(getListState);

    let param = [
      {
        name: 'page',
        value: payload.selectedPage,
      },
      {
        name: 'orderBy',
        value: list.orderBy,
      },
      {
        name: 'sortedBy',
        value: list.sortedBy,
      },
      {
        name: 'limit',
        value: payload.limit,
      },
    ];

    if (list.filter || '') {
      param = [
        ...param,
        ...[
          {
            name: 'search',
            value: list.filter,
          },
          {
            name: 'searchFields',
            value: list.filterFields,
          },
          {
            name: 'searchJoin',
            value: list.searchJoin,
          },
        ],
      ];
    }

    const response = yield call(
      loadOrdersAsync,
      param.map((t) => `${t.name}=${t.value}`).join('&')
    );

    yield put(action.loadOrdersSuccess(response));
  } catch (error) {
    yield put(action.loadOrdersError(error));
  }
}

export function* watchLoadLoadOrders() {
  yield takeEvery(action.LOAD_ORDERS, loadOrders);
}

export default function* rootSaga() {
  yield all([fork(watchLoadLoadOrders)]);
}
