/* eslint-disable no-else-return */
/* eslint-disable eqeqeq */
import { INIT_PAGE_STATE } from '../../constants/constants';

import { LOAD_ORDERS, LOAD_ORDERS_ERROR, LOAD_ORDERS_SUCCESS } from './action';

const INIT_STATE = {
  list: {
    data: null,
    loading: false,
    error: false,
    filter: null,
    orderBy: null,
    sortedBy: 'asc',
    filterFields: 'name:ilike;id:ilike',
    searchJoin: 'or',
    page: INIT_PAGE_STATE,
    message: null,
    isListEnd: false,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_ORDERS:
      return {
        ...state,
        list: {
          ...state.list,
          data: action.payload.selectedPage == 1 ? null : state.list.data,
          loading: true,
          error: false,
          filter: action.payload.filter,
          orderBy: action.payload.orderBy,
          sortedBy: action.payload.sortedBy,
          page: INIT_PAGE_STATE,
          message: null,
        },
      };

    case LOAD_ORDERS_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          data:
            action.payload.meta.pagination.current_page > 1
              ? [
                  ...state.list.data,
                  ...action.payload.data.map((t) => {
                    return {
                      ...t,
                      ...t.attributes,
                    };
                  }),
                ]
              : action.payload.data.map((t) => {
                  return {
                    ...t,
                    ...t.attributes,
                  };
                }),
          page:
            action.payload.meta && action.payload.meta.pagination
              ? {
                  ...state.list.page,
                  currentPage: action.payload.meta.pagination.current_page,
                  selectedPageSize: action.payload.meta.pagination.per_page,
                  totalItemCount: action.payload.meta.pagination.total,
                  totalPage: action.payload.meta.pagination.total_pages,
                  itemCount: action.payload.meta.pagination.count,
                }
              : state.list.page,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_ORDERS_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: false,
          error: true,
          page: INIT_PAGE_STATE,
          message: action.payload,
        },
      };

    default:
      return state;
  }
};
