export const SET_CURRENT = '[TRACKING] SET_CURRENT';
export const SAVE_LOCATION = '[TRACKING] SAVE_LOCATION';
export const SYNC_LOCATION = '[TRACKING] SYNC_LOCATION';
export const SYNC_LOCATION_SUCCESS = '[TRACKING] SYNC_LOCATION_SUCCESS';

export const setCurrentLocation = ({
  data = {
    lat: null,
    long: null,
    alt: null,
    waktu: null,
  },
}) => ({
  type: SET_CURRENT,
  payload: { data },
});

export const saveLocation = ({ data }) => ({
  type: SAVE_LOCATION,
  payload: { data },
});

export const syncLocation = () => ({
  type: SYNC_LOCATION,
});

export const syncLocationSuccess = ({ succeedData }) => ({
  type: SYNC_LOCATION_SUCCESS,
  payload: { succeedData },
});
