import { SAVE_LOCATION, SET_CURRENT, SYNC_LOCATION_SUCCESS } from './actions';

const INIT_STATE = {
  data: [],
  current: {
    lat: null,
    long: null,
    alt: null,
    waktu: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SAVE_LOCATION:
      return {
        ...state,
        data: [
          ...state.data,
          {
            lat: action.payload.data.lat,
            long: action.payload.data.long,
            alt: action.payload.data.alt,
            waktu: action.payload.data.waktu,
          },
        ],
      };
    case SET_CURRENT:
      return {
        ...state,
        current: {
          lat: action.payload.data.lat || state.current.lat,
          long: action.payload.data.long || state.current.long,
          alt: action.payload.data.alt || state.current.alt,
          waktu: action.payload.data.waktu || state.current.waktu,
        },
      };
    case SYNC_LOCATION_SUCCESS:
      return {
        ...state,
        data: [
          ...state.data.filter(
            (t) => !action.payload.succeedData.some((v) => v.waktu === t.waktu)
          ),
        ],
      };
    default:
      return { ...state };
  }
};
