/* eslint-disable eqeqeq */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { CRMGET, GET, POST2, RICOGET, CRMPOST2 } from '../../helpers/apiHelper';
import * as action from './action';

// export const getListState = (state) => state.servicesApp.list;

const servicesAsync = async () => CRMGET('api/getItems');
const unitAsync = async () => GET(`master/unit`);
const vesselAsync = async (id) => RICOGET(`/api/getvessels/${id}`);

const rfqCrmAsync = async (item) => {
  const test = await CRMPOST2('api/postRfq', item);
  return test;
};

const addRFQAsync = async (item) => {
  const test = await POST2(`/rfq`, item);
  return test;
};

function* loadServices() {
  let response = null;
  console.log('masuk');
  try {
    response = yield call(servicesAsync);
    console.log('response', response);
    yield put(action.loadServicesSuccess(response));
  } catch (error) {
    yield put(action.loadServicesError(error));
  }
}

function* loadUnit() {
  let response = null;
  try {
    response = yield call(unitAsync);
    yield put(action.loadUnitSuccess(response));
  } catch (error) {
    yield put(action.loadUnitError(error));
  }
}

function* loadVessel({ payload }) {
  let response = null;
  try {
    response = yield call(vesselAsync, payload);
    yield put(action.loadVesselSuccess(response));
  } catch (error) {
    yield put(action.loadVesselError(error));
  }
}

function* saveRfqCrm({ payload }) {
  let responseCRM = null;
  let responseMYBKI = null;

  const data = new FormData();
  data.append('rfq_date', payload.crm.rfq_date);
  data.append('client', payload.crm.client);
  data.append('department', payload.crm.department);
  data.append('service_group', payload.crm.service_group);
  data.append('status', payload.crm.status);
  data.append('note', payload.crm.note);
  data.append('items', JSON.stringify(payload.crm.items));

  try {
    if (payload) {
      responseCRM = yield call(rfqCrmAsync, data);
      if (responseCRM.metadata.code == 200) {
        responseMYBKI = yield call(
          addRFQAsync,
          JSON.stringify({
            ...payload.rico,
            status: '1',
            rfq_no: responseCRM.data,
          })
        );
        console.log('response1', responseMYBKI);
      }
      if (
        payload.crm.service_group === 'klasifikasi' &&
        responseCRM.metadata.code == 200
      ) {
        yield put(action.saveRfqCrmKlasifikasiSuccess(responseCRM));
      } else if (
        payload.crm.service_group === 'komersil' &&
        responseCRM.metadata.code === 200
      ) {
        yield put(action.saveRfqCrmKomersilSuccess(responseCRM));
      }
    } else {
      throw new Error('invalid save item, cannot be null');
    }
  } catch (error) {
    yield put(action.saveRfqCrmError(error));
  }
}

export function* watchLoadServices() {
  yield takeEvery(action.LOAD_SERVICES, loadServices);
}

export function* watchLoadUnit() {
  yield takeEvery(action.LOAD_UNIT, loadUnit);
}

export function* watchLoadVessel() {
  yield takeEvery(action.LOAD_VESSEL, loadVessel);
}

export function* watchSaveRfqCrm() {
  yield takeEvery(action.SAVE_RFQ_CRM, saveRfqCrm);
}

export default function* rootSaga() {
  yield all([
    fork(watchLoadServices),
    fork(watchLoadUnit),
    fork(watchLoadVessel),
    fork(watchSaveRfqCrm),
  ]);
}
