import { INIT_PAGE_STATE } from '../../constants/constants';

import {
  LOAD_SERVICES,
  LOAD_SERVICES_ERROR,
  LOAD_SERVICES_SUCCESS,
  LOAD_UNIT,
  LOAD_UNIT_ERROR,
  LOAD_UNIT_SUCCESS,
  LOAD_VESSEL,
  LOAD_VESSEL_ERROR,
  LOAD_VESSEL_SUCCESS,
  SAVE_RFQ_CRM,
  SAVE_RFQ_CRM_ERROR,
  SAVE_RFQ_CRM_KLASIFIKASI_SUCCESS,
  SAVE_RFQ_CRM_KOMERSIL_SUCCESS,
  SELECTED_SERVICES,
  SELECTED_CART,
  CLEAR_CART,
  CLEAR_RFQ,
  REMOVE_SERVICES,
} from './action';

const INIT_STATE = {
  list: {
    data: null,
    loading: false,
    error: false,
    filter: null,
    orderBy: null,
    sortedBy: 'asc',
    filterFields: 'name:ilike;id:ilike',
    searchJoin: 'or',
    page: INIT_PAGE_STATE,
    message: null,
  },
  selectedServices: {
    data: null,
  },
  selectedCart: {
    klasifikasi: null,
    komersil: null,
    message: null,
  },
  unit: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  vessel: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  rfqCRM: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  remove: {
    id: null,
    status: null,
    message: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_SERVICES:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: true,
          error: false,
          //   filter: action.payload.filter,
          //   orderBy: action.payload.orderBy,
          //   sortedBy: action.payload.sortedBy,
          page: INIT_PAGE_STATE,
          message: null,
        },
      };
    case LOAD_SERVICES_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          data: action.payload.response,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_SERVICES_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: false,
          error: true,
          page: INIT_PAGE_STATE,
          message: action.payload,
        },
      };

    case SELECTED_SERVICES:
      return {
        ...state,
        selectedServices: {
          ...state.selectedServices,
          data: action.payload,
        },
      };

    case SELECTED_CART:
      return {
        ...state,
        selectedCart: {
          ...state.selectedCart,
          klasifikasi: action.payload.klasifikasi,
          komersil: action.payload.komersil,
        },
      };

    case LOAD_UNIT:
      return {
        ...state,
        unit: {
          ...state.unit,
          data: null,
          loading: true,
          error: false,
          message: null,
        },
      };
    case LOAD_UNIT_SUCCESS:
      return {
        ...state,
        unit: {
          ...state.unit,
          data: action.payload.data,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_UNIT_ERROR:
      return {
        ...state,
        unit: {
          ...state.unit,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    case LOAD_VESSEL:
      return {
        ...state,
        vessel: {
          ...state.vessel,
          data: action.payload.id,
          loading: true,
          error: false,
          message: null,
        },
      };
    case LOAD_VESSEL_SUCCESS:
      return {
        ...state,
        vessel: {
          ...state.vessel,
          data: action.payload.response,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_VESSEL_ERROR:
      return {
        ...state,
        vessel: {
          ...state.vessel,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    case SAVE_RFQ_CRM:
      return {
        ...state,
        rfqCRM: {
          ...state.rfqCRM,
          data: action.payload,
          loading: true,
          error: false,
          message: null,
        },
      };
    case SAVE_RFQ_CRM_KLASIFIKASI_SUCCESS:
      return {
        ...state,
        rfqCRM: {
          ...state.rfqCRM,
          data: null,
          loading: false,
          error: false,
          message: 'Klasifikasi',
        },
      };
    case SAVE_RFQ_CRM_KOMERSIL_SUCCESS:
      return {
        ...state,
        rfqCRM: {
          ...state.rfqCRM,
          data: null,
          loading: false,
          error: false,
          message: 'Komersil',
        },
      };
    case SAVE_RFQ_CRM_ERROR:
      return {
        ...state,
        rfqCRM: {
          ...state.rfqCRM,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    case CLEAR_CART:
      return {
        ...state,
        selectedCart: {
          ...state.selectedCart,
          data: null,
          message:
            action.payload === 'Klasifikasi' ? 'Klasifikasi' : 'Komersil',
        },
      };

    case CLEAR_RFQ:
      return {
        ...state,
        rfqCRM: {
          ...state.rfqCRM,
          data: null,
          message: null,
        },
      };

    case REMOVE_SERVICES:
      return {
        ...state,
        remove: {
          ...state.remove,
          id: action.payload.id,
          status: action.payload.status,
          message: 'Success',
        },
      };

    default:
      return state;
  }
};
