export const LOAD_SERVICES = 'LOAD_SERVICES';
export const LOAD_SERVICES_SUCCESS = 'LOAD_SERVICES_SUCCESS';
export const LOAD_SERVICES_ERROR = 'LOAD_SERVICES_ERROR';

export const LOAD_UNIT = 'LOAD_UNIT';
export const LOAD_UNIT_SUCCESS = 'LOAD_UNIT_SUCCESS';
export const LOAD_UNIT_ERROR = 'LOAD_UNIT_ERROR';

export const LOAD_VESSEL = 'LOAD_VESSEL';
export const LOAD_VESSEL_SUCCESS = 'LOAD_VESSEL_SUCCESS';
export const LOAD_VESSEL_ERROR = 'LOAD_VESSEL_ERROR';

export const SAVE_RFQ_CRM = 'SAVE_RFQ_CRM';
export const SAVE_RFQ_CRM_KLASIFIKASI_SUCCESS =
  'SAVE_RFQ_CRM_KLASIFIKASI_SUCCESS';
export const SAVE_RFQ_CRM_KOMERSIL_SUCCESS = 'SAVE_RFQ_CRM_KOMERSIL_SUCCESS';
export const SAVE_RFQ_CRM_ERROR = 'SAVE_RFQ_CRM_ERROR';

export const SELECTED_SERVICES = 'SELECTED_SERVICES';

export const SELECTED_CART = 'SELECTED_CART';

export const CLEAR_CART = 'CLEAR_CART';

export const CLEAR_RFQ = 'CLEAR_RFQ';

export const REMOVE_SERVICES = 'REMOVE_SERVICES';

export const loadServices = () => ({
  type: LOAD_SERVICES,
});

export const loadServicesSuccess = (data) => ({
  type: LOAD_SERVICES_SUCCESS,
  payload: data,
});

export const loadServicesError = (error) => ({
  type: LOAD_SERVICES_ERROR,
  payload: error,
});

export const loadUnit = () => ({
  type: LOAD_UNIT,
});

export const loadUnitSuccess = (data) => ({
  type: LOAD_UNIT_SUCCESS,
  payload: data,
});

export const loadUnitError = (error) => ({
  type: LOAD_UNIT_ERROR,
  payload: error,
});

export const loadVessel = (id) => ({
  type: LOAD_VESSEL,
  payload: id,
});

export const loadVesselSuccess = (data) => ({
  type: LOAD_VESSEL_SUCCESS,
  payload: data,
});

export const loadVesselError = (error) => ({
  type: LOAD_VESSEL_ERROR,
  payload: error,
});

export const saveRfqCrm = (crm, rico) => ({
  type: SAVE_RFQ_CRM,
  payload: { crm, rico },
});

export const saveRfqCrmKlasifikasiSuccess = (data) => ({
  type: SAVE_RFQ_CRM_KLASIFIKASI_SUCCESS,
  payload: data,
});

export const saveRfqCrmKomersilSuccess = (data) => ({
  type: SAVE_RFQ_CRM_KOMERSIL_SUCCESS,
  payload: data,
});

export const saveRfqCrmError = (error) => ({
  type: SAVE_RFQ_CRM_ERROR,
  payload: error,
});

export const selectedCart = (klasifikasi, komersil) => {
  return {
    type: SELECTED_CART,
    payload: { klasifikasi, komersil },
  };
};

export const selectedServices = (data) => {
  return {
    type: SELECTED_SERVICES,
    payload: data,
  };
};

export const clearCart = (status) => {
  return {
    type: CLEAR_CART,
    payload: status,
  };
};

export const clearRfq = () => {
  return {
    type: CLEAR_RFQ,
  };
};

export const removeServices = (id, status) => {
  return {
    type: REMOVE_SERVICES,
    payload: { id, status },
  };
};
