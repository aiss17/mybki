import {
  LOAD_CERTIFICATE_BKI,
  LOAD_CERTIFICATE_BKI_SUCCESS,
  LOAD_CERTIFICATE_BKI_ERROR,
} from './action';

const INIT_STATE = {
  list: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_CERTIFICATE_BKI:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: true,
          error: false,
          message: action?.payload?.message,
        },
      };
    case LOAD_CERTIFICATE_BKI_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          data: action?.payload?.data?.map((t) => {
            return {
              ...t,
            };
          }),
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_CERTIFICATE_BKI_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: false,
          error: true,
          message: action?.payload,
        },
      };

    default:
      return state;
  }
};
