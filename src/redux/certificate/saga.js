import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { POST } from '../../helpers/apiHelper';
import * as action from './action';

const ceritifcateAsync = async (data) =>
  POST('/api-certif/getCertificateByIdCustomer', data);

function* loadCertificateBki({ payload }) {
  try {
    const response = yield call(ceritifcateAsync, payload);
    yield put(action.loadCertificateBkiSuccess(response));
  } catch (error) {
    yield put(action.loadCertificateBkiError(error));
  }
}

export function* watchLoadCertificateBki() {
  yield takeEvery(action.LOAD_CERTIFICATE_BKI, loadCertificateBki);
}

export default function* rootSaga() {
  yield all([fork(watchLoadCertificateBki)]);
}
