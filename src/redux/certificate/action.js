export const LOAD_CERTIFICATE_BKI = 'LOAD_CERTIFICATE_BKI';
export const LOAD_CERTIFICATE_BKI_SUCCESS = 'LOAD_CERTIFICATE_BKI_SUCCESS';
export const LOAD_CERTIFICATE_BKI_ERROR = 'LOAD_CERTIFICATE_BKI_ERROR';

export const loadCertificateBki = (data) => ({
  type: LOAD_CERTIFICATE_BKI,
  payload: data,
});

export const loadCertificateBkiSuccess = (data) => ({
  type: LOAD_CERTIFICATE_BKI_SUCCESS,
  payload: data,
});

export const loadCertificateBkiError = (error) => ({
  type: LOAD_CERTIFICATE_BKI_ERROR,
  payload: error,
});
