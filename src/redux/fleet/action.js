import { INIT_PAGE_STATE } from "../../constants/constants";

export const LOAD_FLEET = 'LOAD_FLEET';
export const LOAD_FLEET_SUCCESS = 'LOAD_FLEET_SUCCESS';
export const LOAD_FLEET_ERROR = 'LOAD_FLEET_ERROR';
export const LOAD_VESSEL_DETAIL = 'LOAD_VESSEL_DETAIL';
export const LOAD_VESSEL_DETAIL_SUCCESS = 'LOAD_VESSEL_DETAIL_SUCCESS';
export const LOAD_VESSEL_DETAIL_ERROR = 'LOAD_VESSEL_DETAIL_ERROR';
export const LOAD_SURVEY_AUDIT = 'LOAD_SURVEY_AUDIT';
export const LOAD_SURVEY_AUDIT_SUCCESS = 'LOAD_SURVEY_AUDIT_SUCCESS';
export const LOAD_SURVEY_AUDIT_ERROR = 'LOAD_SURVEY_AUDIT_ERROR';


export const loadFleets = (p) => {
    const param = {
        ...{
            selectedPage: 1,
            orderBy: 'id',
            sortedBy: 'asc',
            limit: INIT_PAGE_STATE.pageSizes[0],
        },
        ...p,
    };
    return {
        type: LOAD_FLEET,
        payload: {
            selectedPage: param.selectedPage,
            orderBy: param.orderBy,
            sortedBy: param.sortedBy,
            limit: param.limit,
        },
    };
};

export const loadFleetsSuccess = (data) => ({
    type: LOAD_FLEET_SUCCESS,
    payload: data,
});

export const loadFleetsError = (error) => ({
    type: LOAD_FLEET_ERROR,
    payload: error,
})

export const loadVessel = (code) => ({
    type: LOAD_VESSEL_DETAIL,
    payload: code,
})

export const loadVesselSuccess = (data) => ({
    type: LOAD_VESSEL_DETAIL_SUCCESS,
    payload: data,
})

export const loadVesselError = (error) => ({
    type: LOAD_VESSEL_DETAIL_ERROR,
    payload: error,
})

export const loadSurveyAudit = (code) => ({
    type: LOAD_SURVEY_AUDIT,
    payload: code,
})

export const loadSurveyAuditSuccess = (data) => ({
    type: LOAD_SURVEY_AUDIT_SUCCESS,
    payload: data,
})

export const loadSurveyAuditError = (error) => ({
    type: LOAD_SURVEY_AUDIT_ERROR,
    payload: error,
})