import { INIT_PAGE_STATE } from '../../constants/constants';

import { LOAD_FLEET, LOAD_FLEET_SUCCESS, LOAD_FLEET_ERROR, LOAD_VESSEL_DETAIL_SUCCESS, LOAD_VESSEL_DETAIL_ERROR, LOAD_VESSEL_DETAIL, LOAD_SURVEY_AUDIT, LOAD_SURVEY_AUDIT_SUCCESS, LOAD_SURVEY_AUDIT_ERROR } from './action';

const INIT_STATE = {
  list: {
    data: null,
    loading: false,
    orderBy: null,
    error: false,
    sortedBy: 'asc',
    page: INIT_PAGE_STATE,
    message: null,
  },
  vessel: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  survey: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_FLEET:
      return {
        ...state,
        list: {
          ...state.list,
          data: state.list.data,
          loading: true,
          error: false,
          orderBy: action.payload.orderBy,
          sortedBy: action.payload.sortedBy,
          page: INIT_PAGE_STATE,
          message: null,
        },
      };
    case LOAD_FLEET_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          data: action.payload.data.map((t) => {
            return {
              ...t,
              ...t.attributes,
            };
          }),
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_FLEET_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };
    case LOAD_VESSEL_DETAIL:
      return {
        ...state,
        vessel: {
          ...state.vessel,
          data: state.vessel.data,
          loading: true,
          error: false,
          message: null,
        },
      };
    case LOAD_VESSEL_DETAIL_SUCCESS:
      return {
        ...state,
        vessel: {
          loading: false,
          data: action.payload.data,
          error: null,
          message: 'Success',
        }
      };
    case LOAD_VESSEL_DETAIL_ERROR:
      return {
        ...state,
        vessel: {
          loading: false,
          error: true,
          message: action.payload,
          data: null,
        }
      };
    case LOAD_SURVEY_AUDIT:
      return {
        ...state,
        survey: {
          ...state.survey,
          data: state.survey.data,
          loading: true,
          error: false,
          message: null,
        }
      };
    case LOAD_SURVEY_AUDIT_SUCCESS:
      return {
        ...state,
        survey: {
          data: action.payload.data,
          loading: false,
          error: null,
          message: null,
        }
      };
    case LOAD_SURVEY_AUDIT_ERROR:
      return {
        ...state,
        survey: {
          loading: false,
          data: null,
          error: true,
          message: action.payload,
        }
      };
    default:
      return state;
  }
};
