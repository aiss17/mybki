import { all, call, fork, put, select, takeEvery } from 'redux-saga/effects';
import { GET, RICOGET } from '../../helpers/apiHelper';
import * as action from './action';

export const getListState = (state) => state.fleetsApp.list;

const loadFleetsAsync = async (query) => GET(`fleet-access?${query}`);

const loadVesselAsync = async (code) => {
  const req = await RICOGET(`api/vessel/${code}`);
  return req;
}

const loadSurveyAuditAsync = async (code) => {
  const req = await RICOGET(`api/survey-audits/${code}/scheduled`);
  return req;
}

function* loadFleets({ payload }) {
  try {
    const list = yield select(getListState);

    const param = [
      {
        name: 'page',
        value: payload.selectedPage,
      },
      {
        name: 'orderBy',
        value: list.orderBy,
      },
      {
        name: 'sortedBy',
        value: list.sortedBy,
      },
      {
        name: 'limit',
        value: 0,
      },
    ];
    const response = yield call(
      loadFleetsAsync,
      param.map((t) => `${t.name}=${t.value}`).join('&')
    );

    yield put(action.loadFleetsSuccess(response));
  } catch (error) {
    yield put(action.loadFleetsError(error));
  }
}

export function* loadVessel({ payload }) {
  try {
    const response = yield call(loadVesselAsync, payload);
    yield put(action.loadVesselSuccess(response));
  } catch (error) {
    yield put(action.loadVesselError(error));
  }
}

export function* loadSurveyAudit({ payload }) {
  try {
    const response = yield call(loadSurveyAuditAsync, payload);
    yield put(action.loadSurveyAuditSuccess(response));
  } catch (error) {
    yield put(action.loadSurveyAuditError(error));
  }
}

export function* watchLoadFleets() {
  yield takeEvery(action.LOAD_FLEET, loadFleets);
}
export function* watchLoadVessel() {
  yield takeEvery(action.LOAD_VESSEL_DETAIL, loadVessel);
}

export function* watchLoadSurveyAudit() {
  yield takeEvery(action.LOAD_SURVEY_AUDIT, loadSurveyAudit);
}

export default function* rootSaga() {
  yield all([
    fork(watchLoadFleets),
    fork(watchLoadVessel),
    fork(watchLoadSurveyAudit),
  ]);
}
