import { combineReducers } from 'redux';
import authApp from './auth/reducer';
import sampleApp from './sample/reducer';
import trackingApp from './tracking/reducer';
import orderApp from './order/reducer';
import servicesApp from './services/reducer';
import quotationApp from './quotation/reducer';
import fleetsApp from './fleet/reducer';
import invoiceApp from './invoice/reducer';
import paymentApp from './payment/reducer';
import satisfactionApp from './satisfaction/reducer';
import companyApp from './company/reducer';
import certificateApp from './certificate/reducer';

const reducers = combineReducers({
  sampleApp,
  authApp,
  trackingApp,
  orderApp,
  servicesApp,
  quotationApp,
  fleetsApp,
  invoiceApp,
  paymentApp,
  satisfactionApp,
  companyApp,
  certificateApp,
});

export default reducers;
