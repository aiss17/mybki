import {
  LOAD_OAUTH_CONFIG,
  LOAD_OAUTH_CONFIG_ERROR,
  LOAD_OAUTH_CONFIG_SUCCESS,
} from './actions';

const INIT_STATE = {
  context: {
    data: {},
    loading: false,
    error: true,
    isLoaded: false,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_OAUTH_CONFIG:
      return {
        ...state,
        context: {
          ...state.context,
          data: null,
          loading: true,
          error: false,
          isLoaded: false,
        },
      };
    case LOAD_OAUTH_CONFIG_SUCCESS:
      return {
        ...state,
        context: {
          ...state.context,
          data: {
            ...(action.payload || {}),
          },
          loading: false,
          error: false,
          isLoaded: true,
        },
      };
    case LOAD_OAUTH_CONFIG_ERROR:
      return {
        ...state,
        context: {
          ...state.context,
          data: null,
          loading: false,
          error: true,
          isLoaded: false,
        },
      };

    default:
      return { ...state };
  }
};
