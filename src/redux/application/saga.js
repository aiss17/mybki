import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { GET } from '../../helpers/apiHelper';
import * as action from './actions';

export const loadOauthConfigAsync = async () => GET(`/api/get-oauth`);

function* loadOauthConfig({ payload }) {
  try {
    let response = yield call(loadOauthConfigAsync);

    if (response.success) {
      yield put(action.loadOauthConfigSuccess(response));
    } else {
      yield put(action.loadOauthConfigError(response.message));
    }
  } catch (error) {
    yield put(action.loadOauthConfigError(`${error}`));
  }
}

export function* watchLoadOauthConfig() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(action.LOAD_OAUTH_CONFIG, loadOauthConfig);
}

export default function* rootSaga() {
  yield all([fork(watchLoadOauthConfig)]);
}
