export const LOAD_OAUTH_CONFIG = '[APPLICATION] LOAD_OAUTH_CONFIG';
export const LOAD_OAUTH_CONFIG_SUCCESS = '[ARTIKEL] LOAD_OAUTH_CONFIG_SUCCESS';
export const LOAD_OAUTH_CONFIG_ERROR = '[ARTIKEL] LOAD_OAUTH_CONFIG_ERROR';

export const loadOauthConfig = () => ({
  type: LOAD_OAUTH_CONFIG,
});

export const loadOauthConfigSuccess = (data) => ({
  type: LOAD_OAUTH_CONFIG_SUCCESS,
  payload: data,
});

export const loadOauthConfigError = (message) => ({
  type: LOAD_OAUTH_CONFIG_ERROR,
  payload: { message },
});
