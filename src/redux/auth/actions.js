export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR';
export const LOGOUT_USER = 'LOGOUT_USER';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_ERROR = 'FORGOT_PASSWORD_ERROR';
export const LOAD_CURRENT_USER = 'LOAD_CURRENT_USER';
export const LOAD_CURRENT_USER_SUCCESS = 'LOAD_CURRENT_USER_SUCCESS';
export const LOAD_CURRENT_USER_ERROR = 'LOAD_CURRENT_USER_ERROR';
export const SET_GLOBAL_NOTIFICATION_CHANNEL =
  'SET_GLOBAL_NOTIFICATION_CHANNEL';
export const REFRESH_TOKEN = 'REFRESH_TOKEN';

export const loginUser = (user, navigation) => ({
  type: LOGIN_USER,
  payload: { user, navigation },
});
export const loginUserSuccess = (token) => ({
  type: LOGIN_USER_SUCCESS,
  payload: token,
});
export const loginUserError = (message) => ({
  type: LOGIN_USER_ERROR,
  payload: { message },
});

export const loadCurrentUser = (navigation) => ({
  type: LOAD_CURRENT_USER,
  payload: { navigation },
});
export const loadCurrentUserSuccess = (user) => ({
  type: LOAD_CURRENT_USER_SUCCESS,
  payload: user,
});
export const loadCurrentUserError = (message) => ({
  type: LOAD_CURRENT_USER_ERROR,
  payload: { message },
});

export const forgotPassword = (forgotUserMail, navigation) => ({
  type: FORGOT_PASSWORD,
  payload: { forgotUserMail, navigation },
});
export const forgotPasswordSuccess = (forgotUserMail) => ({
  type: FORGOT_PASSWORD_SUCCESS,
  payload: forgotUserMail,
});
export const forgotPasswordError = (message) => ({
  type: FORGOT_PASSWORD_ERROR,
  payload: { message },
});

export const logoutUser = (navigation) => ({
  type: LOGOUT_USER,
  payload: { navigation },
});

export const setGlobalNotificationChannel = (channel) => ({
  type: SET_GLOBAL_NOTIFICATION_CHANNEL,
  // eslint-disable-next-line object-shorthand
  payload: { channel: channel },
});

export const refreshToken = (token) => ({
  type: REFRESH_TOKEN,
  payload: token,
});
