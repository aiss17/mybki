const Token = {
  token_type: 'Bearer',
  expires_in: 60000,
  access_token: '',
  refresh_token: '',
};

export default Token;
