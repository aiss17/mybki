const User = {
  data: {
    type: 'users',
    id: '7',
    attributes: {
      first_name: 'Admin',
      is_internal: true,
      company_id: null,
      last_name: 'Admin',
      email: 'admin@qtasnim.com',
    },
  },
  meta: {
    include: ['roles', 'permissions'],
  },
};

export default User;
