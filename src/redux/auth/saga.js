import {
  REACT_APP_OAUTH_CLIENT_ID,
  REACT_APP_OAUTH_CLIENT_SECRET,
  REACT_APP_OAUTH_GRANT_TYPE,
} from '@env';
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { UserRole } from '../../constants/defaultValues';
import { GET, simplePOST } from '../../helpers/apiHelper';
import Constants from '../../helpers/Constants';
import { setCurrentToken, setCurrentUser } from '../../helpers/Utils';
import {
  loadCurrentUser,
  loadCurrentUserError,
  loadCurrentUserSuccess,
  LOAD_CURRENT_USER,
  loginUserError,
  loginUserSuccess,
  LOGIN_USER,
  LOGOUT_USER,
} from './actions';

// eslint-disable-next-line no-unused-vars
const loginWithEmailPasswordAsync = async (email, password) =>
  // simplePOST(
  //   `/oauth/token`,
  //   JSON.stringify({
  //     client_id: REACT_APP_OAUTH_CLIENT_ID,
  //     client_secret: REACT_APP_OAUTH_CLIENT_SECRET,
  //     grant_type: REACT_APP_OAUTH_GRANT_TYPE,
  //     password,
  //     username: email,
  //   })
  // );
  simplePOST(
    `/login`,
    JSON.stringify({
      sandi: password,
      email,
    })
  );
const loadUserAsync = async () => GET('/profile');
const logoutAsync = async (navigation) => {
  await setCurrentToken();
  await setCurrentUser();
  navigation.navigate(Constants.MENU_ROOT);
};

function* loginWithEmailPassword({ payload }) {
  const { email, password } = payload.user;
  const { navigation } = payload;
  try {
    const token = yield call(loginWithEmailPasswordAsync, email, password);
    if (!token.message) {
      setCurrentToken(token);
      yield put(loginUserSuccess(token));
      yield all([
        put(loginUserSuccess(token)),
        put(loadCurrentUser(navigation)),
      ]);
    } else if (token.message) {
      yield put(loginUserError(token.message));
    } else {
      yield put(loginUserError('unknown error'));
    }
  } catch (error) {
    yield put(loginUserError(`${error}`));
  }
}

function* loadUser({ payload }) {
  const { navigation } = payload;
  try {
    const response = yield call(loadUserAsync);
    if (!response.message && response.data) {
      const item = {
        id: response.data.id,
        role: UserRole.Admin,
        ...response.data.attributes,
      };
      setCurrentUser(item);
      yield put(loadCurrentUserSuccess(item));
      navigation.navigate(Constants.MENU_ROOT);
    } else if (response.message) {
      yield put(loadCurrentUserError(response.message));
    } else {
      yield put(loadCurrentUserError('unknown error'));
    }
  } catch (error) {
    yield put(loadCurrentUserError(`${error}`));
  }
}

function* logout({ payload }) {
  const { navigation } = payload;
  yield call(logoutAsync, navigation);
}

export function* watchLoginUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}

export function* watchLoadUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOAD_CURRENT_USER, loadUser);
}

export function* watchLogoutUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGOUT_USER, logout);
}

export default function* rootSaga() {
  yield all([fork(watchLoginUser), fork(watchLogoutUser), fork(watchLoadUser)]);
}
