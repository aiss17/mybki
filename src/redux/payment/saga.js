// @ts-nocheck
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { CRMGET, CRMPOST3, POST } from '../../helpers/apiHelper';
import * as action from './action';

export const getListState = (state) => state.paymentApp.list;

const loadPaymentsAsync = async (company, query) => {
  const req = await CRMGET(`api/getCompanyNewpayment/id/${company}/${query}`);
  return req;
};

const detailAsync = async (id) => {
  const req = await CRMGET(`api/getNewPayment/id/${id}`);
  return req;
};

const paymentAsync = async (data) => {
  const req = await CRMPOST3(`api/proofPayment`, data);
  return req;
};

const invoiceAsync = async (alias) => CRMGET(`api/getProforma/client/${alias}`);

const addAsync = async (data) => {
  const req = await POST(`/payments/store`, data);
  return req;
};

const duitkuAsync = async (total) => CRMGET(`api/listDuitku/${total}`);

function* loadPayments({ payload }) {
  try {
    const response = yield call(
      loadPaymentsAsync,
      payload.company,
      payload.selectedPage
    );
    yield put(action.loadPaymentsSuccess(response));
  } catch (error) {
    yield put(action.loadPaymentsError(error));
  }
}

function* loadPaymentsDetail({ payload }) {
  try {
    const response = yield call(detailAsync, payload.id);
    yield put(action.loadPaymentsDetailSuccess(response));
  } catch (error) {
    yield put(action.loadPaymentsDetailError(error));
  }
}

function* savePaymentsDetail({ payload }) {
  try {
    const response = yield call(paymentAsync, payload);
    yield put(action.savePaymentsDetailSuccess(response));
  } catch (error) {
    yield put(action.savePaymentsDetailError(error));
  }
}

function* loadPaymentsInvoice({ payload }) {
  try {
    const response = yield call(invoiceAsync, payload);
    yield put(action.loadPaymentsInvoiceSuccess(response));
  } catch (error) {
    yield put(action.loadPaymentsInvoiceError(error));
  }
}

function* savePayments({ payload }) {
  try {
    const response = yield call(addAsync, payload);
    yield put(action.savePaymentsSuccess(response));
  } catch (error) {
    yield put(action.savePaymentsError(error));
  }
}

function* loadDuitkuMethods({ payload }) {
  try {
    const response = yield call(duitkuAsync, payload);
    yield put(action.loadDuitkuMethodsSuccess(response));
  } catch (error) {
    yield put(action.loadDuitkuMethodsError(error));
  }
}

export function* watchLoadPayments() {
  yield takeEvery(action.LOAD_PAYMENTS, loadPayments);
}

export function* watchLoadPaymentsDetail() {
  yield takeEvery(action.LOAD_PAYMENTS_DETAIL, loadPaymentsDetail);
}

export function* watchSavePaymentsDetail() {
  yield takeEvery(action.SAVE_PAYMENTS_DETAIL, savePaymentsDetail);
}

export function* watchLoadPaymentsInvoice() {
  yield takeEvery(action.LOAD_PAYMENTS_INVOICE, loadPaymentsInvoice);
}

export function* watchSavePayments() {
  yield takeEvery(action.SAVE_PAYMENTS, savePayments);
}

export function* watchLoadDuitkuMethods() {
  yield takeEvery(action.LOAD_DUITKU_METHODS, loadDuitkuMethods);
}

export default function* rootSaga() {
  yield all([
    fork(watchLoadPayments),
    fork(watchLoadPaymentsDetail),
    fork(watchSavePaymentsDetail),
    fork(watchLoadPaymentsInvoice),
    fork(watchSavePayments),
    fork(watchLoadDuitkuMethods),
  ]);
}
