import { INIT_PAGE_STATE } from '../../constants/constants';

export const LOAD_PAYMENTS = 'LOAD_PAYMENTS';
export const LOAD_PAYMENTS_SUCCESS = 'LOAD_PAYMENTS_SUCCESS';
export const LOAD_PAYMENTS_ERROR = 'LOAD_PAYMENTS_ERROR';

export const LOAD_PAYMENTS_DETAIL = 'LOAD_PAYMENTS_DETAIL';
export const LOAD_PAYMENTS_DETAIL_SUCCESS = 'LOAD_PAYMENTS_DETAIL_SUCCESS';
export const LOAD_PAYMENTS_DETAIL_ERROR = 'LOAD_PAYMENTS_DETAIL_ERROR';

export const SAVE_PAYMENTS = 'SAVE_PAYMENTS';
export const SAVE_PAYMENTS_SUCCESS = 'SAVE_PAYMENTS_SUCCESS';
export const SAVE_PAYMENTS_ERROR = 'SAVE_PAYMENTS_ERROR';

export const SAVE_PAYMENTS_DETAIL = 'SAVE_PAYMENTS_DETAIL';
export const SAVE_PAYMENTS_DETAIL_SUCCESS = 'SAVE_PAYMENTS_DETAIL_SUCCESS';
export const SAVE_PAYMENTS_DETAIL_ERROR = 'SAVE_PAYMENTS_DETAIL_ERROR';

export const LOAD_PAYMENTS_INVOICE = 'LOAD_PAYMENTS_INVOICE';
export const LOAD_PAYMENTS_INVOICE_SUCCESS = 'LOAD_PAYMENTS_INVOICE_SUCCESS';
export const LOAD_PAYMENTS_INVOICE_ERROR = 'LOAD_PAYMENTS_INVOICE_ERROR';

export const LOAD_DUITKU_METHODS = 'LOAD_DUITKU_METHODS';
export const LOAD_DUITKU_METHODS_SUCCESS = 'LOAD_DUITKU_METHODS_SUCCESS';
export const LOAD_DUITKU_METHODS_ERROR = 'LOAD_DUITKU_METHODS_ERROR';

export const SELECTED_PAYMENT = 'SELECTED_PAYMENT';

export const loadPayments = (p, number) => {
  const param = {
    ...{
      selectedPage: 1,
      filter: null,
      orderBy: 'id',
      sortedBy: 'asc',
      limit: INIT_PAGE_STATE.pageSizes[0],
    },
    ...p,
  };
  return {
    type: LOAD_PAYMENTS,
    payload: {
      selectedPage: number,
      filter: param.filter,
      orderBy: param.orderBy,
      sortedBy: param.sortedBy,
      limit: param.limit,
      company: p,
    },
  };
};

export const loadPaymentsSuccess = (data) => ({
  type: LOAD_PAYMENTS_SUCCESS,
  payload: data,
});

export const loadPaymentsError = (error) => ({
  type: LOAD_PAYMENTS_ERROR,
  payload: error,
});

export const loadPaymentsDetail = (id) => ({
  type: LOAD_PAYMENTS_DETAIL,
  payload: { id },
});

export const loadPaymentsDetailSuccess = (data) => ({
  type: LOAD_PAYMENTS_DETAIL_SUCCESS,
  payload: data,
});

export const loadPaymentsDetailError = (error) => ({
  type: LOAD_PAYMENTS_DETAIL_ERROR,
  payload: error,
});

export const selectedPayment = (data) => {
  return {
    type: SELECTED_PAYMENT,
    payload: data,
  };
};

export const savePaymentsDetail = (data) => {
  return {
    type: SAVE_PAYMENTS_DETAIL,
    payload: data,
  };
};

export const savePaymentsDetailSuccess = (data) => {
  return {
    type: SAVE_PAYMENTS_DETAIL_SUCCESS,
    payload: data,
  };
};

export const savePaymentsDetailError = (error) => {
  return {
    type: SAVE_PAYMENTS_DETAIL_ERROR,
    payload: error,
  };
};

export const loadPaymentsInvoice = (alias) => ({
  type: LOAD_PAYMENTS_INVOICE,
  payload: alias,
});

export const loadPaymentsInvoiceSuccess = (data) => ({
  type: LOAD_PAYMENTS_INVOICE_SUCCESS,
  payload: data,
});

export const loadPaymentsInvoiceError = (error) => ({
  type: LOAD_PAYMENTS_INVOICE_ERROR,
  payload: error,
});

export const savePayments = (item) => ({
  type: SAVE_PAYMENTS,
  payload: item,
});

export const savePaymentsSuccess = (data) => ({
  type: SAVE_PAYMENTS_SUCCESS,
  payload: data,
});

export const savePaymentsError = (error) => ({
  type: SAVE_PAYMENTS_ERROR,
  payload: error,
});

export const loadDuitkuMethods = (total) => ({
  type: LOAD_DUITKU_METHODS,
  payload: total,
});

export const loadDuitkuMethodsSuccess = (data) => ({
  type: LOAD_DUITKU_METHODS_SUCCESS,
  payload: data,
});

export const loadDuitkuMethodsError = (error) => ({
  type: LOAD_DUITKU_METHODS_ERROR,
  payload: error,
});
