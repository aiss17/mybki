/* eslint-disable no-else-return */
/* eslint-disable eqeqeq */
import { INIT_PAGE_STATE } from '../../constants/constants';

import {
  LOAD_PAYMENTS,
  LOAD_PAYMENTS_ERROR,
  LOAD_PAYMENTS_SUCCESS,
  LOAD_PAYMENTS_DETAIL,
  LOAD_PAYMENTS_DETAIL_ERROR,
  LOAD_PAYMENTS_DETAIL_SUCCESS,
  SAVE_PAYMENTS_DETAIL,
  SAVE_PAYMENTS_DETAIL_ERROR,
  SAVE_PAYMENTS_DETAIL_SUCCESS,
  SAVE_PAYMENTS,
  SAVE_PAYMENTS_ERROR,
  SAVE_PAYMENTS_SUCCESS,
  LOAD_PAYMENTS_INVOICE,
  LOAD_PAYMENTS_INVOICE_ERROR,
  LOAD_PAYMENTS_INVOICE_SUCCESS,
  LOAD_DUITKU_METHODS,
  LOAD_DUITKU_METHODS_ERROR,
  LOAD_DUITKU_METHODS_SUCCESS,
} from './action';

const INIT_STATE = {
  list: {
    data: null,
    loading: false,
    error: false,
    filter: null,
    orderBy: null,
    sortedBy: 'asc',
    filterFields: 'name:ilike;id:ilike',
    searchJoin: 'or',
    page: INIT_PAGE_STATE,
    message: null,
    isListEnd: false,
  },
  detail: {
    data: null,
    loading: false,
    error: false,
    message: null,
    save: null,
  },
  invoice: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  add: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  duitku: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_PAYMENTS_DETAIL:
      return {
        ...state,
        detail: {
          ...state.detail,
          data: null,
          loading: true,
          error: false,
          message: null,
          save: null,
        },
      };
    case LOAD_PAYMENTS_DETAIL_SUCCESS:
      return {
        ...state,
        detail: {
          ...state.detail,
          data: action.payload.response,
          loading: false,
          error: false,
          message: 'Success',
          save: null,
        },
      };
    case LOAD_PAYMENTS_DETAIL_ERROR:
      return {
        ...state,
        detail: {
          ...state.detail,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
          save: null,
        },
      };

    case LOAD_PAYMENTS:
      return {
        ...state,
        list: {
          ...state.list,
          data: action.payload.selectedPage == 1 ? null : state.list.data,
          loading: true,
          error: false,
          filter: action.payload.filter,
          orderBy: action.payload.orderBy,
          sortedBy: action.payload.sortedBy,
          page: INIT_PAGE_STATE,
          message: null,
        },
      };
    case LOAD_PAYMENTS_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          data:
            action.payload.response.pagination.current_page > 1
              ? [
                  ...state.list.data,
                  ...action.payload.response.payment.map((t) => {
                    return {
                      ...t,
                      ...t.attributes,
                    };
                  }),
                ]
              : action.payload.response.payment.map((t) => {
                  return {
                    ...t,
                    ...t.attributes,
                  };
                }),
          page:
            action.payload.response && action.payload.response.pagination
              ? {
                  ...state.list.page,
                  currentPage: action.payload.response.pagination.current_page,
                  selectedPageSize: action.payload.response.pagination.per_page,
                  totalItemCount: action.payload.response.pagination.total_rows,
                  totalPage: action.payload.response.pagination.total_page,
                  itemCount: action.payload.response.pagination.total_rows,
                }
              : state.list.page,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_PAYMENTS_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: false,
          error: true,
          page: INIT_PAGE_STATE,
          message: action.payload,
        },
      };

    case SAVE_PAYMENTS_DETAIL:
      return {
        ...state,
        detail: {
          ...state.detail,
          data: null,
          loading: true,
          error: false,
          message: 'Detail',
          save: null,
        },
      };
    case SAVE_PAYMENTS_DETAIL_SUCCESS:
      return {
        ...state,
        detail: {
          ...state.detail,
          data: null,
          loading: false,
          error: false,
          save: 'Success',
          message: 'Detail',
        },
      };
    case SAVE_PAYMENTS_DETAIL_ERROR:
      return {
        ...state,
        detail: {
          ...state.detail,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
          save: null,
        },
      };

    case LOAD_PAYMENTS_INVOICE:
      return {
        ...state,
        invoice: {
          ...state.invoice,
          data: null,
          loading: true,
          error: false,
          message: null,
        },
      };
    case LOAD_PAYMENTS_INVOICE_SUCCESS:
      return {
        ...state,
        invoice: {
          ...state.invoice,
          data: action.payload.response || [],
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_PAYMENTS_INVOICE_ERROR:
      return {
        ...state,
        invoice: {
          ...state.invoice,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    case SAVE_PAYMENTS:
      return {
        ...state,
        add: {
          ...state.add,
          data: null,
          loading: true,
          error: false,
          message: null,
        },
      };
    case SAVE_PAYMENTS_SUCCESS:
      return {
        ...state,
        add: {
          ...state.add,
          data: action.payload.data,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case SAVE_PAYMENTS_ERROR:
      return {
        ...state,
        add: {
          ...state.add,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    case LOAD_DUITKU_METHODS:
      return {
        ...state,
        duitku: {
          ...state.duitku,
          data: null,
          loading: true,
          error: false,
          message: null,
        },
      };
    case LOAD_DUITKU_METHODS_SUCCESS:
      return {
        ...state,
        duitku: {
          ...state.duitku,
          data: action.payload?.response?.paymentFee ?? [],
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_DUITKU_METHODS_ERROR:
      return {
        ...state,
        duitku: {
          ...state.duitku,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    default:
      return state;
  }
};
