// @ts-nocheck
import { all, call, fork, put, select, takeEvery } from 'redux-saga/effects';
import { GET, CRMGET } from '../../helpers/apiHelper';
import * as action from './action';

export const getListState = (state) => state.quotationApp.list;

const loadQuotationsAsync = async (query) => GET(`quotation?${query}`);
const loadQuotationsDetailAsync = async ({ type, rfqNo }) =>
  CRMGET(`api/getDetail/type/${type}/noquotation/${rfqNo}`);

function* loadQuotations({ payload }) {
  try {
    const list = yield select(getListState);

    let param = [
      {
        name: 'page',
        value: payload.selectedPage,
      },
      {
        name: 'orderBy',
        value: list.orderBy,
      },
      {
        name: 'sortedBy',
        value: list.sortedBy,
      },
      {
        name: 'limit',
        value: payload.limit,
      },
    ];

    if (list.filter || '') {
      param = [
        ...param,
        ...[
          {
            name: 'search',
            value: list.filter,
          },
          {
            name: 'searchFields',
            value: list.filterFields,
          },
          {
            name: 'searchJoin',
            value: list.searchJoin,
          },
        ],
      ];
    }

    const response = yield call(
      loadQuotationsAsync,
      param.map((t) => `${t.name}=${t.value}`).join('&')
    );

    yield put(action.loadQuotationsSuccess(response));
  } catch (error) {
    yield put(action.loadQuotationsError(error));
  }
}

function* loadQuotationsDetail({ payload }) {
  try {
    const response = yield call(loadQuotationsDetailAsync, {
      type: 'proposal',
      rfqNo: payload.id,
    });
    yield put(action.loadQuotationsDetailSuccess(response));
  } catch (error) {
    yield put(action.loadQuotationsDetailError(error));
  }
}

export function* watchLoadQuotationsDetail() {
  yield takeEvery(action.LOAD_QUOTATION_DETAIL, loadQuotationsDetail);
}

export function* watchLoadQuotations() {
  yield takeEvery(action.LOAD_QUOTATION, loadQuotations);
}

export default function* rootSaga() {
  yield all([fork(watchLoadQuotations), fork(watchLoadQuotationsDetail)]);
}
