import { INIT_PAGE_STATE } from '../../constants/constants';

export const LOAD_QUOTATION = 'LOAD_QUOTATION';
export const LOAD_QUOTATION_SUCCESS = 'LOAD_QUOTATION_SUCCESS';
export const LOAD_QUOTATION_ERROR = 'LOAD_QUOTATION_ERROR';

export const LOAD_QUOTATION_DETAIL = 'LOAD_QUOTATION_DETAIL';
export const LOAD_QUOTATION_DETAIL_SUCCESS = 'LOAD_QUOTATION_DETAIL_SUCCESS';
export const LOAD_QUOTATION_DETAIL_ERROR = 'LOAD_QUOTATION_DETAIL_ERROR';

export const SELECTED_QUOTATION = 'SELECTED_QUOTATION';

export const loadQuotations = (p) => {
  const param = {
    ...{
      selectedPage: 1,
      filter: null,
      orderBy: 'id',
      sortedBy: 'asc',
      limit: INIT_PAGE_STATE.pageSizes[0],
    },
    ...p,
  };
  return {
    type: LOAD_QUOTATION,
    payload: {
      selectedPage: param.selectedPage,
      filter: param.filter,
      orderBy: param.orderBy,
      sortedBy: param.sortedBy,
      limit: param.limit,
    },
  };
};

export const loadQuotationsSuccess = (data) => ({
  type: LOAD_QUOTATION_SUCCESS,
  payload: data,
});

export const loadQuotationsError = (error) => ({
  type: LOAD_QUOTATION_ERROR,
  payload: error,
});

export const loadQuotationsDetail = (p) => {
  const param = {
    ...{
      selectedPage: 1,
      filter: null,
      orderBy: 'id',
      sortedBy: 'asc',
      limit: INIT_PAGE_STATE.pageSizes[0],
      id: null,
      isQuot: false,
    },
    ...p,
  };
  return {
    type: LOAD_QUOTATION_DETAIL,
    payload: {
      selectedPage: param.selectedPage,
      filter: param.filter,
      orderBy: param.orderBy,
      sortedBy: param.sortedBy,
      limit: param.limit,
      id: param.id,
      isQuot: param.isQuot,
    },
  };
};

export const loadQuotationsDetailSuccess = (data) => ({
  type: LOAD_QUOTATION_DETAIL_SUCCESS,
  payload: data,
});

export const loadQuotationsDetailError = (error) => ({
  type: LOAD_QUOTATION_DETAIL_ERROR,
  payload: error,
});

export const selectedQuotation = (data) => {
  return {
    type: SELECTED_QUOTATION,
    payload: data,
  };
};
