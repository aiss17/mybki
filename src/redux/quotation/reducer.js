/* eslint-disable no-else-return */
/* eslint-disable eqeqeq */
import { INIT_PAGE_STATE } from '../../constants/constants';

import {
  LOAD_QUOTATION,
  LOAD_QUOTATION_ERROR,
  LOAD_QUOTATION_SUCCESS,
  LOAD_QUOTATION_DETAIL,
  LOAD_QUOTATION_DETAIL_ERROR,
  LOAD_QUOTATION_DETAIL_SUCCESS,
  SELECTED_QUOTATION,
} from './action';

const INIT_STATE = {
  list: {
    data: null,
    loading: false,
    error: false,
    filter: null,
    orderBy: null,
    sortedBy: 'asc',
    filterFields: 'name:ilike;id:ilike',
    searchJoin: 'or',
    page: INIT_PAGE_STATE,
    message: null,
    isListEnd: false,
  },

  listDetail: {
    data: null,
    loading: false,
    error: false,
    filter: null,
    orderBy: null,
    sortedBy: 'asc',
    filterFields: 'name:ilike;id:ilike',
    searchJoin: 'or',
    message: null,
    isListEnd: false,
  },

  selectedQuotation: {
    data: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_QUOTATION:
      return {
        ...state,
        list: {
          ...state.list,
          data: action.payload.selectedPage == 1 ? null : state.list.data,
          loading: true,
          error: false,
          filter: action.payload.filter,
          orderBy: action.payload.orderBy,
          sortedBy: action.payload.sortedBy,
          page: INIT_PAGE_STATE,
          message: null,
        },
      };

    case LOAD_QUOTATION_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          data:
            action.payload.meta.pagination.current_page > 1
              ? [
                  ...state.list.data,
                  ...action.payload.data.map((t) => {
                    return {
                      ...t,
                      ...t.attributes,
                    };
                  }),
                ]
              : action.payload.data.map((t) => {
                  return {
                    ...t,
                    ...t.attributes,
                  };
                }),
          page:
            action.payload.meta && action.payload.meta.pagination
              ? {
                  ...state.list.page,
                  currentPage: action.payload.meta.pagination.current_page,
                  selectedPageSize: action.payload.meta.pagination.per_page,
                  totalItemCount: action.payload.meta.pagination.total,
                  totalPage: action.payload.meta.pagination.total_pages,
                  itemCount: action.payload.meta.pagination.count,
                }
              : state.list.page,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_QUOTATION_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: false,
          error: true,
          page: INIT_PAGE_STATE,
          message: action.payload,
        },
      };

    case LOAD_QUOTATION_DETAIL:
      return {
        ...state,
        listDetail: {
          ...state.listDetail,
          data: action.payload.selectedPage == 1 ? null : state.listDetail.data,
          loading: true,
          error: false,
          filter: action.payload.filter,
          orderBy: action.payload.orderBy,
          sortedBy: action.payload.sortedBy,
          page: INIT_PAGE_STATE,
          message: null,
        },
      };

    case LOAD_QUOTATION_DETAIL_SUCCESS:
      return {
        ...state,
        listDetail: {
          ...state.listDetail,
          data: action.payload.response,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_QUOTATION_DETAIL_ERROR:
      return {
        ...state,
        listDetail: {
          ...state.listDetail,
          data: null,
          loading: false,
          error: true,
          page: INIT_PAGE_STATE,
          message: action.payload,
        },
      };

    case SELECTED_QUOTATION:
      return {
        ...state,
        selectedQuotation: { ...state.selectedQuotation, data: action.payload.attributes },
      };

    default:
      return state;
  }
};
