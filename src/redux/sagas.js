import { all } from 'redux-saga/effects';
import applicationSaga from './application/saga';
import authSagas from './auth/saga';
import sampleSagas from './sample/saga';
import trackingSaga from './tracking/saga';
import orderSaga from './order/saga';
import servicesSaga from './services/saga';
import quotationSaga from './quotation/saga';
import invoicesSaga from './invoice/saga';
import paymentSaga from './payment/saga';
import fleetSaga from './fleet/saga';
import satisfactionSaga from './satisfaction/saga';
import companySaga from './company/saga';
import certificateSaga from './certificate/saga';

export default function* rootSaga() {
  yield all([
    sampleSagas(),
    authSagas(),
    trackingSaga(),
    applicationSaga(),
    orderSaga(),
    servicesSaga(),
    quotationSaga(),
    invoicesSaga(),
    paymentSaga(),
    fleetSaga(),
    satisfactionSaga(),
    companySaga(),
    certificateSaga(),
  ]);
}
