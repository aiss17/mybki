// @ts-nocheck
import { all, call, fork, put, select, takeEvery } from 'redux-saga/effects';
import { CRMGET } from '../../helpers/apiHelper';
import * as action from './action';

export const getListState = (state) => state.invoiceApp.list;

const loadInvoicesAsync = async (query) =>
  CRMGET(`api/getMybill/client/${query}`);

function* loadInvoices({ payload }) {
  try {
    const list = yield select(getListState);

    let param = [
      {
        name: 'page',
        value: payload.selectedPage,
      },
      {
        name: 'orderBy',
        value: list.orderBy,
      },
      {
        name: 'sortedBy',
        value: list.sortedBy,
      },
      {
        name: 'limit',
        value: payload.limit,
      },
    ];

    if (list.filter || '') {
      param = [
        ...param,
        ...[
          {
            name: 'search',
            value: list.filter,
          },
          {
            name: 'searchFields',
            value: list.filterFields,
          },
          {
            name: 'searchJoin',
            value: list.searchJoin,
          },
        ],
      ];
    }
    console.log('param', param);
    const response = yield call(loadInvoicesAsync, payload.company);
    yield put(action.loadInvoicesSuccess(response));
  } catch (error) {
    yield put(action.loadInvoicesError(error));
  }
}

export function* watchLoadInvoices() {
  yield takeEvery(action.LOAD_INVOICES, loadInvoices);
}

export default function* rootSaga() {
  yield all([fork(watchLoadInvoices)]);
}
