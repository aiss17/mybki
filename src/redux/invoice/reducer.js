/* eslint-disable no-else-return */
/* eslint-disable eqeqeq */
import { INIT_PAGE_STATE } from '../../constants/constants';

import {
  LOAD_INVOICES,
  LOAD_INVOICES_ERROR,
  LOAD_INVOICES_SUCCESS,
} from './action';

const INIT_STATE = {
  list: {
    data: null,
    loading: false,
    error: false,
    filter: null,
    orderBy: null,
    sortedBy: 'asc',
    filterFields: 'name:ilike;id:ilike',
    searchJoin: 'or',
    page: INIT_PAGE_STATE,
    message: null,
    isListEnd: false,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOAD_INVOICES:
      return {
        ...state,
        list: {
          ...state.list,
          data: action.payload.selectedPage == 1 ? null : state.list.data,
          loading: true,
          error: false,
          filter: action.payload.filter,
          orderBy: action.payload.orderBy,
          sortedBy: action.payload.sortedBy,
          page: INIT_PAGE_STATE,
          message: null,
        },
      };

    case LOAD_INVOICES_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          data: action.payload.response.map((t) => {
            return {
              ...t,
              ...t.attributes,
            };
          }),
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case LOAD_INVOICES_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          data: null,
          loading: false,
          error: true,
          page: INIT_PAGE_STATE,
          message: action.payload,
        },
      };

    default:
      return state;
  }
};
