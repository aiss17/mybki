import { INIT_PAGE_STATE } from '../../constants/constants';

export const LOAD_INVOICES = 'LOAD_INVOICES';
export const LOAD_INVOICES_SUCCESS = 'LOAD_INVOICES_SUCCESS';
export const LOAD_INVOICES_ERROR = 'LOAD_INVOICES_ERROR';

export const loadInvoices = (p) => {
  const param = {
    ...{
      selectedPage: 1,
      filter: null,
      orderBy: 'id',
      sortedBy: 'asc',
      limit: INIT_PAGE_STATE.pageSizes[0],
    },
    ...p,
  };
  return {
    type: LOAD_INVOICES,
    payload: {
      selectedPage: param.selectedPage,
      filter: param.filter,
      orderBy: param.orderBy,
      sortedBy: param.sortedBy,
      limit: param.limit,
      company: p,
    },
  };
};

export const loadInvoicesSuccess = (data) => ({
  type: LOAD_INVOICES_SUCCESS,
  payload: data,
});

export const loadInvoicesError = (error) => ({
  type: LOAD_INVOICES_ERROR,
  payload: error,
});
