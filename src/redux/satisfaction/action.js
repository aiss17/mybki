export const GET_SURVEYID_CRM = 'GET_SURVEYID_CRM';
export const GET_SURVEYID_CRM_SUCCESS = 'GET_SURVEYID_CRM_SUCCESS';
export const GET_SURVEYID_CRM_ERROR = 'GET_SURVEYID_CRM_ERROR';

export const GET_SURVEY_KEPUASAN_CRM = 'GET_SURVEY_KEPUASAN_CRM';
export const GET_SURVEY_KEPUASAN_CRM_SUCCESS =
  'GET_SURVEY_KEPUASAN_CRM_SUCCESS';
export const GET_SURVEY_KEPUASAN_CRM_ERROR = 'GET_SURVEY_KEPUASAN_CRM_ERROR';

export const ADD_NEW_NOTIFICATION = 'ADD_NEW_NOTIFICATION';
export const ADD_NEW_NOTIFICATION_SUCCESS = 'ADD_NEW_NOTIFICATION_SUCCESS';
export const ADD_NEW_NOTIFICATION_ERROR = 'ADD_NEW_NOTIFICATION_ERROR';

export const getSurveyidCrm = (id) => ({
  type: GET_SURVEYID_CRM,
  payload: { id },
});

export const getSurveyidCrmSuccess = (data) => ({
  type: GET_SURVEYID_CRM_SUCCESS,
  payload: data,
});

export const getSurveyidCrmError = (error) => ({
  type: GET_SURVEYID_CRM_ERROR,
  payload: error,
});

export const getSurveyKepuasanCrm = (id) => ({
  type: GET_SURVEY_KEPUASAN_CRM,
  payload: { id },
});

export const getSurveyKepuasanCrmSuccess = (data) => ({
  type: GET_SURVEY_KEPUASAN_CRM_SUCCESS,
  payload: data,
});

export const getSurveyKepuasanCrmError = (error) => ({
  type: GET_SURVEY_KEPUASAN_CRM_ERROR,
  payload: error,
});

export const addNewNotification = (items) => ({
  type: ADD_NEW_NOTIFICATION,
  payload: items,
});

export const addNewNotificationSuccess = (data) => ({
  type: ADD_NEW_NOTIFICATION_SUCCESS,
  payload: data,
});

export const addNewNotificationError = (error) => ({
  type: ADD_NEW_NOTIFICATION_ERROR,
  payload: error,
});
