import {
  GET_SURVEYID_CRM,
  GET_SURVEYID_CRM_ERROR,
  GET_SURVEYID_CRM_SUCCESS,
  GET_SURVEY_KEPUASAN_CRM,
  GET_SURVEY_KEPUASAN_CRM_ERROR,
  GET_SURVEY_KEPUASAN_CRM_SUCCESS,
  ADD_NEW_NOTIFICATION,
  ADD_NEW_NOTIFICATION_ERROR,
  ADD_NEW_NOTIFICATION_SUCCESS,
} from './action';

const INIT_STATE = {
  surveyCrm: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  kepuasan: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
  notif: {
    data: null,
    loading: false,
    error: false,
    message: null,
  },
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_SURVEYID_CRM:
      return {
        ...state,
        surveyCrm: {
          ...state.surveyCrm,
          data: null,
          loading: true,
          error: false,
          message: null,
        },
      };
    case GET_SURVEYID_CRM_SUCCESS:
      return {
        ...state,
        surveyCrm: {
          ...state.surveyCrm,
          data: action.payload.response,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case GET_SURVEYID_CRM_ERROR:
      return {
        ...state,
        surveyCrm: {
          ...state.surveyCrm,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    case GET_SURVEY_KEPUASAN_CRM:
      return {
        ...state,
        kepuasan: {
          ...state.kepuasan,
          data: null,
          loading: true,
          error: false,
          message: null,
        },
      };
    case GET_SURVEY_KEPUASAN_CRM_SUCCESS:
      return {
        ...state,
        kepuasan: {
          ...state.kepuasan,
          data: action.payload.response,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case GET_SURVEY_KEPUASAN_CRM_ERROR:
      return {
        ...state,
        kepuasan: {
          ...state.kepuasan,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    case ADD_NEW_NOTIFICATION:
      return {
        ...state,
        notif: {
          ...state.notif,
          data: null,
          loading: true,
          error: false,
          message: null,
        },
      };

    case ADD_NEW_NOTIFICATION_SUCCESS:
      return {
        ...state,
        notif: {
          ...state.notif,
          data: action.payload,
          loading: false,
          error: false,
          message: 'Success',
        },
      };
    case ADD_NEW_NOTIFICATION_ERROR:
      return {
        ...state,
        notif: {
          ...state.notif,
          data: null,
          loading: false,
          error: true,
          message: action.payload,
        },
      };

    default:
      return state;
  }
};
