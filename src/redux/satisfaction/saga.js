import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { CRMGET, CRMPOST3 } from '../../helpers/apiHelper';
import * as action from './action';

const surveyAsync = async (id) => CRMGET(`api/getSurveynotif/id/${id}`);

const kepuasanAsync = async (id) => CRMGET(`api/getSurveydet/id/${id}`);

const notifAsync = async (data) => CRMPOST3(`/api/postSurvey`, data);

// const closeNotificationCrmAsync = async (surveyId, companyId) => {
//   const req = await CRMPOST3(
//     `api/closeSurvey/surveyid/${surveyId}/clientid/${companyId}`
//   );
//   console.log('req', req);
//   return req;
// };

function* getSurveyidCrm({ payload }) {
  try {
    const response = yield call(surveyAsync, payload.id);
    yield put(action.getSurveyidCrmSuccess(response));
  } catch (error) {
    yield put(action.getSurveyidCrmError(error));
  }
}

function* getSurveyKepuasanCrm({ payload }) {
  try {
    const response = yield call(kepuasanAsync, payload.id);
    yield put(action.getSurveyKepuasanCrmSuccess(response));
  } catch (error) {
    yield put(action.getSurveyKepuasanCrmError(error));
  }
}

function* addNewNotification({ payload }) {
  try {
    let response = null;
    response = yield call(notifAsync, payload);
    // response = yield call(
    //   closeNotificationCrmAsync,
    //   payload.survey?.surveyid,
    //   payload.survey?.clientid
    // );
    yield put(action.addNewNotificationSuccess(response));
  } catch (error) {
    yield put(action.addNewNotificationError(error));
  }
}

export function* watchGetSurveyidCrm() {
  yield takeEvery(action.GET_SURVEYID_CRM, getSurveyidCrm);
}

export function* watchGetSurveyKepuasanCrm() {
  yield takeEvery(action.GET_SURVEY_KEPUASAN_CRM, getSurveyKepuasanCrm);
}

export function* watchAddNewNotification() {
  yield takeEvery(action.ADD_NEW_NOTIFICATION, addNewNotification);
}

export default function* rootSaga() {
  yield all([
    fork(watchGetSurveyidCrm),
    fork(watchGetSurveyKepuasanCrm),
    fork(watchAddNewNotification),
  ]);
}
