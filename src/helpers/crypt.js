import * as Crypto from 'expo-crypto';

const sha1 = async (content) => {
  return Crypto.digestStringAsync(Crypto.CryptoDigestAlgorithm.SHA256, content);
};

export default sha1;
