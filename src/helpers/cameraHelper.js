import * as ImagePicker from 'expo-image-picker';

export const takePicture = async (
  onComplete = (uri) => {},
  type = ImagePicker.MediaTypeOptions.Images
) => {
  // Ask the user for the permission to access the camera
  const permissionResult = await ImagePicker.requestCameraPermissionsAsync();

  if (permissionResult.granted === false) {
    alert("You've refused to allow this appp to access your camera!");
    return;
  }

  const result = await ImagePicker.launchCameraAsync({
    allowsEditing: true,
    aspect: [1, 1],
    quality: 1,
    mediaTypes: type,
  });

  if (!result.cancelled) {
    onComplete(result.uri);
  }
};

export const pickPictureFromGallery = async (
  onComplete = (uri) => {},
  type = ImagePicker.MediaTypeOptions.Images
) => {
  let result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: type,
    allowsEditing: true,
    aspect: [1, 1],
    quality: 1,
  });

  if (!result.cancelled) {
    onComplete(result.uri);
  }
};
