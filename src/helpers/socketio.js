import Echo from 'laravel-echo';
import socketio from 'socket.io-client';
import { REACT_APP_WS_URL } from '@env';

let echovar = null;

if (REACT_APP_WS_URL) {
  echovar = new Echo({
    host: REACT_APP_WS_URL,
    broadcaster: 'socket.io',
    client: socketio,
  });
}

const echo = echovar;

// eslint-disable-next-line import/prefer-default-export
export { echo };
