import * as FileSystem from 'expo-file-system';

export const readAsStringAsync = async (uri, option) => {
  return FileSystem.readAsStringAsync(uri, option);
};

export const deleteAsync = async (uri) => {
  return FileSystem.deleteAsync(uri);
};

export const getInfoAsync = async (uri) => {
  return FileSystem.getInfoAsync(uri);
};

export const moveAsync = async (option) => {
  return FileSystem.moveAsync(option);
};

export const getDocumentDirectory = () => FileSystem.documentDirectory;
