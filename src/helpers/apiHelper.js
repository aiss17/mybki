/* eslint-disable no-unused-expressions */
import {
  REACT_APP_BACKEND,
  REACT_APP_RICO_BASEURL,
  REACT_APP_RICO_ENDPOINT,
  REACT_APP_CRM_BASEURL,
  REACT_APP_CRM_ENDPOINT,
} from '@env';
import axios from 'axios';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { getCurrentToken } from './Utils';

export const errorEntityValidationFormat = (message, model) => {
  return ReactDOMServer.renderToString(
    <>
      {message}
      {model && Object.keys(model).length > 0 && (
        <p>
          <ul>
            {Object.keys(model).map((key) => {
              return <li key={key}>{model[key].find(() => true)}</li>;
            })}
          </ul>
        </p>
      )}
    </>
  );
};

/* eslint-disable import/prefer-default-export */
export const handleApiErrors = (response) => {
  if (!response.ok) {
    if (response.statusText && response.statusText.length > 0) {
      throw Error(response.statusText);
    }
  }

  return response;
};

const handleOkResponse = (response) => {
  if (
    response.headers.get('Content-Type') &&
    response.headers.get('Content-Type').indexOf('application/json') >= 0
  ) {
    return response.json();
  }

  return response.text();
};

export const simplePOST = (
  contextPath,
  body = '{}',
  contentType = 'application/json'
) =>
  fetch(`${REACT_APP_BACKEND}${contextPath}`, {
    method: 'POST',
    headers: { 'Content-Type': contentType },
    body,
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const simpleGET = (contextPath) => {
  fetch(`${REACT_APP_BACKEND}${contextPath}`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => {
      handleApiErrors(response);
    })
    .then((response) => {
      handleOkResponse(response);
    });
};

export const fetchPOST = (contextPath, body = '{}') =>
  fetch(`${REACT_APP_BACKEND}${contextPath}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getCurrentToken().access_token}`,
    },
    body,
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const GET = (url) => {
  return axios({
    url,
    method: 'GET',
  }).then((response) => response.data);
};

export const POST = (url, reqBody, headers = {}, progress = (ev) => {}) =>
  axios({
    url,
    method: 'POST',
    data: reqBody,
    onUploadProgress: progress,
    headers,
  }).then((response) => response.data);

export const POST2 = (url, reqBody) => {
  return axios({
    url,
    method: 'POST',
    data: reqBody,
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => response.data)
    .catch((err) => console.log('error', err));
};

export const PUT = (url, reqBody) =>
  axios({
    baseURL: REACT_APP_BACKEND,
    url,
    method: 'PUT',
    data: reqBody,
  }).then((response) => response.data);

export const DELETE = (url) =>
  axios({
    url,
    method: 'DELETE',
  }).then((response) => response.data);

export const fetchPUT = (contextPath, body = '{}') =>
  fetch(`${REACT_APP_BACKEND}${contextPath}`, {
    method: 'PUT',
    body,
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const fetchDELETE = (contextPath) =>
  fetch(`${REACT_APP_BACKEND}${contextPath}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getCurrentToken().access_token}`,
    },
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const fetchGET = (contextPath) =>
  fetch(`${REACT_APP_BACKEND}${contextPath}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getCurrentToken().access_token}`,
    },
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const RICOGET = (url) =>
  fetch(`${REACT_APP_RICO_BASEURL}${url}`, {
    method: 'GET',
    headers: {
      'x-token': `${REACT_APP_RICO_ENDPOINT}`,
    },
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const RICOPOST = (url, data) =>
  fetch(`${REACT_APP_RICO_BASEURL}${url}`, {
    method: 'POST',
    headers: {
      'x-token': `${REACT_APP_RICO_ENDPOINT}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: new URLSearchParams(data),
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

// =========== CRM ENDPOINT ===========

export const CRMGET = (url) =>
  fetch(`${REACT_APP_CRM_BASEURL}${url}`, {
    method: 'GET',
    headers: {
      'x-token': `${REACT_APP_CRM_ENDPOINT}`,
    },
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const CRMPOST = (url, data) =>
  fetch(`${REACT_APP_CRM_BASEURL}${url}`, {
    method: 'POST',
    headers: {
      'x-token': `${REACT_APP_CRM_ENDPOINT}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: new URLSearchParams(data),
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const CRMPOST2 = (url, data) =>
  fetch(`${REACT_APP_CRM_BASEURL}${url}`, {
    method: 'POST',
    headers: {
      'x-token': `${REACT_APP_CRM_ENDPOINT}`,
    },
    body: data,
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const CRMPOST3 = (url, data) =>
  fetch(`${REACT_APP_CRM_BASEURL}${url}`, {
    method: 'POST',
    headers: {
      'x-token': `${REACT_APP_CRM_ENDPOINT}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const CRMPUT = (url, data) =>
  fetch(`${REACT_APP_CRM_BASEURL}${url}`, {
    method: 'PUT',
    headers: {
      'x-token': `${REACT_APP_CRM_ENDPOINT}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: new URLSearchParams(data),
  })
    .then((response) => handleApiErrors(response))
    .then((response) => handleOkResponse(response));

export const loginWithIdiUser = async (idiUser) => {
  const data = new FormData();
  data.append('email', idiUser.email);
  data.append('npa', idiUser.npa);
  data.append('nama', idiUser.nama);
  data.append('tanggal_daftar', idiUser.tanggal_daftar);
  data.append('wilayah', idiUser.wilayah);
  data.append('cabang', idiUser.cabang);
  return simplePOST('/api/get-user', data, 'multipart/form-data');
};
