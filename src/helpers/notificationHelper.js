import Toast from '@mitesh-v2stech/react-native-toast-message';

const toast = (message, type = 'success') => {
  Toast.show({
    title: 'Info',
    message,
    type,
  });
};

export default toast;
