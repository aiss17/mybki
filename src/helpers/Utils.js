import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';

export const getFormData = (object) => {
  const formData = new FormData();
  Object.keys(object).forEach((key) =>
    formData.append(key, object[key].toString())
  );
  return formData;
};

export const mapOrder = (array, order, key) => {
  // eslint-disable-next-line func-names
  array.sort(function (a, b) {
    const A = a[key];
    const B = b[key];
    if (order.indexOf(`${A}`) > order.indexOf(`${B}`)) {
      return 1;
    }
    return -1;
  });
  return array;
};

export const getDateWithFormat = () => {
  const today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; // January is 0!

  const yyyy = today.getFullYear();
  if (dd < 10) {
    dd = `0${dd}`;
  }
  if (mm < 10) {
    mm = `0${mm}`;
  }
  return `${dd}.${mm}.${yyyy}`;
};

export const getCurrentTime = () => {
  const now = new Date();
  return `${now.getHours()}:${now.getMinutes()}`;
};

export const getCurrentUser = async () => {
  let user = null;

  try {
    let userData = await AsyncStorage.getItem('userData');
    user = JSON.parse(userData);
  } catch (error) {
    console.log('>>>>: src/helpers/Utils.js  : getCurrentUser -> error', error);
    user = null;
  }

  return user;
};

export const setCurrentUser = async (user) => {
  try {
    if (user) {
      await AsyncStorage.setItem('userData', JSON.stringify(user));
    } else {
      await AsyncStorage.removeItem('userData');
    }
  } catch (error) {
    console.log('>>>>: src/helpers/Utils.js : setCurrentUser -> error', error);
  }
};

export const getCurrentToken = async () => {
  let token = null;
  try {
    token =
      (await AsyncStorage.getItem('JWT-TOKEN')) != null
        ? JSON.parse(await AsyncStorage.getItem('JWT-TOKEN'))
        : null;
  } catch (error) {
    console.log(
      '>>>>: src/helpers/Utils.js  : getCurrentToken -> error',
      error
    );
    token = null;
  }
  return token;
};

export const setCurrentToken = async (token) => {
  try {
    if (token) {
      await AsyncStorage.setItem('JWT-TOKEN', JSON.stringify(token));
    } else {
      await AsyncStorage.removeItem('JWT-TOKEN');
    }
  } catch (error) {
    console.log('>>>>: src/helpers/Utils.js : setCurrentToken -> error', error);
  }
};

export const getFediUser = async () => {
  let user = null;

  try {
    let userData = await AsyncStorage.getItem('fediUserData');
    user = JSON.parse(userData);
  } catch (error) {
    console.log('>>>>: src/helpers/Utils.js  : getFediUser -> error', error);
    user = null;
  }

  return user;
};

export const setFediUser = async (user) => {
  try {
    if (user) {
      await AsyncStorage.setItem('fediUserData', JSON.stringify(user));
    } else {
      await AsyncStorage.removeItem('fediUserData');
    }
  } catch (error) {
    console.log('>>>>: src/helpers/Utils.js : setFediUser -> error', error);
  }
};

export const RenderHTML = ({ HTML }) => (
  // eslint-disable-next-line react/no-danger
  <span className="text-break" dangerouslySetInnerHTML={{ __html: HTML }} />
);

export const bytesToSize = (bytes) => {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};
