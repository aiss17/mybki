import React, { useEffect, useRef, useState } from 'react';
// import { connect } from 'react-redux';
import { AppState, StyleSheet, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import {
  getCurrentToken,
  getCurrentUser,
  setCurrentToken,
} from './helpers/Utils';
import {
  loadCurrentUserSuccess,
  loginUserSuccess,
  refreshToken,
} from './redux/auth/actions';
import { configureStore } from './redux/store';
import { appContainerRef } from './RootNavigation';
import DefaultStack from './stacks/DefaultStack';
import { POST } from './helpers/apiHelper';

export const store = configureStore();
export const persistor = persistStore(store);

const AppContainer = createAppContainer(DefaultStack);
const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
});

const App = (props) => {
  const appState = useRef(AppState.currentState);
  const [lockScreen, setLockScreen] = useState(false);

  const handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      getCurrentUser().then((user) => {
        if (user) {
          setLockScreen(true);
        } else {
          setLockScreen(false);
        }
      });
    }

    appState.current = nextAppState;
  };

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, []);

  let current = null;
  getCurrentToken().then((result) => {
    current = result;
  });

  useEffect(() => {
    async function refreshTokenBki(key) {
      const data = {
        key,
      };
      const res = await POST(`regenerate-token`, data);
      if (res?.status === 'success') {
        const oldKeys = current?.key_bki;
        const newKeys = {
          ...oldKeys,
          key_bki: res?.key,
        };
        setCurrentToken(newKeys);
        refreshToken(newKeys);
      }
    }
    const JwtToken = current;
    const timeout = setInterval(() => {
      refreshTokenBki(JwtToken?.key_bki);
    }, 60 * 1000);
    return () => clearInterval(timeout);
  }, [current]);

  return (
    <View style={styles.flex}>
      <Provider store={store}>
        <AppContainer ref={appContainerRef} screenProps={props} />
      </Provider>
    </View>
  );
};

export default App;

const getAsyncStorage = () => {
  return (dispatch) => {
    getCurrentUser().then((result) => {
      dispatch(loadCurrentUserSuccess(result));
    });
    getCurrentToken().then((result) => {
      dispatch(loginUserSuccess(result));
    });
  };
};

// Dispatch the getAsyncStorage() action directly on the store.
store.dispatch(getAsyncStorage());
