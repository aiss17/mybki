import { createStackNavigator } from 'react-navigation-stack';
import BottomTabs from '../screens/bottom-tabs';
// import DetailService from '../screens/services/components/detail';
import Certificates from '../screens/certificates';
import CertificateDetail from '../screens/certificates/components/detail';
import Invoices from '../screens/invoices';
import MyCompany from '../screens/my-company';
import Quotation from '../screens/quotation';
import Satisfaction from '../screens/satisfaction';
import QuotationDetail from '../screens/quotation/components/detail';
import Orders from '../screens/orders';
import Payments from '../screens/payments';
import PaymentConfirmation from '../screens/payments/components/detail/manual';
import PaymentConfirmationNonManual from '../screens/payments/components/detail/nonManual';
import UnderConstruction from '../screens/under-construction';
import Vessels from '../screens/vessels';
import VesselDetail from '../screens/vessels/components/detail';
import ListVessel from '../screens/vessels/components/detail-fleet';
import DetailVessel from '../screens/vessels/components/detail-fleet/detail-vessel';
import VideoViewer from '../screens/video-viewer';
import WebViewer from '../screens/web-viewer';
import Profile from '../screens/profile';
import AddNewPayments from '../screens/payments/components/addNew';

// import CartDetail from '../screens/services/components/cart'

const AppStack = createStackNavigator(
  {
    BottomTabs,
    VideoViewer,
    WebViewer,
    UnderConstruction,
    Vessels,
    VesselDetail,
    ListVessel,
    DetailVessel,
    Certificates,
    CertificateDetail,
    Invoices,
    Payments,
    Orders,
    MyCompany,
    Quotation,
    QuotationDetail,
    PaymentConfirmation,
    PaymentConfirmationNonManual,
    Profile,
    AddNewPayments,
    Satisfaction,
    // CartDetail,
    // DetailService,
  },
  {
    index: 0,
    initialRouteName: 'BottomTabs',
    /** No Header METHOD 1  */
    headerMode: 'none',
    /** No Header METHOD 2  */
    defaultNavigationOptions: {
      // header: () => null,
    },
  }
);

export default AppStack;
