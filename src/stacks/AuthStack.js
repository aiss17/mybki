import { createStackNavigator } from 'react-navigation-stack';
import Login from '../screens/login';

const AuthStack = createStackNavigator(
  {
    Login,
  },
  {
    index: 0,
    initialRouteName: 'Login',
    /** No Header METHOD 1  */
    headerMode: 'none',
    /** No Header METHOD 2  */
    defaultNavigationOptions: {
      // header: () => null,
    },
  }
);

export default AuthStack;
