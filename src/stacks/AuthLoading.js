import React, { useEffect } from 'react';
import { ActivityIndicator } from 'react-native';
import { getCurrentUser } from '../helpers/Utils';

const AuthLoading = ({ navigation }) => {
  useEffect(() => {
    getCurrentUser().then((user) => {
      navigation.navigate(user ? 'BottomTabs' : 'Auth');
    });
  });

  return <ActivityIndicator />;
};

export default AuthLoading;
