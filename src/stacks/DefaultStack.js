import { createSwitchNavigator } from 'react-navigation';
import AppStack from './AppStack';
import AuthLoading from './AuthLoading';
import AuthStack from './AuthStack';

export default createSwitchNavigator(
  {
    AuthLoading,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    index: 0,
    initialRouteName: 'AuthLoading',
    /** No Header METHOD 1  */
    headerMode: 'none',
    /** No Header METHOD 2  */
    defaultNavigationOptions: {
      // header: () => null,
    },
  }
);
